package org.bitbucket.unaszole.xmlstreameditor.builders;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

public class EndElementBuilder
{
	private static XMLEventFactory FACTORY = XMLEventFactory.newInstance();
	private QName qname;
	
	private EndElementBuilder(QName qname)
	{
		this.qname = qname;
	}
	
	public static EndElementBuilder from(EndElement originalElt)
	{
		return new EndElementBuilder(originalElt.getName());
	}
	
	public static EndElementBuilder forStart(StartElement originalElt)
	{
		return new EndElementBuilder(originalElt.getName());
	}
	
	public static EndElementBuilder newElt(QName qname)
	{
		return new EndElementBuilder(qname);
	}
	
	public EndElement build()
	{
		return FACTORY.createEndElement(qname, null);
	}
}