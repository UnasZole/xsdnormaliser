package org.bitbucket.unaszole.xmlstreameditor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import org.bitbucket.unaszole.xmlstreameditor.builders.EventList;
import org.bitbucket.unaszole.xmlstreameditor.xml.XMLElement;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.collections4.Predicate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
	Abstract class to simplify implementation of an EventHandler that is modifying of the structure of the XML document.
	@param <Scope> The contents of a scope for the specific implementation.
	@param <Context> The interface the context should implement in order for this handler to read from or write to it.
*/
public abstract class StructureEditingEventHandler<Scope extends StructureEditingEventHandler.EditionScope, GivenContext extends StructureAwareEventHandler.Context> extends StructureAwareEventHandler<Scope, GivenContext>
{
	private static final Logger LOGGER = LoggerFactory.getLogger(StructureEditingEventHandler.class);
	
	/**
		The edition scope contains a sequence of elements, all immediate siblings, that may need to be edited.
		It contains at least one element (based on the StartElement event which opened the scope).
	*/
	public static class EditionScope extends ScopedEventHandler.BasicScope
	{
		private List<XMLElement> elementsInScope = null;
		
		public EditionScope(XMLElement firstElement)
		{
			elementsInScope = new ArrayList<XMLElement>();
			elementsInScope.add(firstElement);
		}
		
		public List<XMLElement> getElementsInScope()
		{
			return elementsInScope;
		}
		
		/**
			Add a new element to the scope.
			This element MUST be the immediate next sibling of the last element added.
		*/
		public void addElementToScope(XMLElement newElement)
		{
			if(elementsInScope.size() > 0)
			{
				XMLElement lastEltInScope = elementsInScope.get(elementsInScope.size() - 1);
				if(!lastEltInScope.getParent().equals(newElement.getParent()) ||
					newElement.getIndex() != lastEltInScope.getIndex() + 1)
				{
					throw new UnsupportedOperationException("Attempting to add element to scope that is not next sibling.");
				}
			}
			elementsInScope.add(newElement);
		}
	}
	
	/**
	 * Implementation of the ElementLocator that supports modifying the structural information it contains.
	 * Has private visibility, because no modification of the elements structure should be done outside of this class, to ensure it always match actual operations on the document.
	 */
	private static class EditableElementLocator extends XMLElement
	{
		public EditableElementLocator(EditableElementLocator parent, StartElement event)
		{
			super(parent, event);
		}
		
		public void setEndEvent(EndElement event)
		{
			this.endEvent = event;
		}
		
		public void removeFromDocument()
		{
			isInDocument = false;
			if(parent != null)
			{
				((EditableElementLocator)parent).children.remove(this);
			}
			parent = null;
		}
		
		public void insertAt(XMLElement newParent, int index)
		{
			if(isInDocument)
			{
				throw new UnsupportedOperationException("Inserting element which is already in document : " + this);
			}
			((EditableElementLocator)newParent).children.add(index, this);
			parent = newParent;
			isInDocument = true;
		}
	}
	
	/**
	 * @param parent A parent element, or null if the new element is root.
	 * @param event The XML event corresponding to the opening tag of this element.
	 * @return A new ElementLocator, as last child of the given parent.
	 */
	protected static XMLElement getElementLocator(XMLElement parent, StartElement event)
	{
		return new EditableElementLocator((EditableElementLocator)parent, event);
	}
	
	protected static void registerElementLocatorEnd(XMLElement elt, EndElement event)
	{
		((EditableElementLocator)elt).setEndEvent(event);
	}
	
	/**
		Represents an element that is not part of the document, providing facilities to edit it and insert it.
	*/
	public static class ExtractedElement
	{
		private XMLElement locator;
		private List<XMLEvent> contents;
		
		/**
		 * @param locator The element to represent.
		 * @param contents All XML events (from StartElement to EndElement and everything in between) belonging to this element.
		 */
		public ExtractedElement(XMLElement locator, List<XMLEvent> contents)
		{
			this.locator = locator;
			this.contents = contents;
		}
		
		/**
		 * Create a new element from scratch, providing start and end event, so it can be inserted with an ElementsEditor.
		 */
		public ExtractedElement(StartElement startEvent, EndElement endEvent)
		{
			// Instantiate element locator as root, and mark it as removed from document.
			this.locator = new EditableElementLocator(null, startEvent);
			((EditableElementLocator)this.locator).removeFromDocument();
			// Set element contents as just the start and end element.
			this.contents = Arrays.asList(startEvent, endEvent);
		}
		
		public XMLElement getLocator()
		{
			return locator;
		}
		
		public ElementsEditor getEditor()
		{
			return new ElementsEditor(this.contents);
		}
		
		public List<XMLEvent> getEventList()
		{
			return ListUtils.unmodifiableList(this.contents);
		}
	}
	
	/**
	 * Editor to perform structural changes on XML elements without a direct access to the list of events.
	 */
	public static class ElementsEditor
	{
		public static enum Insert
		{
			BEFORE, AFTER, AT_START_OF, AT_END_OF;
		}
		
		private List<XMLEvent> eventList;
		
		/**
		 * @param eventList A reference to the list of events that this editor can update.
		 * 	This list must contain all events (startElement, endElement, and everything in between) related to an element, if this element needs to be edited.
		 */
		public ElementsEditor(List<XMLEvent> eventList)
		{
			this.eventList = eventList;
		}
		
		/**
		 * Find the index of a given XML event using object instance identity.
		 * (XMLEvent overrides equals with a functional equality check, making regular List.indexOf irrelevant here.)
		 * @param searchedEvent The event to search for
		 * @return Its index in the list managed by this editor.
		 */
		private int indexOf(final XMLEvent searchedEvent)
		{
			return ListUtils.indexOf(this.eventList, new Predicate<XMLEvent>() {
				@Override
				public boolean evaluate(XMLEvent event)
				{
					return searchedEvent == event;
				}
			});
		}
		
		/**
		 * Remove an element from the event list managed by this editor.
		 * @param element The element to remove.
		 * @return The element that was extracted, so that it can be modified and reinserted somewhere else.
		 */
		public ExtractedElement removeElement(XMLElement element)
		{
			LOGGER.debug("Removing element {}", element);
			int startEventIndex = indexOf(element.getEvent());
			int endEventIndex = ScopedEventHandler.indexOfEndElementFor(startEventIndex, this.eventList);
			if(startEventIndex == -1 || endEventIndex == -1)
			{
				throw new UnsupportedOperationException("Cannot delete out of scope element " + element);
			}
			
			List<XMLEvent> elementEvents = new ArrayList<XMLEvent>();
			for(int i = startEventIndex; i <= endEventIndex; i++)
			{
				elementEvents.add(this.eventList.remove(startEventIndex));
			}
			((EditableElementLocator)element).removeFromDocument();

			return new ExtractedElement(element, elementEvents);
		}
		
		/**
		 * Insert a new element in the event list managed by this editor.
		 * @param toInsert The element to insert.
		 * @param position The position rule to use, relative to the reference element.
		 * @param referenceElement The element used to determine the position of the generated element.
		 */
		public void insertElement(ExtractedElement toInsert, Insert position, XMLElement referenceElement)
		{
			LOGGER.debug("Inserting element {} {} {}", toInsert.getLocator(), position, referenceElement);
			EditableElementLocator eltToInsert = (EditableElementLocator)toInsert.getLocator();
			
			int refEltStartIndex = indexOf(referenceElement.getEvent());
			int refEltEndIndex = ScopedEventHandler.indexOfEndElementFor(refEltStartIndex, this.eventList);
			
			int targetEventInsertionIndex = -1;
			XMLElement newParent = null;
			int targetChildIndex = -1;
			
			switch(position)
			{
				case BEFORE:
					// Insert before reference element.
					
					// Insert events at same index as reference element start (ie just before).
					targetEventInsertionIndex = refEltStartIndex;
					
					// As sibling of reference element, same index (ie just before).
					newParent = referenceElement.getParent();
					targetChildIndex = newParent.getChildren().indexOf(referenceElement);
					break;
				case AFTER:
					// Insert after reference element
					
					// Insert events at index immediately after reference element end.
					targetEventInsertionIndex = refEltEndIndex + 1;
					
					// As sibling of reference element, index plus one (ie just after).
					newParent = referenceElement.getParent();
					targetChildIndex = newParent.getChildren().indexOf(referenceElement) + 1;
					break;
				case AT_START_OF:
					// Insert inside reference element, at beginning.
					
					// Insert events at index immediately after start.
					targetEventInsertionIndex = refEltStartIndex + 1;
					
					// As child of reference element, index 0.
					newParent = referenceElement;
					targetChildIndex = 0;
					break;
				case AT_END_OF:
					// Insert inside reference element, at end.
					
					// Insert events at same index as end (ie just before).
					targetEventInsertionIndex = refEltEndIndex;
					
					// As child of reference element, last index.
					newParent = referenceElement;
					targetChildIndex = referenceElement.getChildren().size();
					break;
			}
			// Perform event list update.
			this.eventList.addAll(targetEventInsertionIndex, toInsert.getEventList());
			// Perform element locator update.
			eltToInsert.insertAt(newParent, targetChildIndex);
		}
	}

	/**
		Check if the provided event should open a new scope, and create it if needed.
		@param childElement A description of the new child element's position in document structure, and all data from the StartElement event that declared it.
		@param currentScope The current scope. Could be null if we are not in an open scope.
		@param context The context of the document currently being edited.
		@return The new scope to open, or null if not needed.
	*/
	protected abstract Scope openChildElementScopeIfNeeded(XMLElement childElement, Scope parentElementScope, GivenContext context);
	
	@Override
	protected final Scope openScopeIfNeeded(XMLEvent event, Scope currentScope, GivenContext context, XMLElement currentElementLocator)
	{
		if(event.isStartElement())
		{
			// Can only open scope on new element...
			if(currentScope == null ||
				!currentScope.getElementsInScope().get(0).getParent().equals(currentElementLocator.getParent()))
			{
				// ...That is not sibling of current scope. It can only be a descendant of one of the elements in the scope.
				return openChildElementScopeIfNeeded(currentElementLocator, currentScope, context);
			}
		}
		return null;
	}

	@Override
	protected final List<XMLEvent> handleEventInScope(XMLEvent event, Scope currentScope, GivenContext context, XMLElement currentElementLocator)
	{
		// Nothing done at handle step : all edits performed at close scope, once full elements are read.
		return EventList.from(event);
	}
	
	/**
		Check if the current scope is complete with all elements it contains and should be closed.
		The closed scope will be available for edition to the editElements method.
		This method is guaranteed to be called only when the element that started the scope is complete.
		@param currentScope The current scope.
		@param context The context of the document currently being edited.
		@return True if current scope should be closed, false if it should be kept opened.
	*/
	protected abstract boolean isScopeComplete(Scope currentScope, GivenContext context);
	
	@Override
	protected final boolean isClosingScope(XMLEvent event, Scope currentScope, GivenContext context, XMLElement currentElementLocator)
	{
		if(event.isEndElement())
		{
			// Can only close scope when ending an element that is either the opening element or a next sibling (ie an element in scope), or the parent - or any ancestor, if the scope was kept open by another handler for too long.
			
			if(currentScope.getElementsInScope().indexOf(currentElementLocator) >= 0)
			{
				return isScopeComplete(currentScope, context);
			}
			
			XMLElement scopeAncestor = currentScope.getElementsInScope().get(0).getParent();
			while(scopeAncestor != null)
			{
				if(scopeAncestor.equals(currentElementLocator))
				{
					return isScopeComplete(currentScope, context);
				}
				scopeAncestor = scopeAncestor.getParent();
			}
		}
		return false;
	}
	
	/**
		Perform structure edition tasks on the elements in the scope being closed.
		@param editor The editor to use to perform edition tasks on all elements in scope.
			It is guaranteed to be able to work on all elements in scope.
			It may be able to work on following elements if scope was closed later than requested, but this should NOT be taken for granted.
		@param scope The scope in which edition is performed.
		@param context The context of the document currently being edited.
	*/
	protected abstract void editElements(ElementsEditor editor, Scope scope, GivenContext context);
	
	@Override
	protected final List<XMLEvent> closeScope(List<XMLEvent> events, Scope closedScope, GivenContext context)
	{
		// Instantiate an editor with a shallow copy of the source list and pass it down to specific implementation.
		List<XMLEvent> editedEvents = new ArrayList<XMLEvent>(events);
		editElements(new ElementsEditor(editedEvents), closedScope, context);
		return editedEvents;
	}
}