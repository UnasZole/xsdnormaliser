package org.bitbucket.unaszole.xmlstreameditor.eventsink;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;
import org.bitbucket.unaszole.xmlstreameditor.XMLEventHandler;

public class SinkManagerImpl<GlobalContext> implements SinkManager
{
	private EventSink rootSink;
	private List<BufferedSink> bufferedSinks;
	private List<XMLEventHandler> sinkOwners;
	
	public SinkManagerImpl(EventSink rootSink)
	{
		this.bufferedSinks = new ArrayList<BufferedSink>();
		this.sinkOwners = new ArrayList<XMLEventHandler>();
		this.rootSink = rootSink;
	}
	
	public void write(XMLEvent event) throws XMLStreamException
	{
		EventSink currentSink = (bufferedSinks.size() > 0 ? bufferedSinks.get(0) : rootSink);
		currentSink.write(event);
	}
	
	public List<XMLEventHandler> getBufferOwners()
	{
		return Collections.unmodifiableList(sinkOwners);
	}
	
	private void flushUnpluggedSinks()
	{
		int numSinks = sinkOwners.size();
		while(numSinks > 0 && sinkOwners.get(numSinks - 1) == null)
		{
			// Last sink has no owner.
			BufferedSink lastSink = bufferedSinks.get(numSinks - 1);
			EventSink previousSink = numSinks >= 2 ? bufferedSinks.get(numSinks - 2) : rootSink;
			
			try
			{
				// Dump it into previous sink.
				for(XMLEvent event : lastSink.flush())
				{
					previousSink.write(event);
				}
			}
			catch(XMLStreamException e)
			{
				// TODO : improve exception management.
				e.printStackTrace();
			}
			
			// And remove it from the list.
			sinkOwners.remove(numSinks - 1);
			bufferedSinks.remove(numSinks - 1);
			numSinks--;
		}
	}
	
	/**
		Unplug a handler : all its sinks will be automatically flushed.
	*/
	public void unplug(XMLEventHandler handler)
	{
		for(int i = 0; i < sinkOwners.size(); i++)
		{
			if(this.sinkOwners.get(i) == handler)
			{
				//All occurrences of this handler as owner are replaced by null.
				sinkOwners.set(i, null);
			}
		}
		
		flushUnpluggedSinks();
	}
	
	@Override
	public void startBuffer(XMLEventHandler owner)
	{
		if(owner == null)
		{
			throw new IllegalArgumentException("You must pass an actual instance of XMLEventHandler to this method, not null.");
		}
		
		bufferedSinks.add(0, new BufferedSink());
		sinkOwners.add(0, owner);
	}
	
	@Override
	public boolean canFlush(XMLEventHandler owner)
	{
		if(owner == null)
		{
			throw new IllegalArgumentException("You must pass an actual instance of XMLEventHandler to this method, not null.");
		}
		
		if(sinkOwners.size() > 0 && sinkOwners.get(0) == owner)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	@Override
	public void flushAndWrite(XMLEventHandler owner, SinkManager.EventListTransformer transformer) throws XMLStreamException
	{
		if(!canFlush(owner))
		{
			throw new UnsupportedOperationException("Cannot flush a sink which is not yours.");
		}
		
		sinkOwners.remove(0);
		List<XMLEvent> flushedEvents = bufferedSinks.remove(0).flush();
		flushUnpluggedSinks();
		
		if(transformer != null)
		{
			flushedEvents = transformer.transform(flushedEvents);
		}
		
		for(XMLEvent event : flushedEvents)
		{
			write(event);
		}
	}
	
	/**
		Sort a list of XMLEventHandler, by order in which they would be able to pop from the stack.
		Meaning the owner of the current head goes first, etc.
		@param handlers The list of handlers to sort.
	*/
	public void sortHandlersByOwnedSink(List<XMLEventHandler<? super GlobalContext>> handlers)
	{
		Collections.sort(handlers, new Comparator<XMLEventHandler<? super GlobalContext>>() {
			public int compare(XMLEventHandler<? super GlobalContext> left, XMLEventHandler<? super GlobalContext> right)
			{
				return sinkOwners.indexOf(left) - sinkOwners.indexOf(right);
			}
		});
	}
}