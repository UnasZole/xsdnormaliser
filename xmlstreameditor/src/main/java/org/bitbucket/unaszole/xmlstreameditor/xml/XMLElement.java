package org.bitbucket.unaszole.xmlstreameditor.xml;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.xml.namespace.NamespaceContext;
import javax.xml.namespace.QName;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.EndElement;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.lang3.StringUtils;

/**
 * This represents a given XML element and its place in the structure of a document.
 * This class is immutable from the perspective of its user, but its fields may be modified by implementer to report structural changes.
 */
public abstract class XMLElement
{
	protected XMLElement parent;
	protected StartElement event;
	protected EndElement endEvent;
	protected List<XMLElement> children;
	protected boolean isInDocument;
	
	/**
		This constructor defines an element as last child of the given parent.
	*/
	public XMLElement(XMLElement parent, StartElement event)
	{
		this.parent = parent;
		this.event = event;
		this.endEvent = null;
		this.children = new ArrayList<XMLElement>();
		this.isInDocument = true;
		
		if(parent != null)
		{
			parent.children.add(this);
		}
	}
	
	public XMLElement getParent()
	{
		return parent;
	}
	public StartElement getEvent()
	{
		return event;
	}
	public EndElement getEndEvent()
	{
		return endEvent;
	}
	public int getIndex()
	{
		if(parent == null)
		{
			return 1;
		}
		return parent.children.indexOf(this) + 1;
	}
	public List<XMLElement> getChildren()
	{
		return ListUtils.unmodifiableList(children);
	}
	public boolean isInDocument()
	{
		return isInDocument && (parent == null || parent.isInDocument());
	}
	
	/**
	 * @param nsContext A namespaceContext from which to fetch the prefixes to use for each namespace.
	 * @param uniqueAttributes A set of attribute names for attributes to consider as "unique" identifiers :
	 * 	no sibling of this element can share the same value for this attribute, making it a readable and reliable selection criteria.
	 * @return a readable XPath from the document root to this element.
	 */
	public String readableXPath(NamespaceContext nsContext, Set<String> uniqueAttributes)
	{
		if(!isInDocument)
		{
			return "{DELETED NODE:" + this.event.getName().getLocalPart() + "}";
		}
	
		String nodeTest = "*";
		List<String> predicates = new ArrayList<String>();
		
		QName eventName = this.event.getName();
		if(nsContext != null)
		{
			// If namespace context provided, we can try to use a more readable nodeTest : resolve namespace prefix and local name.
			String nsPrefix = nsContext.getPrefix(eventName.getNamespaceURI());
			if(nsPrefix != null)
			{
				nodeTest = nsPrefix + ":" + eventName.getLocalPart();
			}
		}
		
		if(nodeTest.equals("*"))
		{
			// If node test could not be made readable, add a predicate to select on element name.
			predicates.add("local-name()='" + eventName.getLocalPart() + "'");
		}
		
		boolean uniqueAttributeFound = false;
		if(uniqueAttributes != null)
		{
			Iterator<Attribute> attrs = this.event.getAttributes();
			while(attrs.hasNext())
			{
				Attribute attr = attrs.next();
				String attrName = attr.getName().getLocalPart();
				if(uniqueAttributes.contains(attrName))
				{
					predicates.add("@" + attrName + "='" + attr.getValue() + "'");
					uniqueAttributeFound = true;
				}
			}
		}
		
		if(!uniqueAttributeFound && this.parent != null)
		{
			// If no unique attribute has been found, use a position predicate instead if needed.
			
			if(nodeTest.equals("*"))
			{
				// If there is no node test, XPath position is actual node index.
				if(this.parent.getChildren().size() > 1)
				{
					// Only add position predicate if there is more than one node.
					predicates.add("position()=" + getIndex());
				}
			}
			else
			{
				// A node test based on element name was set : XPath position is the index in the nodeList resulting from
				// that node test, ie in the list of siblings with same name.
				
				final XMLElement currentElement = this;
				
				List<XMLElement> siblingsWithSameName = new ArrayList<XMLElement>();
				
				CollectionUtils.select(this.parent.getChildren(), new Predicate<XMLElement>() {
					@Override
					public boolean evaluate(XMLElement element)
					{
						return 
							element == currentElement ||
							currentElement.getEvent().getName().equals(element.getEvent().getName());
					}
				}, siblingsWithSameName);
				
				if(siblingsWithSameName.size() > 1)
				{
					// Only add position predicate if there is more than one sibling with same name.
					int index = siblingsWithSameName.indexOf(this);
					predicates.add(Integer.toString(index));
				}
			}
		}
		
		return (parent == null ? "" : parent.readableXPath(nsContext, uniqueAttributes)) +
			"/" + nodeTest +
			(predicates.size() == 0 ? "" : "[" + StringUtils.join(predicates, " and ") + "]");
	}
	
	public String toString()
	{
		return readableXPath(null, null);
	}
}