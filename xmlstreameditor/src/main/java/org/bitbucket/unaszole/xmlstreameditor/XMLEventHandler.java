package org.bitbucket.unaszole.xmlstreameditor;

import java.util.List;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;
import org.bitbucket.unaszole.xmlstreameditor.eventsink.SinkManager;

/**
	A class that can handle an XMLEvent.
	This means it can read from it and/or from the context, and return a list of events to replace it and/or write in the context.
	
	Note that it is also part of the contract of XMLEventHandler that the class must have a default constructor (without parameter), as this will be used by the XMLStreamEditor to instantiate it when needed.
	
	@param <Context> The interface the context should implement in order for this handler to read from or write to it.
*/
public interface XMLEventHandler<Context>
{
	/**
		Handle an XML event.
		@param event The event to handle.
		@param sinkManager The sinkManager to use for buffering changes.
		@param context The context of the document currently being edited.
		@return null if nothing to change; else a list of events to replace the received one.
	*/
	List<XMLEvent> handle(XMLEvent event, SinkManager sinkManager, Context context) throws XMLStreamException;
}