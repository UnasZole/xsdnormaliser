package org.bitbucket.unaszole.xmlstreameditor.builders;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.Namespace;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

public class StartElementBuilder
{
	private static XMLEventFactory FACTORY = XMLEventFactory.newInstance();
	private QName qname;
	private List<Attribute> attributes;
	private List<Namespace> namespaces;
	
	private StartElementBuilder(QName qname, Iterator<Attribute> attributes, Iterator<Namespace> namespaces)
	{
		this.qname = qname;
		this.attributes = new ArrayList<Attribute>();
		if(attributes != null)
		{
			while(attributes.hasNext())
			{
				this.attributes.add(attributes.next());
			}
		}
		this.namespaces = new ArrayList<Namespace>();
		if(namespaces != null)
		{
			while(namespaces.hasNext())
			{
				this.namespaces.add(namespaces.next());
			}
		}
	}
	
	public static StartElementBuilder from(StartElement originalElt)
	{
		return new StartElementBuilder(originalElt.getName(), originalElt.getAttributes(), originalElt.getNamespaces());
	}
	
	public static StartElementBuilder newElt(QName qname)
	{
		return new StartElementBuilder(qname, null, null);
	}
	
	public StartElementBuilder withAttribute(QName qname, String value)
	{
		for(int i = 0; i < attributes.size(); i++)
		{
			Attribute attr = attributes.get(i);
			if(qname.equals(attr.getName()))
			{
				if(value == null)
				{
					// If null value, remove attribute.
					attributes.remove(i);
					return this;
				}
				else
				{
					// Else, replace attribute with new value.
					attributes.set(i, FACTORY.createAttribute(qname, value));
					return this;
				}
			}
		}
		
		// If attribute not found, append.
		attributes.add(FACTORY.createAttribute(qname, value));
		return this;
	}
	
	public StartElementBuilder withNamespace(String prefix, String uri)
	{
		for(int i = 0; i < namespaces.size(); i++)
		{
			Namespace ns = namespaces.get(i);
			if(prefix.equals(ns.getPrefix()))
			{
				if(uri == null)
				{
					// If null value, remove namespace.
					namespaces.remove(i);
					return this;
				}
				else
				{
					// Else, replace namespace with new value.
					namespaces.set(i, FACTORY.createNamespace(prefix, uri));
					return this;
				}
			}
		}
		
		// If attribute not found, append.
		namespaces.add(FACTORY.createNamespace(prefix, uri));
		return this;
	}
	
	public StartElement build()
	{
		return FACTORY.createStartElement(qname, attributes.iterator(), namespaces.iterator());
	}
}