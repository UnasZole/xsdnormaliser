package org.bitbucket.unaszole.xmlstreameditor.eventsink;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;

public interface EventSink
{
	void write(XMLEvent event) throws XMLStreamException;
}