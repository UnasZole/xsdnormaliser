package org.bitbucket.unaszole.xmlstreameditor.eventsink;

import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;

public class OutputSink implements EventSink
{
	private XMLEventWriter writer;
	
	public OutputSink(XMLEventWriter writer)
	{
		this.writer = writer;
	}
	
	@Override
	public void write(XMLEvent event) throws XMLStreamException
	{
		writer.add(event);
	}
}

