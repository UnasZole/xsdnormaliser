package org.bitbucket.unaszole.xmlstreameditor.eventsink;

import java.util.ArrayList;
import java.util.List;
import javax.xml.stream.events.XMLEvent;

public class BufferedSink implements EventSink
{
	private List<XMLEvent> bufferedEvents;
	
	public BufferedSink()
	{
		this.bufferedEvents = new ArrayList<XMLEvent>();
	}
	
	@Override
	public void write(XMLEvent event)
	{
		bufferedEvents.add(event);
	}
	
	public List<XMLEvent> flush()
	{
		List<XMLEvent> flushedEvents = bufferedEvents;
		bufferedEvents = new ArrayList<XMLEvent>();
		return flushedEvents;
	}
}

