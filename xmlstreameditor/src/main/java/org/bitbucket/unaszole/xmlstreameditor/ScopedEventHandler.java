package org.bitbucket.unaszole.xmlstreameditor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;
import org.bitbucket.unaszole.xmlstreameditor.eventsink.SinkManager;

/**
	Abstract class to simplifiy implementation of an EventHandler supporting nested scopes for its operations.
	This simplifies the setup if one needs to work on a complex XML structure spanning over several events, which could potentially appear nested inside itself.
	Typically, if one needs to read or edit all instances of an element that can appear inside its own body.
	@param <Scope> The contents of a scope for the specific implementation.
	@param <Context> The interface the context should implement in order for this handler to read from or write to it.
*/
public abstract class ScopedEventHandler<Scope extends ScopedEventHandler.BasicScope, Context> implements XMLEventHandler<Context>
{
	/**
		Utility method for scoped handlers. Given the index of a StartElement in an event list, find the index of the corresponding EndElement event.
		Useful in closeScope method, since there is no way of knowing how much data has been added or modified in the list since the closing was requested.
		@param startEltIndex Index of a StartElement event in the list.
		@param events The list of events to search.
		@return The index of the matching EndElement in the list, or -1 if none found.
	*/
	public static int indexOfEndElementFor(int startEltIndex, List<XMLEvent> events)
	{
		int nestingLevel = 0;
		for(int i = startEltIndex; i < events.size(); i++)
		{
			XMLEvent event = events.get(i);
			if(event.isStartElement())
			{
				nestingLevel++;
			}
			else if(event.isEndElement())
			{
				nestingLevel--;
			}
			
			if(nestingLevel == 0)
			{
				return i;
			}
		}
		
		return -1;
	}
	
	public static class BasicScope
	{
		private boolean toBeClosed = false;
		
		public final void setToBeClosed()
		{
			toBeClosed = true;
		}
		
		public final boolean isToBeClosed()
		{
			return toBeClosed;
		}
	}

	private LinkedList<Scope> openScopesBackingList = new LinkedList<Scope>();
	private Queue<Scope> openScopes = Collections.asLifoQueue(openScopesBackingList);
	
	/**
		Check if the provided event should open a new scope, and create it if needed.
		@param event The current event.
		@param currentScope The current scope. Could be null if we are not in an open scope.
		@param context The context of the document currently being edited.
		@return The new scope to open, or null if not needed.
	*/
	protected abstract Scope openScopeIfNeeded(XMLEvent event, Scope currentScope, Context context);
	
	/**
		Check if the provided event should close the current scope.
		@param event The current event.
		@param currentScope The current scope.
		@param context The context of the document currently being edited.
		@return True if current scope should be closed, false if it should be kept opened.
	*/
	protected abstract boolean isClosingScope(XMLEvent event, Scope currentScope, Context context);
	
	/**
		Handle an event.
		@param event The current event.
		@param currentScope The current scope. Could be null if we are not in an open scope.
		@param context The context of the document currently being edited.
		@return The list of events to replace the current event. Null if the event should be left as-is.
	*/
	protected abstract List<XMLEvent> handleEventInScope(XMLEvent event, Scope currentScope, Context context);
	
	/**
		Actually perform the closing of a scope.
		Allows for reviewing and editing all events that were received in that scope.
		WARNING : The list may have been filled after you requested the close, before the close is actually performed.
		Do not assume that the last elements of the list are the last events you wrote, this is not true.
		@param events The list of events that were received in that scope.
		@param closedScope The scope being closed.
		@param context The context of the document currently being edited.
		@return The list of events to replace those that were received in that scope. Must NOT be null.
	*/
	protected abstract List<XMLEvent> closeScope(List<XMLEvent> events, Scope closedScope, Context context);
	
	private void closePendingScopes(final SinkManager sinkManager, final Context context) throws XMLStreamException
	{
		Scope currentScope = openScopes.peek();
		while(currentScope != null && currentScope.isToBeClosed() && sinkManager.canFlush(this))
		{
			// Flush pending events in scope into the parent, applying closing transformation, and close it.
			final Scope closedScope = openScopes.poll();
			sinkManager.flushAndWrite(this, new SinkManager.EventListTransformer() {
				public List<XMLEvent> transform(List<XMLEvent> events)
				{
					return closeScope(events, closedScope, context);
				}
			});
			currentScope = openScopes.peek();
		}
	}
	
	/**
	 * Get the current scope, being the last unclosed one in the LIFO Queue, ie the first unclosed one in the backing LinkedList.
	 */
	private Scope getCurrentScope()
	{
		for(int i = 0; i < openScopesBackingList.size(); i++)
		{
			Scope currentScope = openScopesBackingList.get(i);
			if(!currentScope.isToBeClosed())
			{
				return currentScope;
			}
		}
		return null;
	}
	
	@Override
	public final List<XMLEvent> handle(XMLEvent event, SinkManager sinkManager, Context context) throws XMLStreamException
	{
		// If pending scopes to close, do it first.
		closePendingScopes(sinkManager, context);
		
		Scope currentScope = getCurrentScope();
		
		// Check if current event should open a new scope, and do it.
		Scope newScope = openScopeIfNeeded(event, currentScope, context);
		if(newScope != null)
		{
			sinkManager.startBuffer(this);
			openScopes.offer(newScope);
			// New scope becomes current scope then.
			currentScope = newScope;
		}
		
		// Handle the current event in the current scope.
		List<XMLEvent> returnedEvents = new ArrayList<XMLEvent>();
		List<XMLEvent> result = handleEventInScope(event, currentScope, context);
		if(result == null)
		{
			// Null means no action, keep the event as is.
			returnedEvents.add(event);
		}
		else
		{
			// Else, insert returned list instead.
			returnedEvents.addAll(result);
		}
		
		// If current event should close the current scope, mark it down.
		if(currentScope != null && isClosingScope(event, currentScope, context))
		{
			currentScope.setToBeClosed();
		}
		
		return returnedEvents;
	}
}