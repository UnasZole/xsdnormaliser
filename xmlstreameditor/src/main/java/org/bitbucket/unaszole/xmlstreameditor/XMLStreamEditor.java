package org.bitbucket.unaszole.xmlstreameditor;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import javax.xml.stream.*;
import javax.xml.stream.events.XMLEvent;
import org.bitbucket.unaszole.xmlstreameditor.eventsink.EventSink;
import org.bitbucket.unaszole.xmlstreameditor.eventsink.OutputSink;
import org.bitbucket.unaszole.xmlstreameditor.eventsink.SinkManagerImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class XMLStreamEditor<GlobalContext>
{
	private static final Logger LOGGER = LoggerFactory.getLogger(XMLStreamEditor.class);
	
	public interface Controller<Context>
	{
		/**
			@param context The current context.
			@return True if processing should be terminated at once. (Nothing more will be written to sinkManager)
		*/
		boolean terminate(Context context);
		
		/**
			@param handler A handler instance.
			@param context The current context.
			@return True if handler should be unplugged : it will no longer receive any event to handle.
		*/
		boolean unplug(XMLEventHandler<? super Context> handler, Context context);
	}
	
	private List<Class<? extends XMLEventHandler<? super GlobalContext>>> handlerClasses;
	
	/**
		Build a new XML stream editor.
		@param handlerClasses The list of class objects referencing XMLEventHandler implementations that must be instantiated and executed by this editor.
	*/
	public XMLStreamEditor(List<Class<? extends XMLEventHandler<? super GlobalContext>>> handlerClasses)
	{
		this.handlerClasses = handlerClasses;
	}
	
	private List<XMLEventHandler<? super GlobalContext>> getNewHandlers() throws XMLStreamException
	{
		try
		{
			List<XMLEventHandler<? super GlobalContext>> handlers = new ArrayList<XMLEventHandler<? super GlobalContext>>();
			for(Class<? extends XMLEventHandler<? super GlobalContext>> handlerClass : handlerClasses)
			{
				handlers.add(handlerClass.newInstance());
			}
			return handlers;
		}
		catch(Exception e)
		{
			throw new XMLStreamException("Exception raised while instantiating an XMLEventHandler.", e);
		}
	}
	
	/**
		Run this editor in a given environment.
		@param eventReader The source of all XML events to read from.
		@param sinkManager The sink manager in which to output resulting events.
		@param context The context for this run, that can be read from and written to by handlers. Should normally be a new instance specifically for this run.
		@param controller A controller that manages context-based conditions for terminating the process.
		@return True if file was entirely parsed and output entirely generated; false if processing was interrupted.
	*/
	public boolean run(XMLEventReader eventReader, SinkManagerImpl<GlobalContext> sinkManager, GlobalContext context, Controller<GlobalContext> controller) throws XMLStreamException
	{
		List<XMLEventHandler<? super GlobalContext>> eventHandlers = getNewHandlers();
		
		while (eventReader.hasNext())
		{
			// Make sure to run handler which owns the top sink first, to give it opportunity to close it.
			sinkManager.sortHandlersByOwnedSink(eventHandlers);
			
			// List of events to handle.
			// Necessary since the handling of a single event can create several events.
			List<XMLEvent> eventsToHandle = new ArrayList<XMLEvent>();
			eventsToHandle.add(eventReader.nextEvent());
			
			for(XMLEventHandler<? super GlobalContext> handler : eventHandlers)
			{
				// List of events that this handler will leave for next handlers to process - or for sinkManager to write.
				List<XMLEvent> eventsToHandleAfterThisHandler = new ArrayList<XMLEvent>();
				
				// Handle all events in the list.
				for(XMLEvent eventToHandle : eventsToHandle)
				{
					List<XMLEvent> resultEvents = handler.handle(eventToHandle, sinkManager, context);
					if(resultEvents == null)
					{
						// If handle returns null, it means no action : transmit the event as-is.
						eventsToHandleAfterThisHandler.add(eventToHandle);
					}
					else
					{
						// Else, handle returned the list of events to insert instead of the provided one.
						eventsToHandleAfterThisHandler.addAll(resultEvents);
					}
				}
				
				// Handler processing complete : leave it to the next one.
				eventsToHandle = eventsToHandleAfterThisHandler;
			}
			
			// Processing of all handlers complete : write resulting events.
			for(XMLEvent event : eventsToHandle)
			{
				sinkManager.write(event);
			}
			
			if(controller != null)
			{
				if(controller.terminate(context))
				{
					// If controller requests termination based on current context, return.
					return false;
				}
				else
				{
					for(XMLEventHandler<? super GlobalContext> handler : eventHandlers)
					{
						if(controller.unplug(handler, context))
						{
							// If controller requests unplugging a given handler based on current context, remove it from the list and unplug its pending sinks.
							eventHandlers.remove(handler);
							sinkManager.unplug(handler);
						}
					}
				}
			}
		}
		
		List<XMLEventHandler> bufferOwners = sinkManager.getBufferOwners();
		if(bufferOwners.size() > 0)
		{
			LOGGER.error("XMLStreamEditor process terminated with active buffers from : {}", bufferOwners);
		}
		
		return true;
	}
	
	public boolean run(XMLEventReader eventReader, EventSink rootSink, GlobalContext context, Controller<GlobalContext> controller) throws XMLStreamException
	{
		return run(eventReader, new SinkManagerImpl(rootSink), context, controller);
	}
	
	public boolean run(XMLEventReader eventReader, XMLEventWriter writer, GlobalContext context, Controller<GlobalContext> controller) throws XMLStreamException
	{
		return run(eventReader, new OutputSink(writer), context, controller);
	}
	
	public boolean run(InputStream input, EventSink rootSink, GlobalContext context, Controller<GlobalContext> controller) throws XMLStreamException
	{
		XMLInputFactory inFactory = XMLInputFactory.newInstance();
		XMLEventReader eventReader = inFactory.createXMLEventReader(input);
		
		return run(eventReader, rootSink, context, controller);
	}
	
	public boolean run(InputStream input, OutputStream output, GlobalContext context, Controller<GlobalContext> controller) throws XMLStreamException
	{
		XMLInputFactory inFactory = XMLInputFactory.newInstance();
		XMLEventReader eventReader = inFactory.createXMLEventReader(input);
		
		XMLOutputFactory factory = XMLOutputFactory.newInstance();
		factory.setProperty(XMLOutputFactory.IS_REPAIRING_NAMESPACES, true);
		XMLEventWriter writer = factory.createXMLEventWriter(output);
		
		boolean result = run(eventReader, writer, context, controller);
		
		writer.close();
		
		return result;
	}
	
	// Shortcut methods for not setting controller.
	
	public boolean run(XMLEventReader eventReader, SinkManagerImpl sinkManager, GlobalContext context) throws XMLStreamException
	{
		return run(eventReader, sinkManager, context, null);
	}
	
	public boolean run(XMLEventReader eventReader, EventSink rootSink, GlobalContext context) throws XMLStreamException
	{
		return run(eventReader, rootSink, context, null);
	}
	
	public boolean run(XMLEventReader eventReader, XMLEventWriter writer, GlobalContext context) throws XMLStreamException
	{
		return run(eventReader, writer, context, null);
	}
	
	public boolean run(InputStream input, EventSink rootSink, GlobalContext context) throws XMLStreamException
	{
		return run(input, rootSink, context, null);
	}
	
	public boolean run(InputStream input, OutputStream output, GlobalContext context) throws XMLStreamException
	{
		return run(input, output, context, null);
	}
}
