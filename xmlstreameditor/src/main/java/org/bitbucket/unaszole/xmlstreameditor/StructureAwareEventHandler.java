package org.bitbucket.unaszole.xmlstreameditor;

import java.util.ArrayList;
import java.util.IdentityHashMap;
import java.util.List;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.bitbucket.unaszole.xmlstreameditor.xml.XMLElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
	Abstract class to simplify implementation of an EventHandler that is aware of the structure of the XML document.
	Provides the current element's location in the document structure - where "current element" is the element to which the XMLEvent belongs.
	@param <Scope> The contents of a scope for the specific implementation.
	@param <Context> The interface the context should implement in order for this handler to read from or write to it.
*/
public abstract class StructureAwareEventHandler<Scope extends ScopedEventHandler.BasicScope, GivenContext extends StructureAwareEventHandler.Context> extends ScopedEventHandler<Scope, GivenContext>
{
	private static final Logger LOGGER = LoggerFactory.getLogger(StructureAwareEventHandler.class);
	
	public static final class CurrentElementProvider
	{
		private XMLElement documentRootElement = null;
		
		private IdentityHashMap<XMLEvent, XMLElement> eventElementsCache = new IdentityHashMap<XMLEvent, XMLElement>();
		
		private static XMLElement searchForEventInElement(XMLEvent event, XMLElement element)
		{
			if((event.isStartElement() && element.getEvent() == event.asStartElement()) ||
				event.isEndElement() && element.getEndEvent() == event.asEndElement())
			{
				// Check if this event belongs to the given element, if so return it.
				return element;
			}
			
			for(XMLElement child : element.getChildren())
			{
				// Else look recursively for children.
				XMLElement searchResult = searchForEventInElement(event, child);
				if(searchResult != null)
				{
					return searchResult;
				}
			}
			return null;
		}
		
		private static XMLElement searchForDeepestUnclosedElement(XMLElement root)
		{
			if(root.getEndEvent() != null)
			{
				// If this element has been closed, nothing more to search.
				return null;
			}
			
			int nbChildren = root.getChildren().size();
			if(nbChildren > 0)
			{
				// Search recursively in last child for an unclosed element.
				XMLElement searchResult = searchForDeepestUnclosedElement(root.getChildren().get(nbChildren - 1));
				if(searchResult != null)
				{
					// Last child found an unclosed child. Return it up !
					return searchResult;
				}
				// If there is no unclosed child, current element is the opened one.
				return root;
			}
			// Unclosed with no child at all : current element is the opened one.
			return root;
		}
		
		public XMLElement getDocumentRootElement()
		{
			return this.documentRootElement;
		}
		
		public XMLElement getCurrentElement(XMLEvent event)
		{
			if(!(event.isEndElement() || event.isStartElement()))
			{
				// Only start and end elements are handled for now, others are unbound.
				return null;
			}
			
			if(this.documentRootElement == null)
			{
				// If there is no document root, new event is starting new root element.
				this.documentRootElement = StructureEditingEventHandler.getElementLocator(null, event.asStartElement());
				return this.documentRootElement;
			}
			
			// Search for event in cache of resolved elements.
			XMLElement cacheResult = eventElementsCache.get(event);
			if(cacheResult != null)
			{
				LOGGER.trace("Event at {} resolved to {} from cache.", event.getLocation(), cacheResult);
				return cacheResult;
			}
			
			// Search for event in all known elements.
			XMLElement searchResult = searchForEventInElement(event, documentRootElement);
			if(searchResult != null)
			{
				// Cache it before returning
				eventElementsCache.put(event, searchResult);
				LOGGER.trace("Event at {} resolved to {} from search.", event.getLocation(), searchResult);
				return searchResult;
			}
			
			// If event not found so far, it means we are reading at the end of the document. Find last element opened and not closed.
			XMLElement currentUnclosedElement = searchForDeepestUnclosedElement(documentRootElement);
			if(currentUnclosedElement == null)
			{
				// If no current unclosed element, it means document is fully closed. No current element.
				return null;
			}
			
			if(event.isStartElement())
			{
				// If starting an element, it's the last child of the currently unclosed element.
				XMLElement newElement = StructureEditingEventHandler.getElementLocator(currentUnclosedElement, event.asStartElement());
				eventElementsCache.put(event, newElement);
				LOGGER.trace("Event at {} opens new element {}.", event.getLocation(), searchResult);
				return newElement;
			}
			else if(event.isEndElement())
			{
				// If ending an element, it's the currently unclosed one. Register its end event.
				StructureEditingEventHandler.registerElementLocatorEnd(currentUnclosedElement, event.asEndElement());
				LOGGER.trace("Event at {} closes current unclosed element {}.", event.getLocation(), currentUnclosedElement);
				return currentUnclosedElement;
			}
			
			// This should never be reached...
			return null;
		}
	}
	
	public interface Context
	{
		/**
			@return The tool to compute current element locator based on the event being parsed.
				It MUST be single instance of provider for the whole run of the XMLStreamEditor;
				this single instance will ensure that the generated ElementLocator are shared among all StructureAwareEventHandler implementations.
		*/
		CurrentElementProvider getEltProvider();
	}
	
	/**
		Check if the provided event should open a new scope, and create it if needed.
		@param event The current event.
		@param currentScope The current scope. Could be null if we are not in an open scope.
		@param context The context of the document currently being edited.
		@param currentElementLocator a description of the current element's position in document structure.
			If event is a StartElement tag, currentElementLocator corresponds to the position of this newly opened element.
		@return The new scope to open, or null if not needed.
	*/
	protected abstract Scope openScopeIfNeeded(XMLEvent event, Scope currentScope, GivenContext context, XMLElement currentElementLocator);
	
	@Override
	protected final Scope openScopeIfNeeded(XMLEvent event, Scope currentScope, GivenContext context)
	{
		XMLElement currentElement = context.getEltProvider().getCurrentElement(event);
		return openScopeIfNeeded(event, currentScope, context, currentElement);
	}

	/**
		Handle an event.
		@param event The current event.
		@param currentScope The current scope. Could be null if we are not in an open scope.
		@param context The context of the document currently being edited.
		@param currentElementLocator a description of the current element's position in document structure.
		@return The list of events to replace the current event. Null if the event should be left as-is.
	*/
	protected abstract List<XMLEvent> handleEventInScope(XMLEvent event, Scope currentScope, GivenContext context, XMLElement currentElementLocator);
	
	@Override
	protected final List<XMLEvent> handleEventInScope(XMLEvent event, Scope currentScope, GivenContext context)
	{
		XMLElement currentElement = context.getEltProvider().getCurrentElement(event);
		return handleEventInScope(event, currentScope, context, currentElement);
	}
	
	/**
		Check if the provided event should close the current scope.
		@param event The current event.
		@param currentScope The current scope.
		@param context The context of the document currently being edited.
		@param currentElementLocator A description of the current element's position in document structure.
		@return True if current scope should be closed, false if it should be kept opened.
	*/
	protected abstract boolean isClosingScope(XMLEvent event, Scope currentScope, GivenContext context, XMLElement currentElementLocator);
	
	@Override
	protected final boolean isClosingScope(XMLEvent event, Scope currentScope, GivenContext context)
	{
		XMLElement currentElement = context.getEltProvider().getCurrentElement(event);
		return isClosingScope(event, currentScope, context, currentElement);
	}
	
	/**
	 * Default implementation, which purpose is simply to ensure that the whole structure of the document is read and available in the context at the end.
	 */
	public static final class StructureCollectingHandler extends StructureAwareEventHandler<ScopedEventHandler.BasicScope, StructureAwareEventHandler.Context>
	{
		@Override
		protected final ScopedEventHandler.BasicScope openScopeIfNeeded(XMLEvent event, ScopedEventHandler.BasicScope currentScope, StructureAwareEventHandler.Context context, XMLElement currentElementLocator)
		{
			return null;
		}
		
		@Override
		protected final List<XMLEvent> handleEventInScope(XMLEvent event, ScopedEventHandler.BasicScope currentScope, StructureAwareEventHandler.Context context, XMLElement currentElementLocator)
		{
			return null;
		}
		
		@Override
		protected final boolean isClosingScope(XMLEvent event, ScopedEventHandler.BasicScope currentScope, StructureAwareEventHandler.Context context, XMLElement currentElementLocator)
		{
			return false;
		}
		
		@Override
		protected final List<XMLEvent> closeScope(List<XMLEvent> events, ScopedEventHandler.BasicScope closedScope, StructureAwareEventHandler.Context context)
		{
			return events;
		}
	}
}