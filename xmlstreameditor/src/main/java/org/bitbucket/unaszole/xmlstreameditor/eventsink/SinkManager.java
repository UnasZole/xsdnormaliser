package org.bitbucket.unaszole.xmlstreameditor.eventsink;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;
import org.bitbucket.unaszole.xmlstreameditor.XMLEventHandler;

/**
	Maintains a stack of EventSink, each associated with an owning XMLEventHandler.
*/
public interface SinkManager
{
	/**
		Push a new buffered sink, belonging to the specified owner, on top of the stack.
		All future events will go to this sink.
		@param owner The owner of the new sink. Not null.
	*/
	public void startBuffer(XMLEventHandler owner);
	
	/**
		Check if a given handler can flush the top sink, ie. if it is owner of that sink.
		@param owner The handler to check. Not null.
		@return true if top sink belongs to given handler and can be flushed, false otherwise.
	*/
	public boolean canFlush(XMLEventHandler owner);
	
	public interface EventListTransformer
	{
		public List<XMLEvent> transform(List<XMLEvent> events);
	}
	
	/**
		Flush the current top sink and write all corresponding events to the next top sink, applying a transformation if needed.
		@param owner The owner triggering the flush. Not null.
		@param transformer An operator to transform the list of events.
	*/
	public void flushAndWrite(XMLEventHandler owner, EventListTransformer transformer) throws XMLStreamException;
}