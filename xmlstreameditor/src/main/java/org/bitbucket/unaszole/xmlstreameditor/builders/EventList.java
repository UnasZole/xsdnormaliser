package org.bitbucket.unaszole.xmlstreameditor.builders;

import java.util.ArrayList;
import java.util.List;
import javax.xml.stream.events.XMLEvent;

public class EventList
{
	public static List<XMLEvent> from(XMLEvent... events)
	{
		List<XMLEvent> list = new ArrayList<XMLEvent>();
		for(XMLEvent event : events)
		{
			list.add(event);
		}
		
		return list;
	}
}