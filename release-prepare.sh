#! /bin/sh

# -----------------------------------------------
# Release script for continuous integration
# with Maven and Git.
# Developed by Arnaud Vié (unas.zole@gmail.com)
#
# Published under The Apache License, Version 2.0
#   http://www.apache.org/licenses/LICENSE-2.0
# -----------------------------------------------


echoDryRun () {
	printf "Would call : $1"
	shift
	for param in "$@"
	do
		printf " \"$param\""
	done
	printf "\n"
}

# ==========================
# == PARSE SCRIPT OPTIONS ==
# ==========================

OPTS=`getopt -o u:p:b: -l git-user:,git-pwd:,version-pattern:,tag-pattern:,branch:,branching-pattern:,dry-run -- "$@"`
if [ $? != 0 ] ; then echo "Failed parsing options." >&2 ; exit 1 ; fi
eval set -- "$OPTS"

# Git credentials to push release tag.
GIT_USER="git"
GIT_PWD=""
# Patterns used to match and generate version and tag.
VERSION_PATTERN=""
TAG_PATTERN="{VERSION}"
BRANCH="" # NOT IMPLEMENTED YET
BRANCHING_PATTERN="release/*:{POM_VERSION}.{BUILD},*:{BRANCH}-{POM_VERSION}.{BUILD}" # NOT IMPLEMENTED YET
# Simple dry-run mechanism : prepend "echo" to commands in case of dry run. Not perfect (doesn't display quoted arguments properly).
_RUN=""

while true; do
	case "$1" in
		-u | --git-user ) GIT_USER="$2"; shift 2 ;;
		-p | --git-pwd ) GIT_PWD="$2"; shift 2 ;;
		--version-pattern ) VERSION_PATTERN="$2"; shift 2 ;;
		--tag-pattern ) TAG_PATTERN="$2"; shift 2 ;;
		-b | --branch ) BRANCH="$2"; shift 2 ;;
		--branching-pattern ) BRANCHING_PATTERN="$2"; shift 2 ;;
		--dry-run ) _RUN="echoDryRun"; shift 1 ;;
		-- ) shift; break ;;
		* ) echo "Internal error when parsing arguments." ; exit 1 ;;
	esac
done

# ===================
# == TODO : determine the VERSION_PATTERN from the branch and the BRANCHING_PATTERN, if not provided explicitly.
# ===================

if [ "$VERSION_PATTERN" = "" ]; then
	echo "No version pattern given. Stopping process."
	exit 1
fi

# ===================
# == READ POM DATA ==
# ===================

# Read the artifactId, groupId and version from the POM.
pomWithoutParentData=`sed '/<parent>/,/<\\/parent>/d' pom.xml`
GROUP_ID=`echo "$pomWithoutParentData" | sed -n 's#.*<groupId>\(.*\)</groupId>.*#\1#p' | head -n 1`
ARTIFACT_ID=`echo "$pomWithoutParentData" | sed -n 's#.*<artifactId>\(.*\)</artifactId>.*#\1#p' | head -n 1`
VERSION=`echo "$pomWithoutParentData" | sed -n 's#.*<version>\(.*\)-SNAPSHOT</version>.*#\1#p' | head -n 1`
POM_VERSION=`echo "$VERSION" | sed 's#-SNAPSHOT$##'` # Remove the -SNAPSHOT suffix to get non-snapshot version.
POM_GIT_URL=`echo "$pomWithoutParentData" | sed -n 's#.*<developerConnection>scm:git:\(.*\)</developerConnection>.*#\1#p' | head -n 1`


# ===================
# == CONFIGURE GIT ==
# ===================

# Commit author setting
$_RUN git config user.name "$GIT_USER (Automatic project release)"
$_RUN git config user.email "$GIT_USER@automatic-project.release"

# Git remote URL setup
if [ "$GIT_PWD" = "" ]; then
	# Git password is not provided : attempt an SSH connection, providing username (User name defaults to "git" in URL if not explicitly provided).
	GIT_URL=`echo "$POM_GIT_URL" | sed 's#^[^@]*@\([^:/]*\)[:/]#'"$GIT_USER"'@\1:#g'`
else
	# Git password provided : attempt an HTTPS connection.
	GIT_URL=`echo "$POM_GIT_URL" | sed 's#^[^@]*@\([^:/]*\)[:/]#https://'"$GIT_USER:$GIT_PWD"'@\1/#g'`
fi


# ==================================
# == BUILD THE EFFECTIVE PATTERNS ==
# ==================================

# Effective version pattern : substitute all placeholders other than {BUILD} in VERSION_PATTERN by their actual value.
EFFECTIVE_VERSION_PATTERN="$VERSION_PATTERN"
EFFECTIVE_VERSION_PATTERN=`echo "$EFFECTIVE_VERSION_PATTERN" | sed 's#{POM_VERSION}#'"$POM_VERSION"'#g'`

# Effective tag pattern : susbtitute all placeholders other than {BUILD} in TAG_PATTERN by their actual value.
# Including substitution of {VERSION} with the EFFECTIVE_VERSION_PATTERN
EFFECTIVE_TAG_PATTERN="$TAG_PATTERN"
EFFECTIVE_TAG_PATTERN=`echo "$EFFECTIVE_TAG_PATTERN" | sed 's#{POM_VERSION}#'"$POM_VERSION"'#g'`
EFFECTIVE_TAG_PATTERN=`echo "$EFFECTIVE_TAG_PATTERN" | sed 's#{VERSION}#'"$EFFECTIVE_VERSION_PATTERN"'#g'`


# ============================================
# == FIND THE LATEST PUBLISHED BUILD NUMBER ==
# ============================================

HIGHEST_BUILD_NB=-1

compute_highest_buildnb_from () {
	buildnb_sources=$1 # $1 : List of source strings
	buildnb_pattern="$2" # $2 : Pattern to find the build number in the source strings. Only {BUILD} placeholder should be present.
	# Return : none. Acts by side-effect, by writing to HIGHEST_BUILD_NB variable.
	
	buildnb_match_pattern="^"`echo "$buildnb_pattern" | sed 's#{BUILD}#[0-9]+#g'`"$"
	buildnb_capture_pattern="^"`echo "$buildnb_pattern" | sed 's#{BUILD}#\\\([0-9]\\\+\\\)#g'`"$"
	
	#echo "Searching from versions with $buildnb_pattern among $buildnb_sources"
	
	for source in $buildnb_sources
	do
		if echo "$source" | grep -Eq "$buildnb_match_pattern"; then
			
			captured_buildnb=`echo "$source" | sed -n 's#'"$buildnb_capture_pattern"'#\1#p'`
			
			echo "Found build number" $captured_buildnb "from" $source "with" $buildnb_pattern
			
			if [ $captured_buildnb -gt $HIGHEST_BUILD_NB ]; then
				HIGHEST_BUILD_NB=$captured_buildnb
			fi
		fi
	done
}

# == Search on Maven Central ==
echo "Searching build numbers on maven central..."

# Substitute build number placeholder with a wildcard for searching
search_pattern=`echo "$EFFECTIVE_VERSION_PATTERN" | sed 's#{BUILD}#*#g'`
search_url="http://search.maven.org/solrsearch/select?core=gav&wt=json&q=g:$GROUP_ID+AND+a:$ARTIFACT_ID+v:$EFFECTIVE_VERSION_PATTERN"
search_result=`curl -L "$search_url"`

if echo "$search_result" | grep -vEq '"numFound":0,'; then
	# If some artifacts have been found, review all versions. (else, latest build number stays at -1 : we'll generate number 0)
	
	found_versions=`echo $search_result | sed 's#"v":#\n"v":#g' | sed -n 's#.*"v":"\([^"]*\)".*#\1#p'`
	compute_highest_buildnb_from "$found_versions" "$EFFECTIVE_VERSION_PATTERN"
fi

# == Search from git tags ==
echo "Searching build numbers in remote git tags..."
found_tags=`git ls-remote --tags "$GIT_URL" | sed -n 's#.*refs/tags/\(.*\)$#\1#p'`
compute_highest_buildnb_from "$found_tags" "$EFFECTIVE_TAG_PATTERN"


echo "Highest build number found :" $HIGHEST_BUILD_NB


# ===========================================
# == SET POM VERSION TO A NEW BUILD NUMBER ==
# ===========================================

new_build_nb=`expr $HIGHEST_BUILD_NB + 1`
NEW_VERSION=`echo "$EFFECTIVE_VERSION_PATTERN" | sed 's#{BUILD}#'$new_build_nb'#g'`

echo "New version :" $NEW_VERSION

$_RUN mvn versions:set -DnewVersion="$NEW_VERSION"


# ======================================
# == COMMIT, TAG AND PUSH NEW RELEASE ==
# ======================================

# Add all poms to commit, and commit
poms_to_commit=`find . -name 'pom.xml'`
$_RUN git add $poms_to_commit
$_RUN git commit -m "[skip ci] Update POM versions for release $NEW_VERSION"

# Create the tag and push it.
NEW_TAG=`echo "$EFFECTIVE_TAG_PATTERN" | sed 's#{BUILD}#'$new_build_nb'#g'`
$_RUN git tag "$NEW_TAG"
$_RUN git push "$GIT_URL" "refs/tags/$NEW_TAG"

