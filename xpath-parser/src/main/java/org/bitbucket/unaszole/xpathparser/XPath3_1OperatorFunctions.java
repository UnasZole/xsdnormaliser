package org.bitbucket.unaszole.xpathparser;

import java.util.List;

/**
 * Class centralising the evaluation of operators following the mapping at https://www.w3.org/TR/xpath-31/#dt-operator-function
 * and following function definitions at https://www.w3.org/TR/xpath-functions-31/
 *
 * TODO : implement more operators and more strictly adhering to spec !
 */
public class XPath3_1OperatorFunctions
{
	public static class AtomicValue
	{
		public static enum AtomicValueType { BOOLEAN, INTEGER, STRING, SEQUENCE }
		
		private AtomicValueType type;
		
		private Object value;
		
		public AtomicValue(Boolean givenValue)
		{
			type = AtomicValueType.BOOLEAN;
			value = givenValue;
		}
		
		public AtomicValue(Integer givenValue)
		{
			type = AtomicValueType.INTEGER;
			value = givenValue;
		}
		
		public AtomicValue(String givenValue)
		{
			type = AtomicValueType.STRING;
			value = givenValue;
		}
		
		public AtomicValue(List<AtomicValue> givenValue)
		{
			type = AtomicValueType.SEQUENCE;
			value = givenValue;
		}
		
		public Boolean getBooleanValue()
		{
			return type == AtomicValueType.BOOLEAN ? (Boolean) value : null;
		}
		
		public Integer getIntegerValue()
		{
			return type == AtomicValueType.INTEGER ? (Integer) value : null;
		}
		
		public String getStringValue()
		{
			return type == AtomicValueType.STRING ? (String) value : null;
		}
		
		public List<AtomicValue> getSequenceValue()
		{
			return type == AtomicValueType.SEQUENCE ? (List<AtomicValue>) value : null;
		}
	}
	
	public static boolean evaluateEq(AtomicValue leftVal, AtomicValue rightVal)
	{
		if(leftVal.getIntegerValue() != null && rightVal.getIntegerValue() != null)
		{
			return leftVal.getIntegerValue() == rightVal.getIntegerValue();
		}
		else if(leftVal.getStringValue()!= null && rightVal.getStringValue() != null)
		{
			return leftVal.getStringValue().equals(rightVal.getStringValue());
		}
		
		throw new IllegalStateException("Eq received an unsupported value pair : " + leftVal + " and " + rightVal);
	}
	
	public static boolean evaluateGt(AtomicValue leftVal, AtomicValue rightVal)
	{
		if(leftVal.getIntegerValue() != null && rightVal.getIntegerValue() != null)
		{
			return leftVal.getIntegerValue() > rightVal.getIntegerValue();
		}
		
		throw new IllegalStateException("Gt received an unsupported value pair : " + leftVal + " and " + rightVal);
	}
	
	public static boolean evaluateLt(AtomicValue leftVal, AtomicValue rightVal)
	{
		if(leftVal.getIntegerValue() != null && rightVal.getIntegerValue() != null)
		{
			return leftVal.getIntegerValue() < rightVal.getIntegerValue();
		}
		
		throw new IllegalStateException("Lt received an unsupported value pair : " + leftVal + " and " + rightVal);
	}
	
	/**
	 * This method is an entrypoint that will select the appropriate operator function according to
	 * https://www.w3.org/TR/xpath-31/#dt-operator-function
	 * 
	 * @param leftVal The value of the left operand.
	 * @param operator The integer representing the operator literal in grammar.
	 * @param rightVal The value of the right operand.
	 * @return The result of the operation.
	 */
	public static AtomicValue evaluate(AtomicValue leftVal, int operator, AtomicValue rightVal)
	{
		switch(operator)
		{
			case XPath3_1Parser.EQ :
			case XPath3_1Parser.EQUALS :
				return new AtomicValue(evaluateEq(leftVal, rightVal));
			case XPath3_1Parser.NE :
			case XPath3_1Parser.NEQUAL :
				return new AtomicValue(!evaluateEq(leftVal, rightVal));
			case XPath3_1Parser.LT :
			case XPath3_1Parser.LESSTHAN :
				return new AtomicValue(evaluateGt(leftVal, rightVal));
			case XPath3_1Parser.LE :
			case XPath3_1Parser.LESSEQ :
				return new AtomicValue(!evaluateGt(leftVal, rightVal));
			case XPath3_1Parser.GT :
			case XPath3_1Parser.GREATER :
				return new AtomicValue(evaluateGt(leftVal, rightVal));
			case XPath3_1Parser.GE :
			case XPath3_1Parser.GREATEQ :
				return new AtomicValue(!evaluateLt(leftVal, rightVal));
			default:
				throw new IllegalStateException("Unknown value operator received : " + operator);
		}
	}
}
