package org.bitbucket.unaszole.xpathparser;

import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;
import org.antlr.v4.runtime.tree.RuleNode;

/**
 * Implement an XPath visitor with parameterised state.
 * Adapted from https://github.com/antlr/antlr4/issues/641#issuecomment-57070068
 *
 * @param <T> Type representing the evaluation of a node of the XPath syntax tree.
 * @param <S> Context for evaluating a node of the XPath syntax tree.
 */
public abstract class ParameterizedXPath3_1Visitor<T, S> extends XPath3_1BaseVisitor<T> {
    private final S state;

    public ParameterizedXPath3_1Visitor(S state) {
        this.state = state;
    }

    public T visit(ParseTree tree, S state) {
        ParseTreeVisitor<T> targetVisitor = this;
        if (this.state != state) {
            targetVisitor = createVisitor(state);
        }

        return tree.accept(targetVisitor);
    }

    public T visitChildren(@NotNull RuleNode node, S state) {
        ParseTreeVisitor<T> targetVisitor = this;
        if (this.state != state) {
            targetVisitor = createVisitor(state);
        }

        return targetVisitor.visitChildren(node);
    }

    public final S getState() {
        return state;
    }

    @NotNull
    protected abstract ParseTreeVisitor<T> createVisitor(S state);
}
