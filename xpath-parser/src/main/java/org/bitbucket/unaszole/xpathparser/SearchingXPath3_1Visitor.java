package org.bitbucket.unaszole.xpathparser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.bitbucket.unaszole.xpathparser.EvaluatingXPath3_1Visitor.Evaluation;

public abstract class SearchingXPath3_1Visitor<XMLNode> extends EvaluatingXPath3_1Visitor<XMLNode>
{
	public SearchingXPath3_1Visitor(EvaluationContext<XMLNode> state)
	{
		super(state);
	}
	
	/**
	 * State input : context node.
	 * @return List of matching nodes.
	 */
	@Override
	public Evaluation<XMLNode> visitPathExpr(XPath3_1Parser.PathExprContext ctx)
	{
		if(ctx.children.get(0) instanceof TerminalNode)
		{
			// Absolute path : "context node" for beginning of search is the root of document containing current context node.
			XMLNode rootNode = getDocumentRoot(getState().getContextNode());
			int operator = ((TerminalNode)ctx.children.get(0)).getSymbol().getType();
			
			if(ctx.relativePathExpr() != null)
			{
				// If a relative path is given after the root, resolve it in the path eval context.
				if(operator == XPath3_1Parser.ABRPATH)
				{
					throw new UnsupportedOperationException("// operation is not supported yet.");
				}
				else
				{
					return visit(ctx.relativePathExpr(), new EvaluationContext<XMLNode>(rootNode));
				}
			}
			else
			{
				// Else, evaluate to the root node itself.
				// TODO : Fix that, not ok in case of boolean evaluation mode...
				return new Evaluation<XMLNode>(rootNode);
			}
		}
		else
		{
			// Relative path : resolve the relative path in the current context.
			return visitRelativePathExpr(ctx.relativePathExpr());
		}
	}
	
	/**
	 * State input : path root node.
	 * @return List of matching nodes.
	 */
	@Override
	public Evaluation<XMLNode> visitRelativePathExpr(XPath3_1Parser.RelativePathExprContext ctx)
	{
		if(ctx.stepExpr().size() == 1)
		{
			// Only one child expression, return its evaluation as-is.
			return visitStepExpr(ctx.stepExpr(0));
		}
		
		int currentOp = XPath3_1Parser.PATHSEP;
		
		// Matching nodes before first step : only the path root node.
		List<XMLNode> stepMatchingNodes = Arrays.asList(getState().getContextNode());
		
		for(ParseTree child : ctx.children)
		{
			if(child instanceof TerminalNode)
			{
				// Child is terminal node : operator.
				currentOp = ((TerminalNode)child).getSymbol().getType();
			}
			else
			{
				// Child is step expression.
				if(currentOp == XPath3_1Parser.ABRPATH)
				{
					throw new UnsupportedOperationException("// operation is not supported yet.");
				}
				else
				{
					List<XMLNode> nextStepMatchingNodes = new ArrayList<XMLNode>();
					
					// For each of the current step matching nodes, get the list of next step matching nodes and append it.
					for(XMLNode node : stepMatchingNodes)
					{
						nextStepMatchingNodes.addAll(getValueAsNodeList(visit(child, new EvaluationContext<XMLNode>(node))));
					}
					
					stepMatchingNodes = nextStepMatchingNodes;
				}
			}
		}
		return getNodeListAsValue(stepMatchingNodes);
	}
	
	/**
	 * State input : Previous node
	 * @return List of matching following nodes or postfix expression value.
	 */
	@Override
	public Evaluation<XMLNode> visitStepExpr(XPath3_1Parser.StepExprContext ctx)
	{
		if(ctx.axisStep() != null)
		{
			return visitAxisStep(ctx.axisStep());
		}
		return visitPostfixExpr(ctx.postfixExpr());
	}
	
	/**
	 * State input : Previous node
	 * @return List of matching following nodes.
	 */
	@Override
	public Evaluation<XMLNode> visitAxisStep(XPath3_1Parser.AxisStepContext ctx)
	{
		// Get list of nodes matching ReverseStep or ForwardStep, using previous node as context.
		List<XMLNode> followingNodes;
		boolean forwardAxis;
		if(ctx.forwardStep() != null)
		{
			forwardAxis = true;
			followingNodes = getValueAsNodeList(visit(ctx.forwardStep()));
		}
		else
		{
			forwardAxis = false;
			followingNodes = getValueAsNodeList(visit(ctx.reverseStep()));
		}
		
		// Then use this list as context (+ the information that we are in a reverse or forward step,
		// useful for evaluating "context position" predicate) for evaluating predicateList.
		return visit(ctx.predicateList(), new EvaluationContext<XMLNode>(followingNodes, forwardAxis));
	}
	
	private List<XMLNode> evaluateStep(int axis, XPath3_1Parser.NodeTestContext nodeTest)
	{
		// Evaluate the given axis from context node to get following nodes.
		List<XMLNode> followingNodes = evaluateAxis(axis, getState().getContextNode());
		
		// Then for each following node, use it as context to match the NodeTest, and keep it only if result is true.
		List<XMLNode> matchingFollowingNodes = new ArrayList<XMLNode>();
		for(XMLNode followingNode : followingNodes)
		{
			if(visit(nodeTest, new EvaluationContext<XMLNode>(followingNode)).getBooleanValue())
			{
				matchingFollowingNodes.add(followingNode);
			}
		}
		
		return matchingFollowingNodes;
	}
	
	/**
	 * State input : Previous node
	 * @return List of matching following nodes.
	 */
	@Override
	public Evaluation<XMLNode> visitReverseStep(XPath3_1Parser.ReverseStepContext ctx)
	{
		// Read the forward axis.
		int axis = -1;
		if(ctx.abbrevReverseStep() != null)
		{
			// Abbreviated step means parent axis.
			axis = XPath3_1Parser.PARENT;
		}
		else
		{
			// Full step : read axis from the first literal of the reverseAxis.
			axis = ((TerminalNode)(ctx.reverseAxis().getChild(0))).getSymbol().getType();
		}
		
		// Evaluate step with read axis and node test.
		return getNodeListAsValue(evaluateStep(axis, ctx.nodeTest()));
	}
	
	/**
	 * State input : Previous node
	 * @return List of matching following nodes.
	 */
	@Override
	public Evaluation<XMLNode> visitForwardStep(XPath3_1Parser.ForwardStepContext ctx)
	{
		// Read the forward axis.
		int axis = -1;
		XPath3_1Parser.NodeTestContext nodeTest;
		if(ctx.abbrevForwardStep() != null)
		{
			// Abbreviated step means child axis.
			ParseTree abbrevFirstToken = ctx.abbrevForwardStep().getChild(0);
			if(abbrevFirstToken instanceof TerminalNode && ((TerminalNode)abbrevFirstToken).getSymbol().getType() == XPath3_1Parser.AT)
			{
				axis = XPath3_1Parser.ATTRIB;
			}
			else
			{
				axis = XPath3_1Parser.CHILD;
			}
			nodeTest = ctx.abbrevForwardStep().nodeTest();
		}
		else
		{
			// Full step : read axis from the first literal of the forwardAxis.
			axis = ((TerminalNode)(ctx.forwardAxis().getChild(0))).getSymbol().getType();
			nodeTest = ctx.nodeTest();
		}
		
		// Evaluate step with read axis and node test.
		return getNodeListAsValue(evaluateStep(axis, nodeTest));
	}
	
	/**
	 * State input : List of nodes to filter + axis direction (forward or reverse)
	 * @return List of nodes that validate the predicates.
	 */
	@Override
	public Evaluation<XMLNode> visitPredicateList(XPath3_1Parser.PredicateListContext ctx)
	{
		List<XMLNode> currentMatchingNodes = getState().getContextNodes();
		for(XPath3_1Parser.PredicateContext predicate : ctx.predicate())
		{
			List<XMLNode> matchingNodesAfterPredicate = new ArrayList<XMLNode>();
			for(int i = 0; i < currentMatchingNodes.size(); i++)
			{
				XMLNode node = currentMatchingNodes.get(i);
				
				if(visit(predicate, new EvaluationContext<XMLNode>(node, 
						getState().getForwardAxis() ? i + 1 : currentMatchingNodes.size() - i)
						).getBooleanValue())
				{
					matchingNodesAfterPredicate.add(node);
				}
			}
			currentMatchingNodes = matchingNodesAfterPredicate;
		}
		return getNodeListAsValue(currentMatchingNodes);
	}

}
