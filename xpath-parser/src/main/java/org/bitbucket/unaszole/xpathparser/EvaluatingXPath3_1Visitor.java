package org.bitbucket.unaszole.xpathparser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.bitbucket.unaszole.xpathparser.EvaluatingXPath3_1Visitor.Evaluation;
import org.bitbucket.unaszole.xpathparser.XPath3_1OperatorFunctions.AtomicValue;

/**
 * Implement an XPath visitor to evaluate the XPath
 *
 * @param <XMLContext> Context for evaluating a node of the XPath syntax tree.
 * @param <XMLNode> The representation of a node of the XML document.
 */
public abstract class EvaluatingXPath3_1Visitor<XMLNode> 
	extends ParameterizedXPath3_1Visitor<EvaluatingXPath3_1Visitor.Evaluation<XMLNode>, EvaluatingXPath3_1Visitor.EvaluationContext<XMLNode>>
{
	/**
	 * Represent the evaluation of an XPath sub-expression.
	 *
	 * @param <XMLNode> The representation of a node of the XML document.
	 */
	public static class Evaluation<XMLNode>
	{
		public static enum XPathEvaluationType { ATOMIC, XML_NODE, SEQUENCE }
		
		private XPathEvaluationType type;
		
		private Object value;
		
		public Evaluation(List<Evaluation<XMLNode>> givenValue)
		{
			type = XPathEvaluationType.SEQUENCE;
			value = givenValue;
		}
		
		public Evaluation(XMLNode givenValue)
		{
			type = XPathEvaluationType.XML_NODE;
			value = givenValue;
		}
		
		public Evaluation(AtomicValue givenValue)
		{
			type = XPathEvaluationType.ATOMIC;
			value = givenValue;
		}
		
		public Evaluation(Boolean givenValue)
		{
			type = XPathEvaluationType.ATOMIC;
			value = new AtomicValue(givenValue);
		}
		
		public Evaluation(Integer givenValue)
		{
			type = XPathEvaluationType.ATOMIC;
			value = new AtomicValue(givenValue);
		}
		
		public Evaluation(String givenValue)
		{
			type = XPathEvaluationType.ATOMIC;
			value = new AtomicValue(givenValue);
		}
		
		public XMLNode getNodeValue()
		{
			return type == XPathEvaluationType.XML_NODE ? (XMLNode) value : null;
		}
		
		public List<Evaluation<XMLNode>> getSequenceValue()
		{
			return type == XPathEvaluationType.SEQUENCE ? (List<Evaluation<XMLNode>>) value : null;
		}
		
		public AtomicValue getAtomicValue()
		{
			return type == XPathEvaluationType.ATOMIC ? (AtomicValue) value : null;
		}
		
		public Boolean getBooleanValue()
		{
			return type == XPathEvaluationType.ATOMIC ? ((AtomicValue) value).getBooleanValue() : null;
		}
		
		public Integer getIntegerValue()
		{
			return type == XPathEvaluationType.ATOMIC ? ((AtomicValue) value).getIntegerValue() : null;
		}
	}
	
	/**
	 * Represent the context to evaluate an XPath sub-expression.
	 *
	 * @param <XMLNode> The representation of a node of the XML document.
	 */
	public static class EvaluationContext<XMLNode>
	{
		private XMLNode contextNode;
		
		private int contextNodePosition;
		
		private List<XMLNode> contextNodeList;
		
		private boolean forwardAxis;
		
		/**
		 * Usual context for working on a single node.
		 * @param contextNode Context node.
		 */
		public EvaluationContext(XMLNode contextNode)
		{
			this.contextNode = contextNode;
		}
		
		/**
		 * Context for evaluating a predicate list.
		 * @param contextNodeList List of nodes to test.
		 * @param forwardAxis True if current step is on a forward axis, False otherwise.
		 */
		public EvaluationContext(List<XMLNode> contextNodeList, boolean forwardAxis)
		{
			this.contextNodeList = contextNodeList;
			this.forwardAxis = forwardAxis;
		}
		
		/**
		 * Context for evaluating a predicate.
		 * @param contextNode Node to test.
		 * @param contextNodePosition Context position of the node.
		 */
		public EvaluationContext(XMLNode contextNode, int contextNodePosition)
		{
			this.contextNode = contextNode;
			this.contextNodePosition = contextNodePosition;
		}
		
		public XMLNode getContextNode()
		{
			return contextNode;
		}
		
		public List<XMLNode> getContextNodes()
		{
			return contextNodeList;
		}
		
		public int getContextNodePosition()
		{
			return contextNodePosition;
		}
		
		public boolean getForwardAxis()
		{
			return forwardAxis;
		}
	}
	
	protected abstract XMLNode getDocumentRoot(XMLNode ctx);
	
	protected abstract XMLNode getParent(XMLNode ctx);
	
	protected abstract List<XMLNode> getChildren(XMLNode ctx);
	
	protected abstract List<XMLNode> getAttributes(XMLNode ctx);
	
	protected abstract AtomicValue getNodeAtomicValue(XMLNode ctx);
	
	/**
	 * 
	 * @param ctx
	 * @return Empty string if no prefix.
	 */
	protected abstract String getNSPrefix(XMLNode ctx);
	
	/**
	 * 
	 * @param ctx
	 * @return Empty string if no namespace.
	 */
	protected abstract String getNSURI(XMLNode ctx);
	
	protected abstract String getLocalPart(XMLNode ctx);
	
	public EvaluatingXPath3_1Visitor(EvaluatingXPath3_1Visitor.EvaluationContext<XMLNode> state)
	{
		super(state);
	}
	
	protected List<XMLNode> getValueAsNodeList(Evaluation<XMLNode> value)
	{
		List<Evaluation<XMLNode>> sequence = value.getSequenceValue();
		if(sequence == null)
		{
			return null;
		}
		
		List<XMLNode> nodeList = new ArrayList<XMLNode>();
		for(Evaluation<XMLNode> evaluation : sequence)
		{
			nodeList.add(evaluation.getNodeValue());
		}
		return nodeList;
	}
	
	protected Evaluation<XMLNode> getNodeListAsValue(List<XMLNode> nodeList)
	{
		List<Evaluation<XMLNode>> sequence = new ArrayList<EvaluatingXPath3_1Visitor.Evaluation<XMLNode>>();
		for(XMLNode node : nodeList)
		{
			sequence.add(new Evaluation<XMLNode>(node));
		}
		return new Evaluation<XMLNode>(sequence);
	}
	
	protected List<Evaluation<XMLNode>> getValueAsList(Evaluation<XMLNode> value)
	{
		if(value.getSequenceValue() != null)
		{
			return value.getSequenceValue();
		}
		else
		{
			return Collections.singletonList(value);
		}
	}
	
	@Override
	public Evaluation<XMLNode> visitOrExpr(XPath3_1Parser.OrExprContext ctx)
	{
		if(ctx.andExpr().size() == 1)
		{
			// Only one child expression, return its evaluation as-is.
			return visitAndExpr(ctx.andExpr(0));
		}
		
		for(XPath3_1Parser.AndExprContext child : ctx.andExpr())
		{
			Evaluation<XMLNode> subTreeEval = visit(child);
			
			if(subTreeEval.getBooleanValue())
			{
				// If a subtree was evaluated to true, Or expression returns true immediately.
				return new Evaluation<XMLNode>(Boolean.TRUE);
			}
		}
		
		// If no subtree evaluated to true, return false.
		return new Evaluation<XMLNode>(Boolean.FALSE);
	}
	
	@Override
	public Evaluation<XMLNode> visitAndExpr(XPath3_1Parser.AndExprContext ctx)
	{
		if(ctx.comparisonExpr().size() == 1)
		{
			// Only one child expression, return its evaluation as-is.
			return visitComparisonExpr(ctx.comparisonExpr(0));
		}
		
		for(XPath3_1Parser.ComparisonExprContext child : ctx.comparisonExpr())
		{
			Evaluation<XMLNode> subTreeEval = visit(child);
			
			if(!subTreeEval.getBooleanValue())
			{
				// If a subtree was evaluated to false, Or expression returns false immediately.
				return new Evaluation<XMLNode>(Boolean.FALSE);
			}
		}
		
		// If no subtree evaluated to false, return true.
		return new Evaluation<XMLNode>(Boolean.TRUE);
	}
	
	/**
	 * 
	 * @param value Any value.
	 * @return Atomic value computed according to https://www.w3.org/TR/xpath-31/#dt-atomization
	 */
	private AtomicValue atomise(Evaluation<XMLNode> value)
	{
		if(value.getAtomicValue() != null)
		{
			return value.getAtomicValue();
		}
		else if(value.getNodeValue() != null)
		{
			return getNodeAtomicValue(value.getNodeValue());
		}
		else if(value.getSequenceValue() != null)
		{
			List<AtomicValue> atomicValueSequence = new ArrayList<AtomicValue>();
			for(Evaluation<XMLNode> sequenceItem : value.getSequenceValue())
			{
				AtomicValue itemAtomicValue = atomise(sequenceItem);
				if(itemAtomicValue.getSequenceValue() != null)
				{
					// Atomisation flattens sequences : add all subvalues instead of adding the sequence itself.
					atomicValueSequence.addAll(itemAtomicValue.getSequenceValue());
				}
				else
				{
					atomicValueSequence.add(itemAtomicValue);
				}
			}
			return new AtomicValue(atomicValueSequence);
		}
		
		throw new IllegalStateException("Unhandled value to atomise " + value);
	}
	
	private boolean compareValues(Evaluation<XMLNode> leftVal, int operator, Evaluation<XMLNode> rightVal)
	{
		return XPath3_1OperatorFunctions.evaluate(atomise(leftVal), operator, atomise(rightVal)).getBooleanValue();
	}
	
	@Override
	public Evaluation<XMLNode> visitComparisonExpr(XPath3_1Parser.ComparisonExprContext ctx)
	{
		if(ctx.stringConcatExpr().size() == 1)
		{
			// Only one child expression, return its evaluation as-is.
			return visitStringConcatExpr(ctx.stringConcatExpr(0));
		}
		
		// If two elements to compare, perform comparison.
		
		// Evaluate the two elements.
		Evaluation<XMLNode> leftVal = visitStringConcatExpr(ctx.stringConcatExpr(0));
		Evaluation<XMLNode> rightVal = visitStringConcatExpr(ctx.stringConcatExpr(1));
		
		// Determine the comparator.
		if(ctx.valueComp() != null)
		{
			// https://www.w3.org/TR/xpath-31/#id-value-comparisons
			return new Evaluation<XMLNode>(compareValues(leftVal, ((TerminalNode)ctx.valueComp().getChild(0)).getSymbol().getType(), rightVal));
		}
		else if(ctx.generalComp() != null)
		{
			// https://www.w3.org/TR/xpath-31/#id-general-comparisons
			List<Evaluation<XMLNode>> leftValues = getValueAsList(leftVal);
			List<Evaluation<XMLNode>> rightValues = getValueAsList(rightVal);
			
			int operator = ((TerminalNode)ctx.generalComp().getChild(0)).getSymbol().getType();
			
			for(Evaluation<XMLNode> leftValue : leftValues)
			{
				for(Evaluation<XMLNode> rightValue : rightValues)
				{
					if(compareValues(leftValue, operator, rightValue))
					{
						return new Evaluation<XMLNode>(true);
					}
				}
			}
			
			return new Evaluation<XMLNode>(false);
		}
		else if(ctx.nodeComp() != null)
		{
			// TODO : implement as per https://www.w3.org/TR/xpath-31/#id-node-comparisons
		}
		
		throw new IllegalStateException("Unhandled comparison received at " + ctx.getText());
	}
	
	@Override
	public Evaluation<XMLNode> visitAdditiveExpr(XPath3_1Parser.AdditiveExprContext ctx)
	{
		if(ctx.multiplicativeExpr().size() == 1)
		{
			// Only one child expression, return its evaluation as-is.
			return visitMultiplicativeExpr(ctx.multiplicativeExpr(0));
		}
		
		// At least two expressions : compute the actual additive operations.
		int currentValue = 0;
		int currentOp = XPath3_1Parser.PLUS;
		
		for(ParseTree child : ctx.children)
		{
			if(child instanceof TerminalNode)
			{
				// Child is terminal node : operator.
				currentOp = ((TerminalNode)child).getSymbol().getType();
			}
			else
			{
				if(currentOp == XPath3_1Parser.PLUS)
				{
					currentValue += visit(child).getIntegerValue();
				}
				else
				{
					currentValue -= visit(child).getIntegerValue();
				}
			}
		}
		
		return new Evaluation<XMLNode>(currentValue);
	}
	
	protected final List<XMLNode> evaluateAxis(int axis, XMLNode contextNode)
	{
		switch(axis)
		{
			case XPath3_1Parser.CHILD:
				return getChildren(contextNode);
			
			case XPath3_1Parser.DESCND:
				List<XMLNode> descendants = new ArrayList<XMLNode>();
				for(XMLNode child : getChildren(contextNode))
				{
					descendants.addAll(evaluateAxis(XPath3_1Parser.DESELF, child));
				}
				return descendants;
			
			case XPath3_1Parser.SELF:
				return Arrays.asList(contextNode);
				
			case XPath3_1Parser.DESELF:
				List<XMLNode> descendantsOrSelf = new ArrayList<XMLNode>();
				descendantsOrSelf.add(contextNode);
				for(XMLNode child : getChildren(contextNode))
				{
					descendantsOrSelf.addAll(evaluateAxis(XPath3_1Parser.DESELF, child));
				}
				return descendantsOrSelf;
			
			case XPath3_1Parser.ATTRIB:
				return getAttributes(contextNode);
				
			// TODO : implement more axis definitions.
			default:
				throw new UnsupportedOperationException("Unsupported axis type " + axis);
		}
	}
	
	/**
	 * State input : Node to test
	 * @return Boolean : true if node matches, false otherwise.
	 */
	@Override
	public Evaluation<XMLNode> visitNodeTest(XPath3_1Parser.NodeTestContext ctx)
	{
		if(ctx.nameTest() != null)
		{
			return visitNameTest(ctx.nameTest());
		}
		// TODO : implement kindTest
		return null;
	}
	
	/**
	 * State input : Node to test
	 * @return Boolean : true if node matches, false otherwise.
	 */
	@Override
	public Evaluation<XMLNode> visitNameTest(XPath3_1Parser.NameTestContext ctx)
	{
		if(ctx.wildcard() != null)
		{
			// If wildcard, set fields as appropriate.
			return visitWildcard(ctx.wildcard());
		}
		else
		{
			// If explicit qualified name, read it.
			return visitEqName(ctx.eqName());
		}
	}
	
	private boolean evaluateNameTest(String nsPrefix, String nsURI, String localPart, XMLNode contextNode)
	{
		if(nsPrefix != null && !nsPrefix.equals(getNSPrefix(contextNode)))
		{
			return false;
		}
		if(nsURI != null && !nsURI.equals(getNSURI(contextNode)))
		{
			return false;
		}
		if(localPart != null && !localPart.equals(getLocalPart(contextNode)))
		{
			return false;
		}
		return true;
	}
	
	/**
	 * State input : Node to test
	 * @return Boolean : true if node matches, false otherwise.
	 */
	@Override
	public Evaluation<XMLNode> visitWildcard(XPath3_1Parser.WildcardContext ctx)
	{
		String nsPrefix;
		String nsURI;
		String localPart;
		
		TerminalNode firstTermNode = (TerminalNode)(ctx.getChild(0));
		switch(firstTermNode.getSymbol().getType())
		{
			case XPath3_1Parser.MUL:
				// Single wildcard : nothing specified.
				nsPrefix = null;
				nsURI = null;
				localPart = null;
				break;
			
			case XPath3_1Parser.COLWILD:
				// Name wildcard : NS prefix is specified.
				nsPrefix = ctx.NCName().getText();
				nsURI = null;
				localPart = null;
				break;
			
			case XPath3_1Parser.WILDCOL:
				// NS wildcard : name is specified.
				nsPrefix = null;
				nsURI = null;
				localPart = ctx.NCName().getText();
				break;
			
			case XPath3_1Parser.BracedURILiteral:
				// Name wildcard : NS URI is specified.
				nsPrefix = null;
				nsURI = ctx.BracedURILiteral().getText();
				localPart = null;
				break;
			
			default:
				throw new IllegalStateException("Wildcard field with unknown structure encountered : first terminal node is of type " + firstTermNode.getSymbol().getType());
		}
		return new Evaluation<XMLNode>(evaluateNameTest(nsPrefix, nsURI, localPart, getState().getContextNode()));
	}
	
	/**
	 * State input : None
	 * @return Value read from literal
	 */
	@Override
	public Evaluation<XMLNode> visitLiteral(XPath3_1Parser.LiteralContext ctx)
	{
		if(ctx.numericLiteral() != null)
		{
			return visitNumericLiteral(ctx.numericLiteral());
		}
		// If not a numeric literal, it's a string literal. Just exclude the quotes.
		String literal = ctx.getText();
		return new Evaluation<XMLNode>(literal.substring(1, literal.length() - 1));
	}
	
	/**
	 * State input : None
	 * @return Numeric value read from literal
	 */
	@Override
	public Evaluation<XMLNode> visitNumericLiteral(XPath3_1Parser.NumericLiteralContext ctx)
	{
		// TODO : support non-integer literals.
		return new Evaluation<XMLNode>(Integer.valueOf(ctx.getText()));
	}
	
	/**
	 * State input : Node to test
	 * @return Boolean : true if node matches, false otherwise.
	 */
	@Override
	public Evaluation<XMLNode> visitEqName(XPath3_1Parser.EqNameContext ctx)
	{
		String nsPrefix;
		String nsURI;
		String localPart;
		
		if(ctx.qName() != null)
		{
			nsURI = null;
			// qName with NS prefix.
			if(ctx.qName().prefixedName() != null)
			{
				// With an actual prefix set.
				nsPrefix = ctx.qName().prefixedName().prefix().NCName().getText();
				localPart = ctx.qName().prefixedName().localPart().NCName().getText();
			}
			else
			{
				// With no prefix : default NS.
				nsPrefix = "";
				localPart = ctx.qName().unprefixedName().localPart().NCName().getText();
			}
		}
		else
		{
			// uriQualifiedName with NS URI
			nsPrefix = null;
			nsURI = ctx.uriQualifiedName().BracedURILiteral().getText();
			localPart = ctx.uriQualifiedName().NCName().getText();
		}
		
		return new Evaluation<XMLNode>(evaluateNameTest(nsPrefix, nsURI, localPart, getState().getContextNode()));
	}
	
	/**
	 * State input : Input node + context position of input node.
	 * @return Boolean : Predicate truth value for the input node.
	 */
	@Override
	public Evaluation<XMLNode> visitPredicate(XPath3_1Parser.PredicateContext ctx)
	{
		Evaluation<XMLNode> predicateEvaluation = visitExpr(ctx.expr());
		if(predicateEvaluation.getIntegerValue() != null)
		{
			// If predicate evaluates to a single numeric value, it's to be matched to contextposition of the node.
			return new Evaluation<XMLNode>(predicateEvaluation.getIntegerValue() == getState().getContextNodePosition());
		}
		// Else, its effective boolean value is used.
		return predicateEvaluation;
	}
}
