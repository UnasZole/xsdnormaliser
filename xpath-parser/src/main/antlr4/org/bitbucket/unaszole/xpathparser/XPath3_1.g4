grammar XPath3_1;

/* ==================================
	XPath 3.1 grammar.
	Adapted by Arnaud Vié
	unas.zole+avie@gmail.com
================================== */

/*
	Main grammar
	From https://www.w3.org/TR/2017/REC-xpath-31-20170321/#id-grammar
*/

xPath : expr ;

paramList : param (',' param)* ;

param : '$' eqName typeDeclaration? ;

functionBody : enclosedExpr ;

enclosedExpr : '{' expr? '}' ;

expr : exprSingle (',' exprSingle)* ;

exprSingle : forExpr
| letExpr
| quantifiedExpr
| ifExpr
| orExpr ;

forExpr : simpleForClause 'return' exprSingle ;

simpleForClause : 'for' simpleForBinding (',' simpleForBinding)* ;

simpleForBinding : '$' varName 'in' exprSingle ;

letExpr : simpleLetClause 'return' exprSingle ;

simpleLetClause : 'let' simpleLetBinding (',' simpleLetBinding)* ;

simpleLetBinding : '$' varName ':=' exprSingle ;

quantifiedExpr : ('some' | 'every') '$' varName 'in' exprSingle (',' '$' varName 'in' exprSingle)* 'satisfies' exprSingle ;

ifExpr : 'if' '(' expr ')' 'then' exprSingle 'else' exprSingle ;

orExpr : andExpr ( 'or' andExpr )* ;

andExpr : comparisonExpr ( 'and' comparisonExpr )* ;

comparisonExpr : stringConcatExpr ( (valueComp
| generalComp
| nodeComp) stringConcatExpr )? ;

stringConcatExpr : rangeExpr ( '||' rangeExpr )* ;

rangeExpr : additiveExpr ( 'to' additiveExpr )? ;

additiveExpr : multiplicativeExpr ( ('+' | '-') multiplicativeExpr )* ;

multiplicativeExpr : unionExpr ( ('*' | 'div' | 'idiv' | 'mod') unionExpr )* ;

unionExpr : intersectExceptExpr ( ('union' | '|') intersectExceptExpr )* ;

intersectExceptExpr : instanceofExpr ( ('intersect' | 'except') instanceofExpr )* ;

instanceofExpr : treatExpr ( 'instance' 'of' sequenceType )? ;

treatExpr : castableExpr ( 'treat' 'as' sequenceType )? ;

castableExpr : castExpr ( 'castable' 'as' singleType )? ;

castExpr : arrowExpr ( 'cast' 'as' singleType )? ;

arrowExpr : unaryExpr ( '=>' arrowFunctionSpecifier argumentList )* ;

unaryExpr : ('-' | '+')* valueExpr ;

valueExpr : simpleMapExpr ;

generalComp : '=' | '!=' | '<' | '<=' | '>' | '>=' ;

valueComp : 'eq' | 'ne' | 'lt' | 'le' | 'gt' | 'ge' ;

nodeComp : 'is' | '<<' | '>>' ;

simpleMapExpr : pathExpr ('!' pathExpr)* ;

pathExpr : ('/' relativePathExpr?)
| ('//' relativePathExpr)
| relativePathExpr ; /* xgc: leading-lone-slash */

relativePathExpr : stepExpr (('/' | '//') stepExpr)* ;

stepExpr : postfixExpr | axisStep ;

axisStep : (reverseStep | forwardStep) predicateList ;

forwardStep : (forwardAxis nodeTest) | abbrevForwardStep ;

forwardAxis : ('child' '::')
| ('descendant' '::')
| ('attribute' '::')
| ('self' '::')
| ('descendant-or-self' '::')
| ('following-sibling' '::')
| ('following' '::')
| ('namespace' '::') ;

abbrevForwardStep : '@'? nodeTest ;

reverseStep : (reverseAxis nodeTest) | abbrevReverseStep ;

reverseAxis : ('parent' '::')
| ('ancestor' '::')
| ('preceding-sibling' '::')
| ('preceding' '::')
| ('ancestor-or-self' '::') ;

abbrevReverseStep : '..' ;

nodeTest : kindTest | nameTest ;

nameTest : eqName | wildcard ;

wildcard : '*'
| (NCName ':*')
| ('*:' NCName)
| (BracedURILiteral '*') ; /* ws: explicit */

postfixExpr : primaryExpr (predicate | argumentList | lookup)* ;

argumentList : '(' (argument (',' argument)*)? ')' ;

predicateList : predicate* ;

predicate : '[' expr ']' ;

lookup : '?' keySpecifier ;

keySpecifier : NCName | IntegerLiteral | parenthesizedExpr | '*' ;

arrowFunctionSpecifier : eqName | varRef | parenthesizedExpr ;

primaryExpr : literal
| varRef
| parenthesizedExpr
| contextItemExpr
| functionCall
| functionItemExpr
| mapConstructor
| arrayConstructor
| unaryLookup ;

literal : numericLiteral | StringLiteral ;

numericLiteral : IntegerLiteral | DecimalLiteral | DoubleLiteral ;

varRef : '$' varName ;

varName : eqName ;

parenthesizedExpr : '(' expr? ')' ;

contextItemExpr : '.' ;

functionCall : eqName argumentList ; /* xgc: reserved-function-names */

/* gn: parens */
argument : exprSingle | argumentPlaceholder ;

argumentPlaceholder : '?' ;

functionItemExpr : namedFunctionRef | inlineFunctionExpr ;

namedFunctionRef : eqName '#' IntegerLiteral ; /* xgc: reserved-function-names */

inlineFunctionExpr : 'function' '(' paramList? ')' ('as' sequenceType)? functionBody ;

mapConstructor : 'map' '{' (mapConstructorEntry (',' mapConstructorEntry)*)? '}' ;

mapConstructorEntry : mapKeyExpr ':' mapValueExpr ;

mapKeyExpr : exprSingle ;

mapValueExpr : exprSingle ;

arrayConstructor : squareArrayConstructor | curlyArrayConstructor ;

squareArrayConstructor : '[' (exprSingle (',' exprSingle)*)? ']' ;

curlyArrayConstructor : 'array' enclosedExpr ;

unaryLookup : '?' keySpecifier ;

singleType : simpleTypeName '?'? ;

typeDeclaration : 'as' sequenceType ;

sequenceType : ('empty-sequence' '(' ')')
| (itemType occurrenceIndicator?) ;

occurrenceIndicator : '?' | '*' | '+' ; /* xgc: occurrence-indicators */

itemType : kindTest | ('item' '(' ')') | functionTest | mapTest | arrayTest | atomicOrUnionType | parenthesizedItemType ;

atomicOrUnionType : eqName ;

kindTest : documentTest
| elementTest
| attributeTest
| schemaElementTest
| schemaAttributeTest
| piTest
| commentTest
| textTest
| namespaceNodeTest
| anyKindTest ;

anyKindTest : 'node' '(' ')' ;

documentTest : 'document-node' '(' (elementTest | schemaElementTest)? ')' ;

textTest : 'text' '(' ')' ;

commentTest : 'comment' '(' ')' ;

namespaceNodeTest : 'namespace-node' '(' ')' ;

piTest : 'processing-instruction' '(' (NCName | StringLiteral)? ')' ;

attributeTest : 'attribute' '(' (attribNameOrWildcard (',' typeName)?)? ')' ;

attribNameOrWildcard : attributeName | '*' ;

schemaAttributeTest : 'schema-attribute' '(' attributeDeclaration ')' ;

attributeDeclaration : attributeName ;

elementTest : 'element' '(' (elementNameOrWildcard (',' typeName '?'?)?)? ')' ;

elementNameOrWildcard : elementName | '*' ;

schemaElementTest : 'schema-element' '(' elementDeclaration ')' ;

elementDeclaration : elementName ;

attributeName : eqName ;

elementName : eqName ;

simpleTypeName : typeName ;

typeName : eqName ;

functionTest : anyFunctionTest
| typedFunctionTest ;

anyFunctionTest : 'function' '(' '*' ')' ;

typedFunctionTest : 'function' '(' (sequenceType (',' sequenceType)*)? ')' 'as' sequenceType ;

mapTest : anyMapTest | typedMapTest ;

anyMapTest : 'map' '(' '*' ')' ;

typedMapTest : 'map' '(' atomicOrUnionType ',' sequenceType ')' ;

arrayTest : anyArrayTest | typedArrayTest ;

anyArrayTest : 'array' '(' '*' ')' ;

typedArrayTest : 'array' '(' sequenceType ')' ;

parenthesizedItemType : '(' itemType ')' ;

eqName : qName | uriQualifiedName ;




/*
	Terminal Symbols
	From https://www.w3.org/TR/2017/REC-xpath-31-20170321/#terminal-symbols
*/

IntegerLiteral : Digits ;

DecimalLiteral : ('.' Digits) | (Digits '.' [0-9]*) ; /* ws: explicit */

DoubleLiteral : (('.' Digits) | (Digits ('.' [0-9]*)?)) [eE] [+-]? Digits ; /* ws: explicit */

StringLiteral : ('"' (EscapeQuot | ~'"')* '"') | ('\'' (EscapeApos | ~'\'')* '\'') ; /* ws: explicit */

uriQualifiedName : BracedURILiteral NCName ; /* ws: explicit */

BracedURILiteral : 'Q' '{' [^{}]* '}' ; /* ws: explicit */

fragment EscapeQuot : '""' ;

fragment EscapeApos : '\'\'' ;

comment : '(:' (CommentContents | Comment)* ':)' ; /* ws: explicit *//* gn: comments */
// ADAPTATION : Comment is actually a grammar rule, it must not be executed by the ANTLR lexer or it will grab too much text.

//qName : [http://www.w3.org/TR/REC-xml-names/#NT-qName]Names ; /* xgc: xml-version */
//NCName : [http://www.w3.org/TR/REC-xml-names/#NT-NCName]Names ; /* xgc: xml-version */
//Char : [http://www.w3.org/TR/REC-xml#NT-Char]XML ; /* xgc: xml-version */
// ADAPTATION : External references commented out and included in below sections.

fragment Digits : [0-9]+ ;

//CommentContents : (Char+ - (Char* ('(:' | ':)') Char*)) ;
// ADAPTATION : Rule replaced by simpler alternative because - is not supported in ANTLR
commentContents : Char+ ;
// ADAPTATION : CommentContents is actually a grammar rule, it must not be executed by the ANTLR lexer or it will grab too much text.




/*
	XML Symbols
	From https://www.w3.org/TR/REC-xml/#NT-Char
*/

fragment Char : '\u0009' | '\u000A' | '\u000D' | [\u0020-\uD7FF] | [\uE000-\uFFFD] | [\u10000-\u10FFFF] ; /* any Unicode character, excluding the surrogate blocks, FFFE, and FFFF. */

//NameStartChar : ':' | [A-Z] | '_' | [a-z] | [\u00C0-\u00D6] | [\u00D8-\u00F6] | [\u00F8-\u02FF] | [\u0370-\u037D] | [\u037F-\u1FFF] | [\u200C-\u200D] | [\u2070-\u218F] | [\u2C00-\u2FEF] | [\u3001-\uD7FF] | [\uF900-\uFDCF] | [\uFDF0-\uFFFD] | [\u10000-\uEFFFF] ;
// ADAPTATION : Rule rewritten to accomodate NCName change.
fragment NameStartChar : ':' | NCNameStartChar ;

fragment NameChar : NameStartChar | '-' | '.' | [0-9] | '\u00B7' | [\u0300-\u036F] | [\u203F-\u2040] ;

fragment Name : NameStartChar (NameChar)* ;




/*
	XML Namespace Symbols
	From https://www.w3.org/TR/REC-xml-names/#NT-NCName
*/

//NCName : Name - (Char* ':' Char*) ; /* An XML Name, minus the ":" */
// ADAPTATION : Rule rewritten to accomodate lack of support for - operator. Interval [\u10000-\uEFFFF] also removed as this range is not supported
// cf. https://stackoverflow.com/questions/35938284/how-do-i-specify-a-unicode-literal-that-requires-more-than-four-hex-digits-in-an
fragment NCNameStartChar : [A-Z] | '_' | [a-z] | [\u00C0-\u00D6] | [\u00D8-\u00F6] | [\u00F8-\u02FF] | [\u0370-\u037D] | [\u037F-\u1FFF] | [\u200C-\u200D] | [\u2070-\u218F] | [\u2C00-\u2FEF] | [\u3001-\uD7FF] | [\uF900-\uFDCF] | [\uFDF0-\uFFFD] ;
NCName : ( '-' | '.' | [0-9] | '\u00B7' | [\u0300-\u036F] | [\u203F-\u2040] | NCNameStartChar )+ ;

// ADAPTATION : Following rules written as grammar rules (not lexer) to avoid being overshadowed by NCName match.
qName : prefixedName
| unprefixedName ;

prefixedName : prefix ':' localPart ;

unprefixedName : localPart ;

prefix : NCName ;

localPart : NCName ;




/*
	Whitespace skip in lexer
*/

WS: [ \n\t\r]+ -> skip;




/*
	Give names to literals that we will have to distinguish.
	(ie used in grammar with | alternative)
*/

SOME :		'some' ;
EVERY :		'every' ;
PLUS :		'+' ;
MINUS :		'-' ;
MUL : 		'*' ;
DIV : 		'div' ;
IDIV : 		'idiv' ;
MOD : 		'mod' ;
UNION : 		'union' ;
PIPE : 		'|' ;
INTER : 		'intersect' ;
EXCEPT : 		'except' ;
EQUALS :		'=' ;
NEQUAL :		'!=' ;
LESSTHAN :		'<' ;
LESSEQ :		'<=' ;
GREATER :		'>' ;
GREATEQ :		'>=' ;
EQ :		'eq' ;
NE :		'ne' ;
LT : 		'lt' ;
LE :		'le' ;
GT :		'gt' ;
GE :		'ge' ;
IS :		'is' ;
LTLT :		'<<' ;
GTGT :		'>>' ;
PATHSEP :		'/' ;
ABRPATH :		'//' ;
CHILD :		'child' ;
DESCND :		'descendant' ;
ATTRIB :		'attribute' ;
SELF :		'self' ;
DESELF :		'descendant-or-self' ;
FSIBLNG :		'following-sibling' ;
FOLLOW :		'following' ;
NAMESP :		'namespace' ;
AT :		'@' ;
PARENT :		'parent' ;
ANCEST :		'ancestor' ;
PSIBLNG :		'preceding-sibling' ;
PRECED :		'preceding' ;
ANSELF :		'ancestor-or-self' ;
COLWILD :		':*' ;
WILDCOL :		'*:' ;
QUESTION :		'?' ;
