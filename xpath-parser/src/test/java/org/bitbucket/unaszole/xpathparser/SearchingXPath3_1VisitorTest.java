package org.bitbucket.unaszole.xpathparser;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;
import org.bitbucket.unaszole.xpathparser.XPath3_1OperatorFunctions.AtomicValue;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class SearchingXPath3_1VisitorTest
{
	public static class DOMSearchingXPathVisitor extends SearchingXPath3_1Visitor<Node> {

		public DOMSearchingXPathVisitor(EvaluationContext<Node> state)
		{
			super(state);
		}

		@Override
		protected Node getDocumentRoot(Node ctx)
		{
			Node parentNode = ctx.getParentNode();
			if(parentNode == null)
			{
				return ctx;
			}
			else
			{
				return getDocumentRoot(parentNode);
			}
		}

		@Override
		protected Node getParent(Node ctx)
		{
			return ctx.getParentNode();
		}

		@Override
		protected List<Node> getChildren(Node ctx)
		{
			NodeList nodeList = ctx.getChildNodes();
			List<Node> nodes = new ArrayList<Node>();

		    for(int i = 0; i < nodeList.getLength(); i++)
		    {
		    	nodes.add(nodeList.item(i));
		    }

		    return nodes;
		}
		
		@Override
		protected List<Node> getAttributes(Node ctx)
		{
			NamedNodeMap attributeMap = ctx.getAttributes();
			List<Node> attributes = new ArrayList<Node>();
			
			for(int i = 0; i < attributeMap.getLength(); i++)
		    {
				attributes.add(attributeMap.item(i));
		    }
			
			return attributes;
		}

		@Override
		protected String getNSPrefix(Node ctx)
		{
			if(ctx.getPrefix() == null)
			{
				return "";
			}
			return ctx.getPrefix();
		}

		@Override
		protected String getNSURI(Node ctx)
		{
			return ctx.getNamespaceURI();
		}

		@Override
		protected String getLocalPart(Node ctx)
		{
			return ctx.getLocalName();
		}

		@Override
		protected ParseTreeVisitor<Evaluation<Node>> createVisitor(EvaluationContext<Node> state)
		{
			return new DOMSearchingXPathVisitor(state);
		}

		@Override
		protected AtomicValue getNodeAtomicValue(Node ctx)
		{
			return new AtomicValue(ctx.getTextContent());
		}
	}
	
	private List<Node> evaluateXPathInDoc(String xPathString, Document doc) throws Exception
	{
		CharStream in = CharStreams.fromString(xPathString);
		XPath3_1Lexer lexer = new XPath3_1Lexer(in);
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		XPath3_1Parser parser = new XPath3_1Parser(tokens);
		
		XPath3_1Parser.XPathContext mainContext = parser.xPath();
		
		DOMSearchingXPathVisitor visitor = new DOMSearchingXPathVisitor(new EvaluatingXPath3_1Visitor.EvaluationContext<Node>(doc));
		return visitor.getValueAsNodeList(visitor.visitXPath(mainContext));
	}
	
	private NodeList evaluateReferenceXPathInDoc(String xPathString, Document doc) throws Exception
	{
		XPath xPath = XPathFactory.newInstance().newXPath();
		return (NodeList)xPath.evaluate(xPathString, doc.getDocumentElement(), XPathConstants.NODESET);
	}
	
	private void testXPathInFile(String xPathString, File xmlFile) throws Exception
	{
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		dbf.setNamespaceAware(true);
		DocumentBuilder db = dbf.newDocumentBuilder(); 
		Document doc = db.parse(xmlFile);
		
		NodeList referenceResult = evaluateReferenceXPathInDoc(xPathString, doc);
		List<Node> actualResult = evaluateXPathInDoc(xPathString, doc);
		assertEquals("Same number of matching items.", referenceResult.getLength(), actualResult.size());
		
		for(int i = 0; i < referenceResult.getLength(); i++)
		{
			assertEquals("Same matching items in the same order.", referenceResult.item(i), actualResult.get(i));
		}
	}
	
	@Test
	public void testSimpleNodeSearch() throws Exception
	{
		File exampleXmlFile = new File(this.getClass().getResource("example.xml").getFile());
		testXPathInFile("/catalog/book", exampleXmlFile);
	}
	
	@Test
	public void testContextPositionPredicateSearch() throws Exception
	{
		File exampleXmlFile = new File(this.getClass().getResource("example.xml").getFile());
		testXPathInFile("/catalog/book[2]/title", exampleXmlFile);
	}
	
	@Test
	public void testAttributeAxisSearch() throws Exception
	{
		File exampleXmlFile = new File(this.getClass().getResource("example.xml").getFile());
		testXPathInFile("/catalog/book[2]/@id", exampleXmlFile);
	}
	
	@Test
	public void testPredicateWithFieldValue() throws Exception
	{
		File exampleXmlFile = new File(this.getClass().getResource("example.xml").getFile());
		testXPathInFile("/catalog/book[genre='Fantasy']", exampleXmlFile);
	}
	
	@Test
	public void testPredicateWithAttributeValue() throws Exception
	{
		File exampleXmlFile = new File(this.getClass().getResource("example.xml").getFile());
		testXPathInFile("/catalog/book[@id='bk105']", exampleXmlFile);
	}
	
	@Test
	public void testContextPositionInChainedPredicates() throws Exception
	{
		File exampleXmlFile = new File(this.getClass().getResource("example.xml").getFile());
		testXPathInFile("/catalog/book[genre='Computer'][2]", exampleXmlFile);
	}
}
