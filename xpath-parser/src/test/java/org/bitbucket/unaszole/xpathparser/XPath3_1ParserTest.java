package org.bitbucket.unaszole.xpathparser;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.junit.Test;

public class XPath3_1ParserTest
{
	@Test
	public void testBasicXPath() throws Exception
	{
		CharStream in = CharStreams.fromString("/a/b[0 + 4]/c");
		XPath3_1Lexer lexer = new XPath3_1Lexer(in);
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		XPath3_1Parser parser = new XPath3_1Parser(tokens);
		
		XPath3_1Parser.XPathContext mainContext = parser.xPath();
		XPath3_1Visitor<Void> visitor = new XPath3_1BaseVisitor<Void>() {
			@Override
			public Void visitNameTest(XPath3_1Parser.NameTestContext ctx) {
				System.out.println("Name Test : " + ctx.eqName().getText());
				return visitChildren(ctx);
			}
			
			@Override
			public Void visitPredicate(XPath3_1Parser.PredicateContext ctx) {
				System.out.println("Predicate : " + ctx.getText());
				return visitChildren(ctx);
			}
		};
		visitor.visitXPath(mainContext);
	}
}
