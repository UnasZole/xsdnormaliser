# xsdnormaliser
Normalise complex sets of XSDs to simplify their structure. Useful notably before a JaXB compilation, to get rid of unsupported structures.
