package org.bitbucket.unaszole.xsdnormaliser.schemahandlers;

import java.util.List;
import javax.xml.namespace.QName;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import org.bitbucket.unaszole.xmlstreameditor.XMLEventHandler;
import org.bitbucket.unaszole.xmlstreameditor.builders.EventList;
import org.bitbucket.unaszole.xmlstreameditor.builders.StartElementBuilder;
import org.bitbucket.unaszole.xmlstreameditor.eventsink.SinkManager;

/**
	An XMLEventHandler that sets the targetNamespace attribute of the schema to one provided in context.
*/
public class SchemaNamespaceSetter implements XMLEventHandler<SchemaNamespaceSetter.Context>
{
	public interface Context
	{
		String getTargetNamespace();
	}
	
	private static String XMLSCHEMA_NS = "http://www.w3.org/2001/XMLSchema";
	
	@Override
	public List<XMLEvent> handle(XMLEvent inEvent, SinkManager sinkManager, Context context)
	{
		List<XMLEvent> replacementEvents = null;
		
		if(inEvent.isStartElement())
		{
			StartElement event = inEvent.asStartElement();
			QName qname = event.getName();
			if(XMLSCHEMA_NS.equals(qname.getNamespaceURI()))
			{
				if("schema".equals(qname.getLocalPart()))
				{
					String targetNamespace = context.getTargetNamespace();
					return EventList.from(StartElementBuilder.from(event).withAttribute(new QName("targetNamespace"), targetNamespace).withNamespace("", targetNamespace).build());
				}
			}
		}
		return replacementEvents;
	}
}

