package org.bitbucket.unaszole.xsdnormaliser;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;
import org.bitbucket.unaszole.xsdnormaliser.schemahandlers.xsd.NamedEntity;
import org.bitbucket.unaszole.xsdnormaliser.schemahandlers.xsd.NamedEntityMember;
import org.bitbucket.unaszole.xsdnormaliser.schemahandlers.xsd.NamedEntityReference;
import org.bitbucket.unaszole.xsdnormaliser.schemahandlers.xsd.NamedEntityReference.EntityType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConflictResolver
{
	/**
	 * Logger to use in this class.
	 * Each log level should be used as follows :
	 * - error : if logging a behaviour that indicates an error in the xsdnormaliser processing itself.
	 * - warn : if logging a behaviour which is probably due to unsupported structures in the input schemas.
	 * - info : if logging high level information about which step of processing is currently ongoing.
	 * - debug : if logging a specific operation being performed by the application.
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(ConflictResolver.class);
	
	private static class EntityReferenceComparator implements Comparator<NamedEntityReference>
	{
		public int compare(NamedEntityReference first, NamedEntityReference second)
		{
			int namespaceOrder = first.getQName().getNamespaceURI().compareTo(second.getQName().getNamespaceURI());
			if(namespaceOrder != 0)
			{
				return namespaceOrder;
			}
			
			int localNameOrder = first.getQName().getLocalPart().compareTo(second.getQName().getLocalPart());
			if(localNameOrder != 0)
			{
				return localNameOrder;
			}
			
			int typeOrder = first.getType().compareTo(second.getType());
			return typeOrder;
		}
	}
	
	private static class EntityComparator implements Comparator<NamedEntity>
	{
		public int compare(NamedEntity first, NamedEntity second)
		{
			int referenceOrder = new EntityReferenceComparator().compare(first, second);
			if(referenceOrder != 0)
			{
				return referenceOrder;
			}
			
			int schemaPathOrder = first.getSchema().getPath().compareTo(second.getSchema().getPath());
			if(schemaPathOrder != 0)
			{
				return schemaPathOrder;
			}
			
			if(first.getClassBoundNode() != null)
			{
				if(second.getClassBoundNode() == null)
				{
					return -1;
				}
				int classBoundNodeOrder = first.getClassBoundNode().toString().compareTo(second.getClassBoundNode().toString());
				if(classBoundNodeOrder != 0)
				{
					return classBoundNodeOrder;
				}
			}
			else if(second.getClassBoundNode() != null)
			{
				return 1;
			}

			return 0;
		}
	}
	
	private static class MemberComparator implements Comparator<NamedEntityMember>
	{
		public int compare(NamedEntityMember first, NamedEntityMember second)
		{
			int nameOrder = first.getName().compareTo(second.getName());
			if(nameOrder != 0)
			{
				return nameOrder;
			}
			
			int typeOrder = first.getType().compareTo(second.getType());
			if(typeOrder != 0)
			{
				return typeOrder;
			}
			
			int schemaPathOrder = first.getSchema().getPath().compareTo(second.getSchema().getPath());
			if(schemaPathOrder != 0)
			{
				return schemaPathOrder;
			}
			
			if(first.getPropertyBoundNode() != null)
			{
				if(second.getPropertyBoundNode() == null)
				{
					return -1;
				}
				int propertyBoundNodeOrder = first.getPropertyBoundNode().toString().compareTo(second.getPropertyBoundNode().toString());
				if(propertyBoundNodeOrder != 0)
				{
					return propertyBoundNodeOrder;
				}
			}
			else if(second.getPropertyBoundNode() != null)
			{
				return 1;
			}

			return 0;
		}
	}
	
	/**
	 * Enforce an arbitrary number on a set of entity references.
	 * @param entitySet A collection of entity references in any order.
	 * @return The same entity references, in a consistent order based on specific entity reference values.
	 */
	private static List<NamedEntityReference> getSortedEntityReferences(Collection<NamedEntityReference> entitySet)
	{
		List<NamedEntityReference> entities = new ArrayList<NamedEntityReference>();
		entities.addAll(entitySet);
		Collections.sort(entities, new EntityReferenceComparator());
		return entities;
	}
	
	/**
	 * Enforce an arbitrary number on a set of entities, to make sure the result of any computation based on it on is deterministic and as platform-independent as possible.
	 * @param entitySet A collection of entities in any order.
	 * @return The same entities, in a consistent order based on specific entity values.
	 */
	private static List<NamedEntity> getSortedEntities(Collection<NamedEntity> entitySet)
	{
		List<NamedEntity> entities = new ArrayList<NamedEntity>();
		entities.addAll(entitySet);
		Collections.sort(entities, new EntityComparator());
		return entities;
	}
	
	/**
	 * Enforce an arbitrary number on a set of members.
	 * @param memberSet A collection of members in any order.
	 * @return The same members, in a consistent order based on specific member values.
	 */
	private static List<NamedEntityMember> getSortedEntityMembers(Collection<NamedEntityMember> memberSet)
	{
		List<NamedEntityMember> members = new ArrayList<NamedEntityMember>();
		members.addAll(memberSet);
		Collections.sort(members, new MemberComparator());
		return members;
	}
	
	private static List<NamedEntity> getConflictingClassNameEntities(final NamedEntity currentEntity, List<NamedEntity> entitiesFromNS)
	{
		return getSortedEntities(CollectionUtils.select(entitiesFromNS, new Predicate<NamedEntity>(){
			@Override
			public boolean evaluate(NamedEntity entity)
			{
				return 
					entity != currentEntity && 
					entity.getClassBoundNode() != null &&
					entity.getJavaName().equals(currentEntity.getJavaName());
			}
		}));
	}
	
	private static void resolveClassNameConflict(NamedEntity entityOne, NamedEntity entityTwo, List<NamedEntity> entitiesFromNS)
	{
		switch(entityOne.getType())
		{
			case TOPLEVEL_ELEMENT:
				// There can be only one top level element with that name : suffixing it with "Elt", should be enough.
				// (At worst, if another ___Elt already exists, the conflict will appear again, and this will be renamed to ___EltElt.)
				entityOne.overrideJavaName(entityOne.getJavaName() + "Elt");
				break;
			
			case INNER_ELEMENT:
				// There can be plenty of inner elements with same name. We may need to suffix with a number to cover all cases.
				entityOne.overrideJavaName(entityOne.getJavaName() + "SubElt");
				List<NamedEntity> conflictingEntities;
				String entityOneJavaNameBeforeLoop = entityOne.getJavaName();
				int suffixNumber = 2;
				while(!(conflictingEntities = getConflictingClassNameEntities(entityOne, entitiesFromNS)).isEmpty())
				{
					entityOne.overrideJavaName(entityOneJavaNameBeforeLoop + suffixNumber);
				}
				break;
			
			case TYPE:
				// Entity one is a type : by convention, we don't want to rebind it. Handle the conflict on entity two !
				if(entityTwo.getType() != EntityType.TYPE)
				{
					resolveClassNameConflict(entityTwo, entityOne, entitiesFromNS);
				}
				else
				{
					// Except if entity two is a type as well. That's an error case anyway, but handle it in best effort mode.
					LOGGER.warn("Resolving class conflict on two types with same name : {}", entityOne.getQName());
					entityOne.overrideJavaName(entityOne.getJavaName() + "Type");
				}
				break;
			
			default:
				// Unknown type : log error and resolve conflict in best effort mode.
				LOGGER.error("Resolving class conflict on entity of unknown type : {} conflicting with {}", entityOne, entityTwo);
				entityOne.overrideJavaName(entityOne.getJavaName() + entityOne.getType());
				break;
		}
	}
	
	/**
	 * Check if a given entity has a class name conflict with any other entity from the same namespace.
	 * If so, override the Java name of either this entity or the conflicting ones until no conflict remains.
	 * @param currentEntity The entity to check.
	 * @param entitiesFromNS The list of all entities in the same namespace.
	 */
	private static void detectAndResolveClassNameConflicts(NamedEntity currentEntity, List<NamedEntity> entitiesFromNS)
	{
		if(currentEntity.getClassBoundNode() != null)
		{
			// If current entity is class-bound, it may have class name conflicts.
			
			List<NamedEntity> conflictingEntities = null;
			while(!(conflictingEntities = getConflictingClassNameEntities(currentEntity, entitiesFromNS)).isEmpty())
			{
				// As long as there are still conflicts with the current entity, keep working.
				LOGGER.debug("Current entity {} conflicts with {}", currentEntity, conflictingEntities);
				for(NamedEntity conflictingEntity : conflictingEntities)
				{
					if(currentEntity.getType() == conflictingEntity.getType() && currentEntity.getType() != EntityType.INNER_ELEMENT)
					{
						// If that conflict is actually forbidden by XSD itself (same type), log error !
						// (Inner elements are not triggering an error, as they are not populating the XSD namespace and therefore not in conflict at XSD level).
						LOGGER.warn("Two entities of same name and type, forbidden by XSD : {} and {}", currentEntity, conflictingEntity);
					}
					
					// Proceed to resolution, even if error (best effort...)
					resolveClassNameConflict(currentEntity, conflictingEntity, entitiesFromNS);
				}
			}
		}
	}
	
	/**
	 * Resolve an entity reference from the set of all entities registered during this run.
	 */
	private static NamedEntity resolveReference(final NamedEntityReference reference, Map<String, Set<NamedEntity>> allEntitiesPerNS)
	{
		Set<NamedEntity> entitiesFromNS = allEntitiesPerNS.get(reference.getQName().getNamespaceURI());
		
		if(entitiesFromNS == null)
		{
			// No entities parsed from this NS during this run : reference cannot be resolved.
			return null;
		}
		
		Collection<NamedEntity> matchingEntities = CollectionUtils.select(entitiesFromNS, new Predicate<NamedEntity>(){
			@Override
			public boolean evaluate(NamedEntity entity)
			{
				return 
					entity.getType() == reference.getType() && 
					entity.getQName().getLocalPart().equals(reference.getQName().getLocalPart());
			}
		});
		
		
		if(matchingEntities.size() == 0)
		{
			// Target entity not found in namespace even though the namespace was handled in this run. Log and return null.
			LOGGER.warn("Found no entity matching {}", reference);
			return null;
		}
		
		if(matchingEntities.size() > 1)
		{
			// Target entity found twice or more. Log, but still return first (best effort).
			LOGGER.warn("Found {} entities matching {} : {}", matchingEntities.size(), reference, matchingEntities);
		}
		
		return matchingEntities.iterator().next();
	}
	
	private static boolean isMemberConflicting(NamedEntityMember currentMember, List<NamedEntityMember> existingMembers)
	{
		for(NamedEntityMember existingMember : existingMembers)
		{
			if(existingMember == currentMember)
			{
				LOGGER.error("Current member {} was already collected...", currentMember);
				return false;
			}
			else if(existingMember.getJavaName().equals(currentMember.getJavaName()))
			{
				LOGGER.debug("Current member {} conflicts with existing member {}", currentMember, existingMember);
				return true;
			}
		}
		return false;
	}
	
	private static void collectMembersAndSolvePropertyNameConflicts(NamedEntity currentEntity, Map<String, Set<NamedEntity>> allEntitiesPerNS, List<NamedEntityMember> collectedMembers)
	{
		// Recursively call the method on all bases (in deterministic order) to populate list of pre-existing members.
		for(NamedEntityReference baseRef : getSortedEntityReferences(currentEntity.getBases()))
		{
			NamedEntity base = resolveReference(baseRef, allEntitiesPerNS);
			if(base != null)
			{
				collectMembersAndSolvePropertyNameConflicts(base, allEntitiesPerNS, collectedMembers);
			}
		}
		
		// Process all members defined on this specific entity (in deterministic order)
		for(NamedEntityMember currentMember : getSortedEntityMembers(currentEntity.getMembers()))
		{
			if(isMemberConflicting(currentMember, collectedMembers))
			{
				// First solution : try to append "Prop" or "Attr" to the java name.
				switch(currentMember.getType())
				{
					case PROPERTY:
						currentMember.overrideJavaName(currentMember.getJavaName() + "Prop");
						break;
					case ATTRIBUTE:
						currentMember.overrideJavaName(currentMember.getJavaName() + "Attr");
						break;
				}
			}
			int memberNameSuffix = 2;
			String memberJavaNameBeforeLoop = currentMember.getJavaName();
			while(isMemberConflicting(currentMember, collectedMembers))
			{
				// If conflict not solved, append a number suffix until it is.
				currentMember.overrideJavaName(memberJavaNameBeforeLoop + memberNameSuffix);
				memberNameSuffix++;
			}
			
			// Member is now conflict-free : collect it !
			collectedMembers.add(currentMember);
		}
	}
	
	/**
	 * Check all members of a given entity for property name conflict with any of its bases.
	 * If a conflict is detected, override the Java name of conflicting methods until no conflict remains.
	 * @param currentEntity The entity to check.
	 * @param allEntitiesPerNS All entities registered in this run, regrouped by namespace.
	 */
	private static void detectAndResolvePropertyNameConflicts(NamedEntity currentEntity, Map<String, Set<NamedEntity>> allEntitiesPerNS)
	{
		collectMembersAndSolvePropertyNameConflicts(currentEntity, allEntitiesPerNS, new ArrayList<NamedEntityMember>());
	}
	
	public static void resolveConflicts(Map<String, Set<NamedEntity>> allEntitiesPerNS)
	{
		for(Map.Entry<String, Set<NamedEntity>> entry : allEntitiesPerNS.entrySet())
		{
			// Treat each namespace sequentially to handle class name conflicts.
			LOGGER.info("\n=== Conflict resolution for namespace {} start. ===", entry.getKey());
			
			// Get all entities registered from the namespace, sorted..
			List<NamedEntity> entitiesFromNS = getSortedEntities(entry.getValue());
			
			for(NamedEntity currentEntity : entitiesFromNS)
			{
				detectAndResolveClassNameConflicts(currentEntity, entitiesFromNS);
				
				detectAndResolvePropertyNameConflicts(currentEntity, allEntitiesPerNS);
			}
			
			LOGGER.info("\n=== Conflict resolution for namespace {} complete. ===\n\n\n", entry.getKey());
		}
	}
}
