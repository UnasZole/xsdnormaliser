package org.bitbucket.unaszole.xsdnormaliser;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.io.FilenameUtils;
import org.bitbucket.unaszole.xsdnormaliser.schemahandlers.xsd.NamedEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class XSDNormaliser
{
	private static final Logger LOGGER = LoggerFactory.getLogger(XSDNormaliser.class);
	
	private Configuration config;
	private File targetSchemasDirectory;
	private File targetCatalogsDirectory;
	private File targetBindingsDirectory;
	private SchemaNormaliser schemaNormaliser;
	/* private BindingNormaliser bindingNormaliser; */
	/* private CatalogNormaliser catalogNormaliser; */
	
	private Map<String, Set<String>> dependenciesPerNS;
	private Map<String, Set<NamedEntity>> entitiesPerNS;
	
	private NormalisedSchemasManager normalisedSchemasManager;
	
	public XSDNormaliser(Configuration configuration)
	{
		this.config = configuration;
		this.targetSchemasDirectory = new File(config.getTargetDirectory(), "schemas");
		this.targetCatalogsDirectory = new File(config.getTargetDirectory(), "catalogs");
		this.targetBindingsDirectory = new File(config.getTargetDirectory(), "bindings");
		this.dependenciesPerNS = new HashMap<String, Set<String>>();
		this.entitiesPerNS = new HashMap<String, Set<NamedEntity>>();
		this.normalisedSchemasManager = new NormalisedSchemasManager(
			this.targetSchemasDirectory,
			configuration.sameSchemaName_KeepOnlyOne(),
			configuration.sameSchemaName_ReverseOrder(),
			this.entitiesPerNS
		);
		this.schemaNormaliser = new SchemaNormaliser(this.config, this.targetSchemasDirectory,
			this.dependenciesPerNS, this.entitiesPerNS,	this.normalisedSchemasManager);
	}
	
	private void normaliseFile(File sourceFile) throws FileNotFoundException, IOException
	{
		String extension = FilenameUtils.getExtension(sourceFile.getName());
		
		if("xsd".equals(extension))
		{
			schemaNormaliser.normalise(sourceFile);
		}
		else if("xjb".equals(extension))
		{
		}
		else if("cat".equals(extension))
		{
		}
	}
	
	private void normalise(File source) throws FileNotFoundException, IOException
	{
		if(source.isDirectory())
		{
			// Get ordered directory contents (Subdirectories first, then files, all in lexicographic order)
			File[] entries = source.listFiles();
			Arrays.sort(entries, new Comparator<File>() {
				public int compare(File first, File second)
				{
					if(first.isDirectory() == second.isDirectory())
					{
						// If both entries are of same type, use default order. Reversed if configuration set.
						return first.compareTo(second) * (config.sameSchemaName_ReverseOrder() ? -1 : 1);
					}
					else if(first.isDirectory() && !second.isDirectory())
					{
						// Directory has lower rank, since it must be explored before.
						return -1;
					}
					else
					{
						return 1;
					}
				}
			});
			
			for(File entry : entries)
			{
				normalise(entry);
			}
		}
		else
		{
			normaliseFile(source);
		}
	}
	
	public void run() throws FileNotFoundException, IOException
	{
		// Step 1 : Run normaliser for all files in the source path.
		LOGGER.info("\n==== Process start for {}. ====", config.getSourcePath());
		normalise(config.getSourcePath());
		
		Map<String, Set<File>> schemasPerNS = normalisedSchemasManager.getSchemasPerNamespace();
		
		// Step 2 : Generate namespace entrypoint schemas.
		LOGGER.info("\n\n\n\n==== Starting generation of namespace entrypoints. ====");
		NamespaceEntrypointGenerator.generateEntrypoints(schemasPerNS, targetSchemasDirectory);
		
		// Step 3 : Generate catalog file for namespaces normalised during this run.
		LOGGER.info("\n\n\n\n==== Starting generation of catalog file. ====");
		CatalogGenerator.generateCatalog(schemasPerNS.keySet(),
																		 targetCatalogsDirectory,
																		 targetSchemasDirectory,
																		 config.getSourcePath());
		
		// Step 4 : Resolve conflicts on registered entities and generate bindings.
		LOGGER.info("\n\n\n\n==== Starting computation of conflict resolution. ====");
		ConflictResolver.resolveConflicts(this.entitiesPerNS);
		LOGGER.info("\n\n\n\n==== Starting generation of binding files. ====");
		BindingGenerator.generateBindings(this.entitiesPerNS, targetBindingsDirectory, config.packageBinding_Prefix());
		LOGGER.info("\n\n\n\n==== Process complete. ====");
	}
}
