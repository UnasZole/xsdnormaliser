package org.bitbucket.unaszole.xsdnormaliser;

import com.sun.xml.bind.api.impl.NameConverter;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javanet.staxutils.IndentingXMLEventWriter;
import javax.xml.namespace.QName;
import javax.xml.stream.*;
import javax.xml.stream.events.XMLEvent;
import org.apache.commons.io.FileUtils;
import org.bitbucket.unaszole.xmlstreameditor.builders.*;

public class NamespaceEntrypointGenerator
{
	private static XMLEventFactory FACTORY = XMLEventFactory.newInstance();
	private static String XSD_NS = "http://www.w3.org/2001/XMLSchema";
	private static QName SCHEMA_ELT = new QName(XSD_NS, "schema");
	private static QName INCLUDE_ELT = new QName(XSD_NS, "include");
	
	public static File getEntrypointForNamespace(String targetNamespace, File targetSchemasDirectory) throws IOException
	{
		File targetEntrypointsDirectory = new File(targetSchemasDirectory, "entrypoints");
		FileUtils.forceMkdir(targetEntrypointsDirectory);
		
		String nsPackageName = "".equals(targetNamespace) ? "__localnamespace" : (new NameConverter.Standard()).toPackageName(targetNamespace);
		return new File(targetEntrypointsDirectory, nsPackageName + "-entrypoint.xsd");
	}
	
	private static void generateEntrypointForNs(String targetNamespace, Set<File> schemasInNs, 
																				File targetSchemasDirectory) throws XMLStreamException, FileNotFoundException, IOException
	{
		File entrypointSchemaFile = getEntrypointForNamespace(targetNamespace, targetSchemasDirectory);
		XMLOutputFactory factory = XMLOutputFactory.newInstance();
		factory.setProperty(XMLOutputFactory.IS_REPAIRING_NAMESPACES, true);
		XMLEventWriter writer = new IndentingXMLEventWriter(factory.createXMLEventWriter(new BufferedOutputStream(new FileOutputStream(entrypointSchemaFile))));
		
		writer.add(FACTORY.createStartDocument("UTF-8", "1.0"));
		writer.add(StartElementBuilder.newElt(SCHEMA_ELT).withAttribute(new QName("targetNamespace"), targetNamespace).build());
		
		for(File targetSchema : schemasInNs)
		{
			String relativeTargetSchemaLocation = FilePathUtils.getRelativePath(entrypointSchemaFile, targetSchema);
			writer.add(StartElementBuilder.newElt(INCLUDE_ELT).withAttribute(new QName("schemaLocation"), relativeTargetSchemaLocation).build());
			writer.add(EndElementBuilder.newElt(INCLUDE_ELT).build());
		}
		
		writer.add(EndElementBuilder.newElt(SCHEMA_ELT).build());
		writer.add(FACTORY.createEndDocument());
		writer.close();
	}
	
	public static void generateEntrypoints(Map<String, Set<File>> allSchemasPerNS, File targetSchemasDirectory) throws FileNotFoundException, IOException
	{
		for(Map.Entry<String, Set<File>> entry : allSchemasPerNS.entrySet())
		{
			try
			{
				generateEntrypointForNs(entry.getKey(), entry.getValue(), targetSchemasDirectory);
			}
			catch(XMLStreamException e)
			{
				// TODO : handle exception properly.
			}
		}
	}
}
