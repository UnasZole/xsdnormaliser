package org.bitbucket.unaszole.xsdnormaliser;

import com.sun.xml.bind.api.impl.NameConverter;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import javanet.staxutils.IndentingXMLEventWriter;
import javax.xml.namespace.NamespaceContext;
import javax.xml.namespace.QName;
import javax.xml.stream.*;
import javax.xml.stream.events.XMLEvent;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.io.FileUtils;
import org.bitbucket.unaszole.xmlstreameditor.builders.*;
import org.bitbucket.unaszole.xmlstreameditor.xml.XMLElement;
import org.bitbucket.unaszole.xsdnormaliser.schemahandlers.xsd.NamedEntity;
import org.bitbucket.unaszole.xsdnormaliser.schemahandlers.xsd.NamedEntityMember;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BindingGenerator
{
	private static final Logger LOGGER = LoggerFactory.getLogger(BindingGenerator.class);
	
	public static Collection<NamedEntityMember> getOverriddenMembers(NamedEntity entity)
	{
		return CollectionUtils.select(entity.getMembers(), new Predicate<NamedEntityMember>(){
			@Override
			public boolean evaluate(NamedEntityMember member)
			{
				return member.isJavaNameOverridden();
			}
		});
	}
	
	public static Map<String, Map<File, Set<NamedEntity>>> getBoundEntitiesPerNsAndSchema(Map<String, Set<NamedEntity>> allEntitiesPerNS)
	{
		Map<String, Map<File, Set<NamedEntity>>> boundEntitiesPerNsAndSchema = new HashMap<String, Map<File, Set<NamedEntity>>>();
		
		for(Map.Entry<String, Set<NamedEntity>> entry : allEntitiesPerNS.entrySet())
		{
			String namespace = entry.getKey();
			for(NamedEntity entity: entry.getValue())
			{
				if(entity.isJavaNameOverridden() || getOverriddenMembers(entity).size() > 0)
				{
					// If entities own name or member names is overridden, register it for binding.
					
					if(!boundEntitiesPerNsAndSchema.containsKey(namespace))
					{
						boundEntitiesPerNsAndSchema.put(namespace, new HashMap<File, Set<NamedEntity>>());
					}
					if(!boundEntitiesPerNsAndSchema.get(namespace).containsKey(entity.getSchema()))
					{
						boundEntitiesPerNsAndSchema.get(namespace).put(entity.getSchema(), new HashSet<NamedEntity>());
					}
					boundEntitiesPerNsAndSchema.get(namespace).get(entity.getSchema()).add(entity);
				}
			}
		}
		
		return boundEntitiesPerNsAndSchema;
	}
	
	private static XMLEventFactory FACTORY = XMLEventFactory.newInstance();
	private static final String XSD_NS = "http://www.w3.org/2001/XMLSchema";
	private static final String JAXB_NS = "http://java.sun.com/xml/ns/jaxb";
	private static final QName BINDINGS_ELT = new QName(JAXB_NS, "bindings");
	private static final QName SCHEMABINDINGS_ELT = new QName(JAXB_NS, "schemaBindings");
	private static final QName CLASS_ELT = new QName(JAXB_NS, "class");
	private static final QName PROPERTY_ELT = new QName(JAXB_NS, "property");
	private static final QName PACKAGE_ELT = new QName(JAXB_NS, "package");
	private static NamespaceContext NS_CONTEXT = new NamespaceContext() {
		public String getNamespaceURI(String prefix)
		{
			if(prefix.equals("xsd"))
			{
				return XSD_NS;
			}
			return null;
		}
		
		public String getPrefix(String nsURI)
		{
			if(nsURI.equals(XSD_NS))
			{
				return "xsd";
			}
			return null;
		}
		
		public Iterator<String> getPrefixes(String nsURI)
		{
			return null;
		}
	};
	private static Set<String> UNIQUE_ATTRS = new HashSet(Arrays.asList("name"));
	
	private static void writeBindingForPackage(String targetNamespace, String packagePrefix, XMLEventWriter writer) throws XMLStreamException
	{
		if(packagePrefix != null && !packagePrefix.isEmpty())
		{
			// If package prefix specified only, generate binding.
			LOGGER.debug("Generating package binding for namespace {}.", targetNamespace);
			writer.add(StartElementBuilder.newElt(BINDINGS_ELT).withAttribute(new QName("node"), "/" + NS_CONTEXT.getPrefix(XSD_NS) + ":schema").build());
			writer.add(StartElementBuilder.newElt(SCHEMABINDINGS_ELT).build());
			writer.add(StartElementBuilder.newElt(PACKAGE_ELT).withAttribute(new QName("name"), packagePrefix + "." + (new NameConverter.Standard()).toPackageName(targetNamespace)).build());
			writer.add(EndElementBuilder.newElt(PACKAGE_ELT).build());
			writer.add(EndElementBuilder.newElt(SCHEMABINDINGS_ELT).build());
			writer.add(EndElementBuilder.newElt(BINDINGS_ELT).build());
		}
	}
	
	private static void writeBindingsForBoundEntities(Set<NamedEntity> entities, XMLEventWriter writer) throws XMLStreamException
	{
		for(NamedEntity entity : entities)
		{
			// Handle class binding for the node if necessary.
			XMLElement classBoundNode = entity.getClassBoundNode();
			if(classBoundNode != null && entity.isJavaNameOverridden())
			{
				writer.add(StartElementBuilder.newElt(BINDINGS_ELT).withAttribute(new QName("node"), classBoundNode.readableXPath(NS_CONTEXT, UNIQUE_ATTRS)).build());
				writer.add(StartElementBuilder.newElt(CLASS_ELT).withAttribute(new QName("name"), entity.getJavaName()).build());
				writer.add(EndElementBuilder.newElt(CLASS_ELT).build());
				writer.add(EndElementBuilder.newElt(BINDINGS_ELT).build());
			}
			
			// Handle property bindings for members if necessary.
			for(NamedEntityMember member : entity.getMembers())
			{
				XMLElement propertyBoundNode = member.getPropertyBoundNode();
				if(propertyBoundNode != null && member.isJavaNameOverridden())
				{
					writer.add(StartElementBuilder.newElt(BINDINGS_ELT).withAttribute(new QName("node"), propertyBoundNode.readableXPath(NS_CONTEXT, UNIQUE_ATTRS)).build());
					writer.add(StartElementBuilder.newElt(PROPERTY_ELT).withAttribute(new QName("name"), member.getJavaName()).build());
					writer.add(EndElementBuilder.newElt(PROPERTY_ELT).build());
					writer.add(EndElementBuilder.newElt(BINDINGS_ELT).build());
				}
			}
		}
	}
	
	private static void generateBindingFileForNs(String targetNamespace, Map<File, Set<NamedEntity>> boundEntitiesInNs, 
		File targetBindingsDirectory, String packagePrefix) throws XMLStreamException, FileNotFoundException
	{
		LOGGER.info("\n=== Binding generation for namespace {} start. ===", targetNamespace);
		String nsPackageName = "".equals(targetNamespace) ? "__localnamespace" : (new NameConverter.Standard()).toPackageName(targetNamespace);
		
		File targetBindingFile = new File(targetBindingsDirectory, nsPackageName + ".xjb");
		
		XMLOutputFactory factory = XMLOutputFactory.newInstance();
		factory.setProperty(XMLOutputFactory.IS_REPAIRING_NAMESPACES, true);
		XMLEventWriter writer = new IndentingXMLEventWriter(factory.createXMLEventWriter(new BufferedOutputStream(new FileOutputStream(targetBindingFile))));
		
		writer.add(FACTORY.createStartDocument("UTF-8", "1.0"));
		writer.add(StartElementBuilder.newElt(BINDINGS_ELT).withAttribute(new QName("version"), "2.1").withNamespace("xsd", XSD_NS).build());
		
		for(Map.Entry<File, Set<NamedEntity>> entry : boundEntitiesInNs.entrySet())
		{
			String relativeTargetSchemaLocation = FilePathUtils.getRelativePath(targetBindingFile, entry.getKey());
			writer.add(StartElementBuilder.newElt(BINDINGS_ELT).withAttribute(new QName("schemaLocation"), relativeTargetSchemaLocation).build());
			writeBindingForPackage(targetNamespace, packagePrefix, writer);
			writeBindingsForBoundEntities(entry.getValue(), writer);
			writer.add(EndElementBuilder.newElt(BINDINGS_ELT).build());
		}
		
		writer.add(EndElementBuilder.newElt(BINDINGS_ELT).build());
		writer.add(FACTORY.createEndDocument());
		writer.close();
		LOGGER.info("\n=== Binding generation for namespace {} complete. ===\n\n\n", targetNamespace);
	}
	
	public static void generateBindings(Map<String, Set<NamedEntity>> allEntitiesPerNS, File targetBindingsDirectory, String packagePrefix) throws FileNotFoundException, IOException
	{
		FileUtils.forceMkdir(targetBindingsDirectory);
		
		for(Map.Entry<String, Map<File, Set<NamedEntity>>> nsBoundEntities : getBoundEntitiesPerNsAndSchema(allEntitiesPerNS).entrySet())
		{
			try
			{
				generateBindingFileForNs(nsBoundEntities.getKey(), nsBoundEntities.getValue(), targetBindingsDirectory, packagePrefix);
			}
			catch(XMLStreamException e)
			{
				LOGGER.error("Exception received during binding generation for namespace: " + nsBoundEntities.getKey(), e);
			}
		}
	}
}
