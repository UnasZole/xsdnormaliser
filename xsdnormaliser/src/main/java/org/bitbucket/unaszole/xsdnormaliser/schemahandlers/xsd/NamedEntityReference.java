package org.bitbucket.unaszole.xsdnormaliser.schemahandlers.xsd;

import javax.xml.namespace.QName;

/**
	This class represents all data necessary to uniquely identify a public named entity in XSD, meaning any entity which can be generated into a public Java named class.
	This can be a type, a top-level element, a top-level attribute, a group or an attributeGroup.
	Inner elements can also be represented by this class : this is because JAXB's localScoping=toplevel option makes them into public Java classes, and they therefore can conflict with public entities. And even as nested classes, they can conflict with their parents, hence why they need to be managed.
*/
public class NamedEntityReference
{
	public static enum EntityType
	{
		TYPE, TOPLEVEL_ELEMENT, INNER_ELEMENT, GROUP, ATTRIBUTE, ATTRIBUTE_GROUP;
	}
	
	private EntityType type;
	private QName qName;

	public NamedEntityReference(EntityType type, QName qName)
	{
		this.type = type;
		this.qName = qName;
	}
	
	public EntityType getType()
	{
		return type;
	}
	
	public QName getQName()
	{
		return qName;
	}
	
	public String toString()
	{
		return "{" + type + ":" + qName + "}";
	}
}