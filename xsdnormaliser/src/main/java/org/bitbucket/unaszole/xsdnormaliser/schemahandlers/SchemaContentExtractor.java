package org.bitbucket.unaszole.xsdnormaliser.schemahandlers;

import java.util.List;
import javax.xml.namespace.QName;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import org.bitbucket.unaszole.xmlstreameditor.XMLEventHandler;
import org.bitbucket.unaszole.xmlstreameditor.builders.EventList;
import org.bitbucket.unaszole.xmlstreameditor.eventsink.SinkManager;

/**
	An XMLEventHandler that returns only the contents of the schema tag. The tag itself and anything outside is stripped.
*/
public class SchemaContentExtractor implements XMLEventHandler<Object>
{
	private static String XMLSCHEMA_NS = "http://www.w3.org/2001/XMLSchema";
	
	private boolean insideSchema = false;
	
	@Override
	public List<XMLEvent> handle(XMLEvent inEvent, SinkManager sinkManager, Object context)
	{
		// By default, return events without modification.
		List<XMLEvent> returnedEvents = null;
		
		if(inEvent.isEndElement())
		{
			EndElement event = inEvent.asEndElement();
			QName qname = event.getName();
			if(XMLSCHEMA_NS.equals(qname.getNamespaceURI())
				&& "schema".equals(qname.getLocalPart()))
			{
				insideSchema = false;
			}
		}
		
		if(!insideSchema)
		{
			// If outside of schema, remove everything.
			returnedEvents = EventList.from();
		}
		
		if(inEvent.isStartElement())
		{
			StartElement event = inEvent.asStartElement();
			QName qname = event.getName();
			if(XMLSCHEMA_NS.equals(qname.getNamespaceURI())
				&& "schema".equals(qname.getLocalPart()))
			{
				insideSchema = true;
			}
		}
		
		return returnedEvents;
	}
}