package org.bitbucket.unaszole.xsdnormaliser.schemahandlers;

import java.util.ArrayList;
import javax.xml.namespace.QName;
import org.bitbucket.unaszole.xmlstreameditor.StructureAwareEventHandler;
import org.bitbucket.unaszole.xmlstreameditor.StructureAwareEventHandler.Context;
import org.bitbucket.unaszole.xmlstreameditor.StructureEditingEventHandler;
import org.bitbucket.unaszole.xmlstreameditor.StructureEditingEventHandler.EditionScope;
import org.bitbucket.unaszole.xmlstreameditor.StructureEditingEventHandler.ExtractedElement;
import org.bitbucket.unaszole.xmlstreameditor.StructureEditingEventHandler.ElementsEditor.Insert;
import org.bitbucket.unaszole.xmlstreameditor.builders.StartElementBuilder;
import org.bitbucket.unaszole.xmlstreameditor.builders.EndElementBuilder;
import org.bitbucket.unaszole.xmlstreameditor.xml.XMLElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
	An XMLEventHandler that edits group elements to avoid non-predictable builds.
	Indeed, when a group contains an element defined by an anonymous complexType, JAXB will generate the class for it only as part of a type that uses that group, 
	and all further uses of that group will be bound to that same class.
	This means that, depending on the order in which JAXB will process users, the generated class is different.
	cf. https://stackoverflow.com/questions/29668858/how-to-influence-class-generation-of-group-declarations-with-xjc
	
	The solution here is to remove all anonymous complex types inside group definitions : make them named complexType definitions instead.
*/
public class GroupElementsHandler extends StructureEditingEventHandler<EditionScope, Context>
{
	private static final Logger LOGGER = LoggerFactory.getLogger(GroupElementsHandler.class);
	
	private static final String XSD_NS = "http://www.w3.org/2001/XMLSchema";
	private static final QName XS_GROUP = new QName(XSD_NS, "group");
	private static final QName XS_ELEMENT = new QName(XSD_NS, "element");
	private static final QName XS_COMPLEXTYPE = new QName(XSD_NS, "complexType");
	private static final QName NAME_ATTR = new QName("name");
	private static final QName TYPE_ATTR = new QName("type");
	
	protected final EditionScope openChildElementScopeIfNeeded(XMLElement childElement, EditionScope parentElementScope, Context context)
	{
		// If opening a group element with a name, open a new scope, as it will probably need edition.
		if(childElement.getEvent().getName().equals(XS_GROUP) && childElement.getEvent().getAttributeByName(NAME_ATTR) != null)
		{
			return new EditionScope(childElement);
		}
		return null;
	}
	
	protected final boolean isScopeComplete(EditionScope currentScope, Context context)
	{
		// Method is called only if opening group element is complete, and that's all we need.
		return true;
	}
	
	private void handleGroupElementType(ElementsEditor editor, XMLElement element, XMLElement parentGroupElement, XMLElement parentElementElement)
	{
		if(element.getEvent().getName().equals(XS_COMPLEXTYPE))
		{
			// Element is an anonymous complexType definition ! Time to handle it.
			LOGGER.debug("Starting handling of group anonymous type at {}.", element);
			
			// Step 1 : Choose a name for this complexType.
			String complexTypeName = 
				parentGroupElement.getEvent().getAttributeByName(NAME_ATTR).getValue() + // Group name.
				parentElementElement.getEvent().getAttributeByName(NAME_ATTR).getValue(); // Element name.
			
			// Step 2 : Create a new, named complexType element, and insert it before the group.
			ExtractedElement namedComplexType = new ExtractedElement(
				StartElementBuilder.from(element.getEvent())
					.withAttribute(NAME_ATTR, complexTypeName)
					.build(),
				EndElementBuilder.from(element.getEndEvent()).build()
			);
			editor.insertElement(namedComplexType, Insert.BEFORE, parentGroupElement);
			
			// Step 3 : Populate the new named complexType with the contents of the anonymous one, then delete the empty anonymous one.
			while(element.getChildren().size() > 0)
			{
				editor.insertElement(editor.removeElement(element.getChildren().get(0)),
					Insert.AT_END_OF, namedComplexType.getLocator());
			}
			editor.removeElement(element);
			
			// Step 4 : create a new "xs:element" referencing the new type name.
			ExtractedElement newElement = new ExtractedElement(
				StartElementBuilder.from(parentElementElement.getEvent())
					.withAttribute(TYPE_ATTR, complexTypeName)
					.build(),
				EndElementBuilder.from(parentElementElement.getEndEvent()).build()
			);
			
			// Step 5 : Insert new element before the existing one, and extract the old one.
			editor.insertElement(newElement, Insert.BEFORE, parentElementElement);
			editor.removeElement(parentElementElement);
			
			LOGGER.debug("Completed handling of group anonymous type at {}.", element);
		}
	}
	
	private void handleGroupElement(ElementsEditor editor, XMLElement element, XMLElement parentGroupElement)
	{
		if(element.getEvent().getName().equals(XS_ELEMENT))
		{
			// If it's an element, let's treat it.
			if(element.getEvent().getAttributeByName(NAME_ATTR) != null)
			{
				// Element definition : let's actually treat it.
				// Note : instantiating a new list, since the children list may be modified by the editions performed in the loop.
				for(XMLElement child : new ArrayList<XMLElement>(element.getChildren()))
				{
					handleGroupElementType(editor, child, parentGroupElement, element);
				}
			}
			// Else, element reference, nothing to do.
		}
		else
		{
			// Else, it's group, choice, sequence, any or all, recursively treat its children.
			// Note : instantiating a new list, since the children list may be modified by the editions performed in the loop.
			for(XMLElement child : new ArrayList<XMLElement>(element.getChildren()))
			{
				handleGroupElement(editor, child, parentGroupElement);
			}
		}
	}
	
	protected final void editElements(ElementsEditor editor, EditionScope scope, Context context)
	{
		XMLElement groupElement = scope.getElementsInScope().get(0);
		
		for(XMLElement childElement : groupElement.getChildren())
		{
			// Browsing through the group child elements : any, choice or sequence.
			handleGroupElement(editor, childElement, groupElement);
		}
	}
}