package org.bitbucket.unaszole.xsdnormaliser;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.xml.stream.XMLStreamException;
import org.apache.commons.io.FileUtils;
import org.bitbucket.unaszole.xmlstreameditor.*;
import org.bitbucket.unaszole.xsdnormaliser.schemahandlers.*;
import org.bitbucket.unaszole.xsdnormaliser.schemahandlers.xsd.NamedEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
	Normalisation of XSD schema files.
*/
public class SchemaNormaliser
{
	private static final Logger LOGGER = LoggerFactory.getLogger(SchemaNormaliser.class);
	
	public static class NormaliserIncludeHandler extends IncludeHandler<SchemaNormaliserIncludedSchemaContext>{}
	
	private static class SchemaNormaliserController implements XMLStreamEditor.Controller<SchemaNormaliserRootSchemaContext>
	{
		private boolean localNamespace_Ignore;
		private boolean targetNamespaceRead = false;
		
		public SchemaNormaliserController(boolean localNamespace_Ignore)
		{
			this.localNamespace_Ignore = localNamespace_Ignore;
		}
		
		@Override
		public boolean terminate(SchemaNormaliserRootSchemaContext ctx)
		{
			if(!targetNamespaceRead)
			{
				// If target namespace was already read by this controller, no need to process everything again.
				
				String targetNamespace = ctx.getTargetNamespace();
				if(targetNamespace != null)
				{
					// If targetNamespace was read in the context, time to process it.
					targetNamespaceRead = true;
					
					if("".equals(targetNamespace) && localNamespace_Ignore)
					{
						// If local namespace read, and local namespace should be ignored, terminate.
						return true;
					}
					else
					{
						// If namespace to be kept, check if there is a matching file already normalised.
						// If there is already a matching normalised file, terminate.
						return ctx.getNormalisedSchemasManager().getPathOfAlreadyNormalisedSchema(targetNamespace, ctx.getSourceFile()) != null;
					}
				}
			}
			
			return false;
		}
		
		@Override
		public boolean unplug(XMLEventHandler<? super SchemaNormaliserRootSchemaContext> handler, SchemaNormaliserRootSchemaContext context)
		{
			return false;
		}
	}
	
	private final Configuration config;
	private final Map<String, Set<String>> dependenciesPerNS;
	private final Map<String, Set<NamedEntity>> entitiesPerNS;
	private final NormalisedSchemasManager normalisedSchemasManager;
	private final List<Class<? extends XMLEventHandler<? super SchemaNormaliserRootSchemaContext>>> rootHandlerList;
	private final List<Class<? extends XMLEventHandler<? super SchemaNormaliserIncludedSchemaContext>>> includedHandlerList;
	private final XMLStreamEditor<SchemaNormaliserRootSchemaContext> editor;
	
	public SchemaNormaliser(Configuration configuration, File targetSchemasDirectory,
		Map<String, Set<String>> dependenciesPerNS,
		Map<String, Set<NamedEntity>> entitiesPerNS,
		NormalisedSchemasManager normalisedSchemasManager)
	{
		this.config = configuration;
		
		this.dependenciesPerNS = dependenciesPerNS;
		this.entitiesPerNS = entitiesPerNS;
		
		this.normalisedSchemasManager = normalisedSchemasManager;
		
		this.rootHandlerList = new ArrayList<Class<? extends XMLEventHandler<? super SchemaNormaliserRootSchemaContext>>>();
		this.rootHandlerList.add(NormaliserIncludeHandler.class);
		this.rootHandlerList.add(NamespaceHandler.class);
		
		this.includedHandlerList = new ArrayList<Class<? extends XMLEventHandler<? super SchemaNormaliserIncludedSchemaContext>>>();
		this.includedHandlerList.add(NormaliserIncludeHandler.class);
		this.includedHandlerList.add(NamespaceHandler.class);
		
		if(config.deterministicBuild_groupElements())
		{
			this.rootHandlerList.add(GroupElementsHandler.class);
			this.includedHandlerList.add(GroupElementsHandler.class);
		}
		else
		{
			// If GroupElementsHandler is not present, ensure there is at least another StructureAwareEventHandler, that will collect all entities.
			this.rootHandlerList.add(StructureAwareEventHandler.StructureCollectingHandler.class);
			this.includedHandlerList.add(StructureAwareEventHandler.StructureCollectingHandler.class);
		}
		
		this.editor = new XMLStreamEditor<SchemaNormaliserRootSchemaContext>(rootHandlerList);
	}
	
	public void normalise(File sourceXsd) throws FileNotFoundException, IOException
	{
		IncludeHandler.Strategy inclusionStrategy = config.include_InBody() ? IncludeHandler.Strategy.IN_BODY : 
			(config.include_EnforceTargetNamespace() ? IncludeHandler.Strategy.ENFORCE_TARGET_NAMESPACE : 
			IncludeHandler.Strategy.KEEP_CHAMELEON);
		
		SchemaNormaliserRootSchemaContext ctx = new SchemaNormaliserRootSchemaContext(
			sourceXsd,
			this.includedHandlerList,
			
			normalisedSchemasManager,
			
			inclusionStrategy,
			config.sameSchemaName_KeepOnlyOne()
		);
		
		LOGGER.info("\n=== Starting normalisation of {}. ===", sourceXsd);
		
		File tempOutput = File.createTempFile("tmpout-", ".xsd");
		
		BufferedInputStream inputStream = new BufferedInputStream(new FileInputStream(sourceXsd));
		BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(tempOutput));
		
		boolean complete;
		
		try
		{
			complete = editor.run(inputStream, outputStream, ctx, new SchemaNormaliserController(config.localNamespace_Ignore()));
		}
		catch(XMLStreamException e)
		{
			LOGGER.error("Exception during normalisation of " + sourceXsd, e);
			complete = false;
		}
		finally
		{
			inputStream.close();
			outputStream.close();
		}
		
		if(complete)
		{
			// If file was entirely parsed, move to target location.
			String targetNS = ctx.getTargetNamespace();
			
			File targetFilePath = normalisedSchemasManager.getAndRegisterPathForNewNormalisedSchema(targetNS, sourceXsd).getAbsoluteFile();
			FileUtils.moveFile(tempOutput, targetFilePath);
			
			// Fill dependencies read from this schema file into the context for this namespace.
			if(!dependenciesPerNS.containsKey(targetNS))
			{
				dependenciesPerNS.put(targetNS, new HashSet<String>());
			}
			dependenciesPerNS.get(targetNS).addAll(ctx.getDependencies());
			
			// Collect entities from this schema file and fill them into the context for this namespace.
			if(!entitiesPerNS.containsKey(targetNS))
			{
				entitiesPerNS.put(targetNS, new HashSet<NamedEntity>());
			}
			entitiesPerNS.get(targetNS).addAll(SchemaEntitiesCollector.collectEntities(ctx.getEltProvider().getDocumentRootElement(),
				targetNS, targetFilePath, config.nameBinding_duplicateNamesSupportTopLevelScoping()));
			LOGGER.info("\n=== Completed normalisation of {}, placed at {}. ===\n\n\n", sourceXsd, targetFilePath);
		}
		else
		{
			// Else, clean out temporary file.
			tempOutput.delete();
			LOGGER.info("\n=== Aborted normalisation of {}. ===\n\n\n", sourceXsd);
		}
	}
}
