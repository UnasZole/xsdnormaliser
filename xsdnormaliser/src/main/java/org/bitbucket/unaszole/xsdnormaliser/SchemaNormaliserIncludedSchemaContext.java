package org.bitbucket.unaszole.xsdnormaliser;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.bitbucket.unaszole.xmlstreameditor.*;
import org.bitbucket.unaszole.xsdnormaliser.schemahandlers.*;
import org.bitbucket.unaszole.xsdnormaliser.schemahandlers.xsd.NamedEntity;

public class SchemaNormaliserIncludedSchemaContext
	extends SchemaNormaliserBaseContext<SchemaNormaliserIncludedSchemaContext>
{
	private File targetIncludingFile;
	
	/**
	@param sourceFile The location of the current file.
	@param strategy The strategy to use for managing inclusions.
	@param includeHandlerClasses The handlers to enable to normalise included files.
	@param dependencies The list that should be filled by all dependencies of the current namespace.
	@param entities The list that should be filled by all entities registered in the current namespace.
	
	@param targetNamespace The target namespace of the including file, and therefore of the current file too.
	@param targetIncludingFile The target location of the including file.
	*/
	public SchemaNormaliserIncludedSchemaContext(
		// Inputs of the edition in progress.
		File sourceFile,
		List<File> includingSourceFiles,
		List<Class<? extends XMLEventHandler<? super SchemaNormaliserIncludedSchemaContext>>> includeHandlerClasses,
		String targetNamespace,
		
		// State of the edition in progress.
		IncludeHandler.NormalisedSchemasManager normalisedSchemasManager,
		Set<String> dependencies,
		
		IncludeHandler.Strategy strategy,
		boolean sameSchemaName_KeepOnlyOne)
	{
		super(
			sourceFile,
			includingSourceFiles,
			includeHandlerClasses,
			
			normalisedSchemasManager,
			dependencies,
			
			strategy,
			sameSchemaName_KeepOnlyOne
		);
		setTargetNamespace(targetNamespace);
		
		if(strategy == IncludeHandler.Strategy.IN_BODY)
		{
			// If including in body, target file is same as including file.
			this.targetFile = targetIncludingFile;
		}
	}
	
	@Override
	public SchemaNormaliserIncludedSchemaContext getIncludeContext(File sourceFile)
	{
		List<File> childIncludingSourceFiles = new ArrayList<File>(getIncludingSourceFiles());
		childIncludingSourceFiles.add(getSourceFile());
		
		return new SchemaNormaliserIncludedSchemaContext(
			sourceFile,
			childIncludingSourceFiles,
			getIncludeHandlerClasses(),
			getTargetNamespace(),
			
			getNormalisedSchemasManager(),
			getDependencies(),
			
			getInclusionStrategy(),
			this.sameSchemaName_KeepOnlyOne
		);
	}
}
	
