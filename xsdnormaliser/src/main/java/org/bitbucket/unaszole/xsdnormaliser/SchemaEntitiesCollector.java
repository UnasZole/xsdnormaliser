package org.bitbucket.unaszole.xsdnormaliser;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.HashSet;
import java.util.Set;
import javax.xml.namespace.QName;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;

import org.bitbucket.unaszole.xmlstreameditor.xml.XMLElement;
import org.bitbucket.unaszole.xsdnormaliser.schemahandlers.xsd.NamedEntity;
import org.bitbucket.unaszole.xsdnormaliser.schemahandlers.xsd.NamedEntityMember;
import org.bitbucket.unaszole.xsdnormaliser.schemahandlers.xsd.NamedEntityMember.MemberType;
import org.bitbucket.unaszole.xsdnormaliser.schemahandlers.xsd.NamedEntityReference;
import org.bitbucket.unaszole.xsdnormaliser.schemahandlers.xsd.NamedEntityReference.EntityType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SchemaEntitiesCollector
{
	private static final Logger LOGGER = LoggerFactory.getLogger(SchemaEntitiesCollector.class);
	
	private static String XMLSCHEMA_NS = "http://www.w3.org/2001/XMLSchema";
	
	private static QName parseReferenceAttribute(StartElement event, String attributeName, String currentNamespace)
	{
		Attribute xsRefAttributeEvent = event.getAttributeByName(new QName(attributeName));
		String xsRefAttribute = xsRefAttributeEvent == null ? null : xsRefAttributeEvent.getValue();
		if(xsRefAttribute == null)
		{
			return null;
		}
		
		if(xsRefAttribute.indexOf(":") >= 0)
		{
			// Reference from another namespace : resolve it.
			String[] refNsAndName = xsRefAttribute.split(":");
			return new QName(event.getNamespaceContext().getNamespaceURI(refNsAndName[0]), refNsAndName[1]);
		}
		else
		{
			// Reference from same namespace.
			return new QName(currentNamespace, xsRefAttribute);
		}
	}
	
	/**
		Read an XSD tag and check if it defines an entity.
		@param event The XSD tag to read from.
		@param isTopLevel True only if current tag is at top level of the schema, not inside another entity definition.
		@param targetNamespace The targetNamespace of the document being analysed.
		@param targetFile The file location of the document being analysed.
		@return A NamedEntity if this tag defines any, null otherwise.
	*/
	private static NamedEntity getEntityFromTag(StartElement event, boolean isTopLevel, String targetNamespace, File targetFile)
	{
		EntityType entityType = null;
		QName entityQName = null;
		Set<NamedEntityReference> entityBases = new HashSet<NamedEntityReference>();
		
		String xsTagName = event.getName().getLocalPart();
		Attribute xsNameAttributeEvent = event.getAttributeByName(new QName("name"));
		String xsNameAttribute = xsNameAttributeEvent == null ? null : xsNameAttributeEvent.getValue();
	
		if(("simpleType".equals(xsTagName) || "complexType".equals(xsTagName)) && xsNameAttribute != null)
		{
			entityType = EntityType.TYPE;
			entityQName = new QName(targetNamespace, xsNameAttribute);
		}
		else if("group".equals(xsTagName) && xsNameAttribute != null)
		{
			entityType = EntityType.GROUP;
			entityQName = new QName(targetNamespace, xsNameAttribute);
		}
		else if("attributeGroup".equals(xsTagName) && xsNameAttribute != null)
		{
			entityType = EntityType.ATTRIBUTE_GROUP;
			entityQName = new QName(targetNamespace, xsNameAttribute);
		}
		else if("element".equals(xsTagName) && xsNameAttribute != null)
		{
			if(isTopLevel)
			{
				entityType = EntityType.TOPLEVEL_ELEMENT;
			}
			else
			{
				entityType = EntityType.INNER_ELEMENT;
			}
			entityQName = new QName(targetNamespace, xsNameAttribute);
			
			// Element tag may define a base in its own type attribute.
			QName baseRefName = parseReferenceAttribute(event, "type", targetNamespace);
			if(baseRefName != null)
			{
				entityBases.add(new NamedEntityReference(EntityType.TYPE, baseRefName));
			}
		}
		
		if(entityQName != null && entityType != null)
		{
			// Instantiating the NamedEntity. Bases, Properties and Attributes are empty lists that will be filled later on.
			return new NamedEntity(
				entityType,
				entityQName,
				targetFile,
				entityBases,
				new HashSet<NamedEntityMember>()
			);
		}
		return null;
	}
	
	/**
		Read an XSD tag and check if it defines a reference to a base for its containing entity.
		A "base" is another entity from which the containing entity will inherit properties and attributes.
		@param event The XSD tag to read from.
		@param targetNamespace The targetNamespace of the document being analysed.
		@return A NamedEntityReference to the base entity if this tag refers to any, null otherwise.
	*/
	private static NamedEntityReference getBaseReferenceFromTag(StartElement event, String targetNamespace)
	{
		EntityType refType = null;
		QName refQName = null;
		
		String xsTagName = event.getName().getLocalPart();
		
		if("extension".equals(xsTagName) ||
			"restriction".equals(xsTagName))
		{
			refQName = parseReferenceAttribute(event, "base", targetNamespace);
			refType = EntityType.TYPE;
		}
		else if ("group".equals(xsTagName))
		{
			refQName = parseReferenceAttribute(event, "ref", targetNamespace);
			refType = EntityType.GROUP;
		}
		else if ("attributeGroup".equals(xsTagName))
		{
			refQName = parseReferenceAttribute(event, "ref", targetNamespace);
			refType = EntityType.ATTRIBUTE_GROUP;
		}
		
		if(refType != null && refQName != null)
		{
			return new NamedEntityReference(
				refType,
				refQName
			);
		}
		return null;
	}
	
	/**
		Read an XSD tag and check if it defines a property for its containing entity.
		@param event The XSD tag to read from.
		@param targetNamespace The targetNamespace of the document being analysed.
		@param targetFile The file location of the document being analysed.
		@return The name of the property if this tag defines any, null otherwise.
	*/
	private static NamedEntityMember getMemberFromTag(StartElement event, String targetNamespace, File targetFile)
	{
		String xsTagName = event.getName().getLocalPart();
		Attribute xsNameAttributeEvent = event.getAttributeByName(new QName("name"));
		String xsNameAttribute = xsNameAttributeEvent == null ? null : xsNameAttributeEvent.getValue();
		
		MemberType memberType = null;
		String memberName = null;
		
		if("any".equals(xsTagName))
		{
			// An xs:any tag systematically declares a jaxb property with name "Any".
			return new NamedEntityMember(MemberType.PROPERTY, "Any", targetFile);
		}
		
		if("element".equals(xsTagName))
		{
			memberType = MemberType.PROPERTY;
		}
		else if("attribute".equals(xsTagName))
		{
			memberType = MemberType.ATTRIBUTE;
		}
		
		if(memberType != null)
		{
			if(xsNameAttribute != null)
			{
				// If element definition, its name will be used for methods.
				memberName = xsNameAttribute;
			}
			else
			{
				// Else, element reference : parse the reference and get its local name, as it will be used for methods.
				QName propertyQName = parseReferenceAttribute(event, "ref", targetNamespace);
				if(propertyQName != null)
				{
					memberName = propertyQName.getLocalPart();
				}
			}
		}
		
		if(memberType != null && memberName != null)
		{
			NamedEntityMember result = new NamedEntityMember(memberType, memberName, targetFile);
			return result;
		}
		return null;
	}
	
	private static boolean entityJavaNameConflictsWithContainer(List<NamedEntity> enclosingEntities, String javaName)
	{
		for(NamedEntity enclosingEntity : enclosingEntities)
		{
			// Loop through all parent scopes to analyse containing entities.
			if(javaName.equals(enclosingEntity.getJavaName()))
			{
				// Given javaName is already used by a containing entity.
				LOGGER.debug("Java name {} conflicts with containing entity {}", javaName, enclosingEntity);
				return true;
			}
		}
		return false;
	}
	
	private static void collectEntities(Set<NamedEntity> registeredEntities, XMLElement currentElement, List<NamedEntity> enclosingEntities,
		String targetNamespace, File targetFile, boolean supportLocalScopingTopLevel)
	{
		List<NamedEntity> enclosingEntitiesForChildren = enclosingEntities;
		NamedEntity currentEntity = null;
		if(enclosingEntities.size() > 0)
		{
			// Current entity is the last enclosing entity.
			currentEntity = enclosingEntities.get(enclosingEntities.size() - 1);
		}
		
		StartElement event = currentElement.getEvent();
		QName qname = event.getName();
		if(XMLSCHEMA_NS.equals(qname.getNamespaceURI()))
		{
			// Step 1 : Check if this new tag contains any information relevant to the entity being defined in the current scope.
			if(currentEntity != null)
			{
				// Check if the element is a reference to a base for current entity.
				NamedEntityReference baseReference = getBaseReferenceFromTag(event, targetNamespace);
				if(baseReference != null)
				{
					LOGGER.debug("Event at {} registers new base {} for entity {}.", event.getLocation(), baseReference, currentEntity);
					currentEntity.getBases().add(baseReference);
				}

				// Check if the element is a member for current entity.
				NamedEntityMember member = getMemberFromTag(event, targetNamespace, targetFile);
				if(member != null)
				{
					member.setPropertyBoundNode(currentElement);
					LOGGER.debug("Event at {} registers new member {} for entity {}.", event.getLocation(), member, currentEntity);
					currentEntity.getMembers().add(member);
				}
			}
			
			// Step 2 : Check if the element is an entity definition itself; if so, this becomes the current entity for children elements.
			NamedEntity entity = getEntityFromTag(event, enclosingEntities.size() == 0, targetNamespace, targetFile);
			if(entity != null)
			{
				LOGGER.debug("Event at {} registers new entity {}.", event.getLocation(), entity);
				while(entityJavaNameConflictsWithContainer(enclosingEntities, entity.getJavaName()))
				{
					// As long as there is a conflict with a container's java name, prefix it with "Inner".
					entity.overrideJavaName("Inner" + entity.getJavaName());
				}
				
				if( // Register an entity if it matches one of the following cases:
					entity.isJavaNameOverridden() || // - a conflict was already detected and name overridden
					entity.getType() != EntityType.INNER_ELEMENT || // - it's an outer element (so it may have conflicts later on)
					supportLocalScopingTopLevel // - it's an inner element, but we need to support localscoping toplevel.
				)
				{
					registeredEntities.add(entity);
				}
				
				// Append this to the list of enclosing entities for children.
				enclosingEntitiesForChildren = new ArrayList<NamedEntity>(enclosingEntities);
				enclosingEntitiesForChildren.add(entity);
				currentEntity = entity;
			}
			
			// Step 3 : Check if the element is the class bound node for current entity.
			if("complexType".equals(qname.getLocalPart()))
			{
				// Complex type tag is the class bound node for current entity.
				// - Either entity defined by this same tag (named complex type).
				// - Or entity defined by parent element tag.
				LOGGER.debug("Event at {} registers new class bound node for entity {}.", event.getLocation(), currentEntity);
				currentEntity.setClassBoundNode(currentElement);
			}
		}
		
		// Step 4 : Handle all children recursively.
		for(XMLElement child : currentElement.getChildren())
		{
			collectEntities(registeredEntities, child, enclosingEntitiesForChildren, targetNamespace, targetFile, supportLocalScopingTopLevel);
		}
	}
	
	public static Set<NamedEntity> collectEntities(XMLElement schemaRootElement, String targetNamespace, File targetFile, boolean supportLocalScopingTopLevel)
	{
		Set<NamedEntity> registeredEntities = new HashSet<NamedEntity>();
		collectEntities(registeredEntities, schemaRootElement, new ArrayList<NamedEntity>(), targetNamespace, targetFile, supportLocalScopingTopLevel);
		return registeredEntities;
	}
}