package org.bitbucket.unaszole.xsdnormaliser;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.Set;
import org.apache.commons.io.FileUtils;

public class CatalogGenerator
{
	public static void generateCatalog(Set<String> normalisedNamespaces, File targetCatalogsDirectory, File targetSchemasDirectory, File sourceDirectory) throws IOException
	{
		FileUtils.forceMkdir(targetCatalogsDirectory);
		File catalogFile = new File(targetCatalogsDirectory, "catalog_" + sourceDirectory.getName() + "_" + (new Date().getTime()) + ".cat");
		BufferedWriter out = new BufferedWriter(new FileWriter(catalogFile));
		
		for(String namespace : normalisedNamespaces)
		{
			File targetEntrypoint = NamespaceEntrypointGenerator.getEntrypointForNamespace(namespace, targetSchemasDirectory);
			out.write("PUBLIC \"" + namespace + "\" \"" + FilePathUtils.getRelativePath(catalogFile, targetEntrypoint) + "\"");
			out.newLine();
		}
		
		out.close();
	}
}