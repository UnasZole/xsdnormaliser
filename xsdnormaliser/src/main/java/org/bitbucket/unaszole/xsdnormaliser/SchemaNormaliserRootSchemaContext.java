package org.bitbucket.unaszole.xsdnormaliser;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import org.bitbucket.unaszole.xmlstreameditor.*;
import org.bitbucket.unaszole.xsdnormaliser.schemahandlers.*;
import org.bitbucket.unaszole.xsdnormaliser.schemahandlers.xsd.NamedEntity;

public class SchemaNormaliserRootSchemaContext
	extends SchemaNormaliserBaseContext<SchemaNormaliserIncludedSchemaContext>
{
	/**
	@param sourceFile The location of the current file.
	@param strategy The strategy to use for managing inclusions.
	@param includeHandlerClasses The handlers to enable to normalise included files.
	
	@param targetSchemasDirectory The root folder for output schema files.
	@param sameSchemaName_KeepOnlyOne The strategy to handle multiple schemas with same name : keep only one if true, concatenate if false.
	*/
	public SchemaNormaliserRootSchemaContext(
		// Inputs of the edition in progress.
		File sourceFile,
		List<Class<? extends XMLEventHandler<? super SchemaNormaliserIncludedSchemaContext>>> includeHandlerClasses,
		
		// State of the edition in progress.
		IncludeHandler.NormalisedSchemasManager normalisedSchemasManager,
		
		// Configuration of the editor.
		IncludeHandler.Strategy strategy,
		boolean sameSchemaName_KeepOnlyOne)
	{
		super(sourceFile, new ArrayList<File>(), includeHandlerClasses, normalisedSchemasManager, new HashSet<String>(), strategy, sameSchemaName_KeepOnlyOne);
	}
	
	@Override
	public final SchemaNormaliserIncludedSchemaContext getIncludeContext(File sourceFile)
	{
		List<File> childIncludingSourceFiles = new ArrayList<File>();
		childIncludingSourceFiles.add(getSourceFile());
		
		return new SchemaNormaliserIncludedSchemaContext(
			sourceFile,
			childIncludingSourceFiles,
			getIncludeHandlerClasses(),
			getTargetNamespace(),
			
			getNormalisedSchemasManager(),
			getDependencies(),
			
			getInclusionStrategy(),
			this.sameSchemaName_KeepOnlyOne
		);
	}
}
	
