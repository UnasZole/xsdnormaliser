package org.bitbucket.unaszole.xsdnormaliser;

import java.io.File;
import org.apache.commons.io.FilenameUtils;

public class FilePathUtils
{
	/**
		Get the relative path URL string from a given source to a given target.
		@sourcePath The source path. If path exists and is a file, the relative path will be computed from the parent directory.
			If path does not exist, it will be assumed as a directory, and used as-is.
		@targetPath The target path.
		@return The relative path written as a URL string.
	*/
	public static String getRelativePath(File sourcePath, File targetPath)
	{
		// First, try to find a common ancestor.
		// Test all ancestors of source (starting with closest), and check if they are ancestors of target.
		File commonAncestor = null;
		File srcParentPath = sourcePath.getAbsoluteFile();
		if(srcParentPath.isFile())
		{
			// A path relative to a file is actually relative to its parent directory.
			srcParentPath = srcParentPath.getParentFile();
		}
		String pathToAncestor = "";
		while(commonAncestor == null && srcParentPath != null)
		{
			File targetParentPath = targetPath.getAbsoluteFile();
			while(commonAncestor == null && targetParentPath != null)
			{
				if(srcParentPath.equals(targetParentPath))
				{
					commonAncestor = srcParentPath;
				}
				else
				{
					targetParentPath = targetParentPath.getParentFile();
				}
			}
			
			if(commonAncestor == null)
			{
				srcParentPath = srcParentPath.getParentFile();
				pathToAncestor += "../";
			}
		}
		
		if(commonAncestor == null)
		{
			// No common ancestor : just return absolute URI to target.
			return targetPath.toURI().toString();
		}
		return pathToAncestor + (commonAncestor.toURI().relativize(targetPath.toURI())).getPath();
	}
	
	/**
		Get a pointer to a new file with a requested path, taking care of adding a suffix if it conflicts with an already existing file.
		@param requestedFilePath The desired file path.
		@return The path to the new available file.
	 */
	public static File getNewFile(File requestedFilePath)
	{
		if(!requestedFilePath.exists())
		{
			// If target file does not exist yet, return it.
			return requestedFilePath;
		}
		else
		{
			File newFilePath = requestedFilePath;
			// If a file with same name already exists, find a new available name by appending a suffix.
			int suffix = 0;
			while(newFilePath.exists())
			{
				suffix++;
				newFilePath = new File(requestedFilePath.getParentFile(), 
					FilenameUtils.getBaseName(requestedFilePath.getName()) + "-" + suffix+ "." + FilenameUtils.getExtension(requestedFilePath.getName()));
			}
			
			return newFilePath;
		}
	}
}