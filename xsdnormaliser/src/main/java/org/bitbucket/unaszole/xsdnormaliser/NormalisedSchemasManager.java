package org.bitbucket.unaszole.xsdnormaliser;

import com.sun.xml.bind.api.impl.NameConverter;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.io.FileUtils;
import org.bitbucket.unaszole.xsdnormaliser.schemahandlers.IncludeHandler;
import org.bitbucket.unaszole.xsdnormaliser.schemahandlers.xsd.NamedEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NormalisedSchemasManager implements IncludeHandler.NormalisedSchemasManager
{
	private static final Logger LOGGER = LoggerFactory.getLogger(NormalisedSchemasManager.class);
	
	private File targetSchemasDirectory = null;
	private boolean sameSchemaName_KeepOnlyOne = true;
	private boolean sameSchemaName_ReverseOrder = true;
	
	private Map<String, Set<NamedEntity>> entitiesPerNS;
	
	/**
	 * Map of all files already normalised.
	 * Uses the following successive indexes :
	 * target namespace -> source schema file name -> source schema file
	 * All files are stored in canonical form.
	 */
	private Map<String, Map<String, Map<File, File>>> normalisedFilesMappingPerNs = new HashMap<String, Map<String, Map<File, File>>>();
	
	public NormalisedSchemasManager(File targetSchemasDirectory, boolean sameSchemaName_KeepOnlyOne, boolean sameSchemaName_ReverseOrder,
		Map<String, Set<NamedEntity>> entitiesPerNS)
	{
		this.targetSchemasDirectory = targetSchemasDirectory;
		this.sameSchemaName_KeepOnlyOne = sameSchemaName_KeepOnlyOne;
		this.sameSchemaName_ReverseOrder = sameSchemaName_ReverseOrder;
		
		this.entitiesPerNS = entitiesPerNS;
	}
	
	private boolean isMappedSourceMatchingRequestedSource(File canonicalMappedSource, File canonicalRequestedSource)
	{
		if(canonicalMappedSource.equals(canonicalRequestedSource))
		{
			// If two files are the same, they will obviously match.
			return true;
		}
		else
		{
			if(!sameSchemaName_KeepOnlyOne)
			{
				// If not keeping only one schema : any other mapping won't match.
				return false;
			}
			
			// Keeping only one schema with same name : any file with higher precedence than the requested source will match.
			boolean mappedSourceBeforeRequestedSource = canonicalMappedSource.getPath().compareTo(canonicalRequestedSource.getPath()) < 0;
			if(mappedSourceBeforeRequestedSource == !sameSchemaName_ReverseOrder)
			{
				// Precedence is computed using lexicographic order, and the reverseOrder setting.
				LOGGER.debug("Mapped source {} is of higher precedence than requested source {} {}.", canonicalMappedSource, canonicalRequestedSource, sameSchemaName_ReverseOrder ? "(reverse order)" : "");
				return true;
			}
		}
		
		return false;
	}
	
	private void initMaps(String targetNamespace, File sourceFile)
	{
		if(!normalisedFilesMappingPerNs.containsKey(targetNamespace))
		{
			normalisedFilesMappingPerNs.put(targetNamespace, new HashMap<String, Map<File, File>>());
		}
		if(!normalisedFilesMappingPerNs.get(targetNamespace).containsKey(sourceFile.getName()))
		{
			normalisedFilesMappingPerNs.get(targetNamespace).put(sourceFile.getName(), new HashMap<File, File>());
		}
	}
	
	private static File getCanonicalFile(File file)
	{
		try
		{
			return file.getCanonicalFile();
		}
		catch(IOException e)
		{
			LOGGER.error("Received exception when resolving canonical path. Defaulting to absolute.", e);
			return file.getAbsoluteFile();
		}
	}
	
	@Override
	public File getPathOfAlreadyNormalisedSchema(String targetNamespace, File sourceFile)
	{
		File canonicalSourceFile = getCanonicalFile(sourceFile);
		
		initMaps(targetNamespace, canonicalSourceFile);
		
		for(Map.Entry<File, File> fileMapping : normalisedFilesMappingPerNs.get(targetNamespace).get(canonicalSourceFile.getName()).entrySet())
		{
			if(isMappedSourceMatchingRequestedSource(fileMapping.getKey(), canonicalSourceFile))
			{
				// This file mapping is considered to match : return the result.
				LOGGER.trace("Normalised file {} from {} is valid for a reference to {}.", fileMapping.getValue(), fileMapping.getKey(), canonicalSourceFile);
				return fileMapping.getValue();
			}
		}
		
		// No matching file mapping found : return null.
		LOGGER.trace("No valid file found for {}.", canonicalSourceFile);
		return null;
	}
	
	private static File getTargetNamespaceDirectory(File targetSchemasDirectory, String targetNamespace)
	{
		String nsPackageName = "".equals(targetNamespace) ? "__localnamespace" : (new NameConverter.Standard()).toPackageName(targetNamespace);
		
		return new File(targetSchemasDirectory, nsPackageName);
	}
	
	private void clearNormalisedSchema(String targetNamespace, final File canonicalSchemaToClear)
	{
		LOGGER.debug("Clearing an already normalised schema : {} {}.", targetNamespace, canonicalSchemaToClear);
		
		// Unregister all entities coming from this schema.
		Set<NamedEntity> entitiesFromNS = entitiesPerNS.get(targetNamespace);
		CollectionUtils.filter(entitiesFromNS, new Predicate<NamedEntity>() {
			@Override
			public boolean evaluate(NamedEntity entity)
			{
				return !getCanonicalFile(entity.getSchema()).equals(canonicalSchemaToClear);
			}
		});
		
		// Delete the normalised schema file.
		canonicalSchemaToClear.delete();
	}
	
	@Override
	public File getAndRegisterPathForNewNormalisedSchema(String targetNamespace, File sourceFile)
	{
		Map<File, File> fileMappingsForThisName = normalisedFilesMappingPerNs.get(targetNamespace).get(sourceFile.getName());
		
		if(sameSchemaName_KeepOnlyOne)
		{
			// If we must keep only one file with same name and we are creating a new one, clear all the others.
			if(fileMappingsForThisName.size() > 0)
			{
				LOGGER.info("New file for {} {} requested but keeping only one : clearing previously normalised versions.", targetNamespace, sourceFile);
				Iterator<Map.Entry<File, File>> iter = fileMappingsForThisName.entrySet().iterator();
				while(iter.hasNext())
				{
					clearNormalisedSchema(targetNamespace, iter.next().getValue());
					iter.remove();
				}
			}
		}
		
		File targetNamespaceDirectory = getTargetNamespaceDirectory(targetSchemasDirectory, targetNamespace);
		File targetFile = FilePathUtils.getNewFile(new File(targetNamespaceDirectory, sourceFile.getName()));
		
		// Register the new target file in the mappings.
		// Assume all maps have been set : contract specifies that this is called only if getPathOfAlreadyNormalisedSchema was called first and returned null.
		fileMappingsForThisName.put(getCanonicalFile(sourceFile), getCanonicalFile(targetFile));
		
		LOGGER.debug("New file at {} available for normalising {} {}.", targetFile, targetNamespace, sourceFile);
		
		return targetFile;
	}
	
	public Map<String, Set<File>> getSchemasPerNamespace()
	{
		Map<String, Set<File>> schemasPerNS = new HashMap<String, Set<File>>();
		for(Map.Entry<String, Map<String, Map<File, File>>> nsMappingEntries : normalisedFilesMappingPerNs.entrySet())
		{
			String namespace = nsMappingEntries.getKey();
			if(nsMappingEntries.getValue().size() > 0)
			{
				// There are actually some files normalised for this namespace : include in in results.
				
				Set<File> normalisedFilesInNs = new HashSet<File>();
				
				for(Map.Entry<String, Map<File, File>> fileNameMappingEntry : nsMappingEntries.getValue().entrySet())
				{
					for(Map.Entry<File, File> fileMappingEntry : fileNameMappingEntry.getValue().entrySet())
					{
						normalisedFilesInNs.add(fileMappingEntry.getValue());
					}
				}
				
				schemasPerNS.put(namespace, normalisedFilesInNs);
			}
		}
		return schemasPerNS;
	}
}