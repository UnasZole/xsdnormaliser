package org.bitbucket.unaszole.xsdnormaliser.schemahandlers;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import javax.xml.namespace.QName;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import org.bitbucket.unaszole.xmlstreameditor.ScopedEventHandler;
import org.bitbucket.unaszole.xmlstreameditor.XMLEventHandler;
import org.bitbucket.unaszole.xmlstreameditor.XMLStreamEditor;
import org.bitbucket.unaszole.xmlstreameditor.builders.EventList;
import org.bitbucket.unaszole.xmlstreameditor.builders.StartElementBuilder;
import org.bitbucket.unaszole.xmlstreameditor.eventsink.BufferedSink;
import org.bitbucket.unaszole.xmlstreameditor.eventsink.SinkManager;
import org.bitbucket.unaszole.xsdnormaliser.FilePathUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
	An XMLEventHandler that reads XMLSchema include tags and manages the included file according to different possible strategies.
	@param <IncludedContext> The interface of the context that will be passed down to the editor working on included files.
*/
public class IncludeHandler<IncludedContext extends SchemaNamespaceSetter.Context & IncludeHandler.Context<IncludedContext>>
	extends ScopedEventHandler<IncludeHandler.Scope, IncludeHandler.Context<IncludedContext>>
{
	private static final Logger LOGGER = LoggerFactory.getLogger(IncludeHandler.class);
	
	public interface Context<InclCtxt>
	{
		/** 
		@return The source file being worked on in this context.
		*/
		File getSourceFile();
		
		/**
		@return The stack of including files, all in absolute form.
			First item in the list is the top level schema,
			Nth item is the one included directly by the N-1th,
			Last item is the one including the current file.
		*/
		List<File> getIncludingSourceFiles();
		
		/**
		@return The targetNamespace of the schema being worked on in this context.
		*/
		String getTargetNamespace();
		
		/**
		@return The strategy to use for managing inclusions.
		*/
		Strategy getInclusionStrategy();
		
		/**
		@return The NormalisedSchemasManager for the current normalisation process.
		*/
		NormalisedSchemasManager getNormalisedSchemasManager();
		
		/**
		@return The list of event handlers to run on included files.
		*/
		List<Class<? extends XMLEventHandler<? super InclCtxt>>> getIncludeHandlerClasses();
		
		/**
		@param sourceFile The source file being included.
		@return The context used for handling the included file.
			Must share the same strategy and same normalised schemas manager.
			Must have same list of including source files, appended with the current source file.
		*/
		InclCtxt getIncludeContext(File sourceFile);
	}
	
	public static enum Strategy {
		/** Leave included schemas untouched, even if chameleon (ie empty targetNamespace attribute). */
		KEEP_CHAMELEON,
		/** Enforce the setting of targetNamespace attribute onto included schemas to same as current document. */
		ENFORCE_TARGET_NAMESPACE,
		/** Extract content of the included schema, and copy inside this schema's body. */
		IN_BODY 
	}
	
	public static interface NormalisedSchemasManager
	{
		/**
		 * Check if a normalised schema for the given namespace and file name already exists, and return its path.
		 * 
		 * @param targetNamespace The target namespace of the source schema to check for.
		 * @param sourceFile The path of the source schema to check for.
		 * @return The path to the normalised schema, if a matching source schema was already normalised and must be referenced.
		 * 	Null if no matching normalised schema exists yet : then, a new normalisation should be performed.
		 */
		public File getPathOfAlreadyNormalisedSchema(String targetNamespace, File sourceFile);
		
		/**
		 * Generate the path to the target normalised schema for a given source file in a given namespace.
		 * Note : This method MUST be called only if getPathOfAlreadyNormalisedSchema returned null.
		 * @param targetNamespace The target namespace of a new normalised schema.
		 * @param sourceFile The path of the source schema.
		 * @return The path where to place the new normalised schema.
		 */
		public File getAndRegisterPathForNewNormalisedSchema(String targetNamespace, File sourceFile);
	}
	
	public static class Scope extends ScopedEventHandler.BasicScope
	{
		private File sourceFile;
		
		public Scope(File sourceFile)
		{
			this.sourceFile = sourceFile;
		}
		
		public File getSourceFile()
		{
			return sourceFile;
		}
	}
	
	private boolean isHeadingElementStartEvent(StartElement event)
	{
		QName qname = event.getName();
		if(XMLSCHEMA_NS.equals(qname.getNamespaceURI()))
		{
			String elementName = qname.getLocalPart();
			if("schema".equals(elementName) || // TODO : change that : considering "schema" as heading element while it's actually root is a hack...
				"include".equals(elementName) ||
				"import".equals(elementName) ||
				"redefine".equals(elementName) ||
				"annotation".equals(elementName))
			{
				return true;
			}
		}
		return false;
	}
	
	private static String XMLSCHEMA_NS = "http://www.w3.org/2001/XMLSchema";
	
	private XMLStreamEditor<? super IncludedContext> streamEditor;
	
	private List<XMLEvent> headingEventList;
	private List<XMLEvent> bodyEventList;
	
	public IncludeHandler()
	{
		headingEventList = new ArrayList<XMLEvent>();
		bodyEventList = new ArrayList<XMLEvent>();
	}
	
	@Override
	protected Scope openScopeIfNeeded(XMLEvent inEvent, Scope currentScope, IncludeHandler.Context<IncludedContext> context)
	{
		if(inEvent.isStartElement())
		{
			StartElement event = inEvent.asStartElement();
			QName qname = event.getName();
			if(XMLSCHEMA_NS.equals(qname.getNamespaceURI()))
			{
				if("include".equals(qname.getLocalPart()))
				{
					String relativePath = event.getAttributeByName(new QName("schemaLocation")).getValue();
					File sourceFile = new File(context.getSourceFile().getParentFile(), relativePath);
					
					return new Scope(sourceFile);
				}
			}
		}
		return null;
	}
	
	@Override
	protected boolean isClosingScope(XMLEvent inEvent, Scope currentScope, IncludeHandler.Context<IncludedContext> context)
	{
		if(inEvent.isEndElement())
		{
			EndElement event = inEvent.asEndElement();
			QName qname = event.getName();
			if(XMLSCHEMA_NS.equals(qname.getNamespaceURI()))
			{
				if("include".equals(qname.getLocalPart()))
				{
					return true;
				}
			}
		}
		return false;
	}
	
	@Override
	protected List<XMLEvent> handleEventInScope(XMLEvent inEvent, Scope currentScope, IncludeHandler.Context<IncludedContext> context)
	{
		if(headingEventList != null && bodyEventList != null)
		{
			// There are included events ready to be dumped. Is it time ?
			if((inEvent.isStartElement() && !isHeadingElementStartEvent(inEvent.asStartElement())) ||
				(inEvent.isEndElement() && inEvent.asEndElement().getName().equals(new QName(XMLSCHEMA_NS, "schema"))))
			{
				// If starting a non-header element or ending schema, then dump included elements just before.
				List<XMLEvent> returnedEvents = new ArrayList<XMLEvent>();
				returnedEvents.addAll(headingEventList);
				returnedEvents.addAll(bodyEventList);
				returnedEvents.add(inEvent);
				// Mark everything as dumped already. 
				headingEventList = null;
				bodyEventList = null;
				
				LOGGER.trace("Reaching body : returning {}", returnedEvents);
				
				return returnedEvents;
			}
		}
		return null;
	}
	
	private File getCurrentTargetFile(IncludeHandler.Context<IncludedContext> context)
	{
		File currentTarget = context.getNormalisedSchemasManager().getPathOfAlreadyNormalisedSchema(context.getTargetNamespace(), context.getSourceFile());
		if(currentTarget == null)
		{
			currentTarget = context.getNormalisedSchemasManager().getAndRegisterPathForNewNormalisedSchema(context.getTargetNamespace(), context.getSourceFile());
		}
		return currentTarget;
	}
	
	@Override
	protected List<XMLEvent> closeScope(List<XMLEvent> events, Scope closedScope, IncludeHandler.Context<IncludedContext> context)
	{
		try
		{
			if(streamEditor == null)
			{
				List<Class<? extends XMLEventHandler<? super IncludedContext>>> handlerClasses = new ArrayList<Class<? extends XMLEventHandler<? super IncludedContext>>>(context.getIncludeHandlerClasses());
			
				switch(context.getInclusionStrategy())
				{
					case KEEP_CHAMELEON:
						// Nothing to do, keep same list of handlers.
						break;
					
					case ENFORCE_TARGET_NAMESPACE:
						handlerClasses.add(SchemaNamespaceSetter.class);
						break;
					
					case IN_BODY:
						handlerClasses.add(SchemaContentExtractor.class);
						break;
				}
				
				streamEditor = new XMLStreamEditor<IncludedContext>(handlerClasses);
			}
			
			String targetNamespace = context.getTargetNamespace();
			File sourceIncludingFile = context.getSourceFile();
			File sourceIncludedFile = closedScope.getSourceFile().getAbsoluteFile();
			
			switch(context.getInclusionStrategy())
			{
				case KEEP_CHAMELEON:
				case ENFORCE_TARGET_NAMESPACE:
					
					File targetIncludedFile = context.getNormalisedSchemasManager().getPathOfAlreadyNormalisedSchema(targetNamespace, sourceIncludedFile);
					if(targetIncludedFile != null)
					{
						// If the required source file for the inclusion has already been normalised, just refer to it.
						LOGGER.debug("Include of {} resolved to already normalised file {}.", sourceIncludedFile, targetIncludedFile);
					}
					else
					{
						// Else, we need to normalise it now.
						IncludedContext includedContext = context.getIncludeContext(sourceIncludedFile);
						targetIncludedFile = context.getNormalisedSchemasManager().getAndRegisterPathForNewNormalisedSchema(targetNamespace, sourceIncludedFile);
						
						LOGGER.info("\n== Starting normalisation of included file {}. ==\n\n", sourceIncludedFile);
						
						BufferedInputStream input = new BufferedInputStream(new FileInputStream(sourceIncludedFile));
						
						// Create the target file in proper folder (creating folder if necessary).
						targetIncludedFile.getParentFile().mkdirs();
						targetIncludedFile.createNewFile();
						
						// Run the stream editor on the included file.
						BufferedOutputStream output = new BufferedOutputStream(new FileOutputStream(targetIncludedFile));
						streamEditor.run(input, output, includedContext);
						input.close();
						output.close();
						
						LOGGER.info("\n== Completed normalisation of included file {}. ==\n\n", sourceIncludedFile);
					}
					
					// Determine relative path of the target included file relative to the parent directory of the current target file.
					// (current target file does not exist : use the parent directory explicitly)
					File targetIncludingFile = getCurrentTargetFile(context);
					String targetIncludedFileRelative = FilePathUtils.getRelativePath(targetIncludingFile.getParentFile(), targetIncludedFile);
					
					// Insert new include tag with target path at index 0 to replace previous one.
					StartElement newIncludeTag = StartElementBuilder.from(events.get(0).asStartElement()).withAttribute(new QName("schemaLocation"), targetIncludedFileRelative).build();
					events.set(0, newIncludeTag);
					
					break;
				
				case IN_BODY:
				
					// Eliminate events related to the include tag.
					// First event, index 0, is the StartElement that opened the scope.
					// Find position of the corresponding end element (which requested the closing of the scope).
					int endElementIndex = indexOfEndElementFor(0, events);
					// Remove events from 0 to end element.
					int numEventsToRemove = endElementIndex + 1;
					while(numEventsToRemove > 0)
					{
						events.remove(0);
						numEventsToRemove--;
					}
					
					if(context.getIncludingSourceFiles().indexOf(sourceIncludedFile) == -1)
					{
						// If file is already included, do nothing, just strip the include tag.
						// Else, manage the inclusion.
					
						// Prepare to read the contents of the file.
						IncludedContext includedContext = context.getIncludeContext(sourceIncludedFile);
						BufferedInputStream input = new BufferedInputStream(new FileInputStream(sourceIncludedFile));

						// If copying contents of include inside body, read the file into a buffered sink.
						BufferedSink outputSink = new BufferedSink();
						streamEditor.run(input, outputSink, includedContext);
						input.close();

						// Separate the list of heading elements, and the list of body elements. Populate the corresponding list in the handler.
						List<XMLEvent> results = outputSink.flush();

						LOGGER.trace("Parsing included file returned {}", results);

						boolean inHead = true;
						for(int i = 0; i < results.size(); i++)
						{
							XMLEvent resultEvent = results.get(i);
							if(resultEvent.isStartElement() && !isHeadingElementStartEvent(resultEvent.asStartElement()))
							{
								inHead = false;
							}
							if(inHead)
							{
								headingEventList.add(resultEvent);
							}
							else
							{
								bodyEventList.add(resultEvent);
							}
						}

						// Paste new contents to replace the include tag.

						// Find position of the first body element in the scoped events.
						int firstBodyPosition = -1;
						for(int i = 0; i < events.size() && firstBodyPosition == -1; i++)
						{
							XMLEvent resultEvent = results.get(i);
							if(resultEvent.isStartElement() && !isHeadingElementStartEvent(resultEvent.asStartElement()))
							{
								firstBodyPosition = i;
							}
						}

						// If there is a body element, then dump the new head and body elements in front.
						if(firstBodyPosition != -1)
						{
							events.addAll(firstBodyPosition, bodyEventList);
							events.addAll(firstBodyPosition, headingEventList);
							// Mark everything as dumped already.
							headingEventList = null;
							bodyEventList = null;
						}
					}
					
					break;
			}
		}
		catch(FileNotFoundException e)
		{
			// Unresolvable include : user error, reported as warning.
			LOGGER.warn("Included file not found.", e);
		}
		catch(Exception e)
		{
			// Unknown exception : log as error.
			LOGGER.error("Unexpected exception received while processing include.", e);
		}
		
		return events;
	}
}

