package org.bitbucket.unaszole.xsdnormaliser.schemahandlers;

import java.util.List;
import javax.xml.namespace.QName;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import org.bitbucket.unaszole.xmlstreameditor.XMLEventHandler;
import org.bitbucket.unaszole.xmlstreameditor.builders.EventList;
import org.bitbucket.unaszole.xmlstreameditor.builders.StartElementBuilder;
import org.bitbucket.unaszole.xmlstreameditor.eventsink.SinkManager;

/**
	An XMLEventHandler that reads the targetNamespace attribute and import tags of your schema to fill the context with this metadata.
*/
public class NamespaceHandler implements XMLEventHandler<NamespaceHandler.Context>
{
	public interface Context
	{
		/**
			Method called by NamespaceHandler to set the targetNamespace read from the schema.
			@param targetNS The targetNamespace if specified; the empty string if attribute is not present.
		*/
		void setTargetNamespace(String targetNS);
		
		/**
			Method called by NamespaceHandler to add a namespace to the list of dependencies.
			@param dependencyNS The namespace to add as a dependency.
		*/
		void addDependency(String dependencyNS);
	}
	
	private static String XMLSCHEMA_NS = "http://www.w3.org/2001/XMLSchema";
	
	@Override
	public List<XMLEvent> handle(XMLEvent inEvent, SinkManager sinkManager, Context context)
	{
		List<XMLEvent> replacementEvents = null;
		
		if(inEvent.isStartElement())
		{
			StartElement event = inEvent.asStartElement();
			QName qname = event.getName();
			if(XMLSCHEMA_NS.equals(qname.getNamespaceURI()))
			{
				if("schema".equals(qname.getLocalPart()))
				{
					Attribute targetNsAttribute = event.getAttributeByName(new QName("targetNamespace"));
					if(targetNsAttribute == null)
					{
						context.setTargetNamespace("");
					}
					else
					{
						context.setTargetNamespace(targetNsAttribute.getValue());
					}
				}
				else if("import".equals(qname.getLocalPart()))
				{
					context.addDependency(event.getAttributeByName(new QName("namespace")).getValue());
					
					replacementEvents = EventList.from(StartElementBuilder.from(event).withAttribute(new QName("schemaLocation"), null).build());
				}
			}
		}
		return replacementEvents;
	}
}

