package org.bitbucket.unaszole.xsdnormaliser;

import java.io.File;

public interface Configuration
{
	// ====== Input and Output directories ======
	
	/**
		@return The source directory to be normalised by XSDNormaliser.
	*/
	public File getSourcePath();
	
	/**
		@return The target directory in which to place normalised schemas, generated bindings and catalogs.
	*/
	public File getTargetDirectory();
	
	// ====== Management of xs:include tags and chameleon schemas. ======
	
	/**
		@return True if the contents of included files must be embedded directly inside the body of including files.
	*/
	public boolean include_InBody();
	
	/**
		@return True if the target namespace of the including file must be enforced on included files. Has no effect if #include_InBody() is true.
	*/
	public boolean include_EnforceTargetNamespace();
	
	// ====== Management of schemas with same file name in same namespace. ======
	
	/**
		@return True if we must keep only one schema and eliminate duplicates. False if all schema with same names should be merged.
	*/
	public boolean sameSchemaName_KeepOnlyOne();
	
	/**
		@return True if directories must be browsed in reverse lexicographic order; false if they must be browsed in lexicographic order.
			This has an effect on which file will be kept when #sameSchemaName_KeepOnlyOne() is true.
	*/
	public boolean sameSchemaName_ReverseOrder();
	
	// ====== Management of schemas with no namespace. ======
	
	/**
		@return True if schemas defined in local namespace (ie without a targetNamespace set) must be ignored; false if they must be included in the target.
	*/
	public boolean localNamespace_Ignore();
	
	// ====== Management of non-deterministic structures. ======
	
	/**
		@return True if normaliser must extract anonymous complexTypes defined inside groups into named complex types to avoid underterministic class generation.
	*/
	public boolean deterministicBuild_groupElements();
	
	// ====== Management of conflicting names in schemas. ======
	
	/**
		@return True if normaliser must generate a binding when an element, a group or a type in the same namespace share the same name.
	*/
	public boolean nameBinding_duplicateNames();
	
	/**
		@return True if normaliser must take into account support for localScoping=toplevel when generating bindings for duplicate names.
	*/
	public boolean nameBinding_duplicateNamesSupportTopLevelScoping();
	
	/**
		@return True if normaliser must generate a binding when an element, a group or a type's name conflicts with a used java class name.
			Eg. when a type is called "Collection"...
	*/
	public boolean nameBinding_javaTypeNames();
	
	// ====== Packaging of generated classes. ======
	
	/**
		@return The prefix to prepend to all packages generated when compiling this repository. Null if default packaging must be used. 
	*/
	public String packageBinding_Prefix();
}