package org.bitbucket.unaszole.xsdnormaliser.schemahandlers.xsd;

import com.sun.xml.bind.api.impl.NameConverter;
import java.io.File;
import javax.xml.namespace.QName;

import org.bitbucket.unaszole.xmlstreameditor.xml.XMLElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
	This class represents a member of a named entity. That is a pointer to another entity, that will be represented by Java methods.
*/
public class NamedEntityMember
{
	private static final Logger LOGGER = LoggerFactory.getLogger(NamedEntityMember.class);
	
	public static enum MemberType
	{
		PROPERTY, ATTRIBUTE;
	}
	
	private MemberType type;
	private String name;
	private File schema;
	private XMLElement propertyBoundNode;
	private String javaName;
	private boolean isJavaNameOverridden;

	/**
	 * @param type The type of member (property or attribute)
	 * @param name The name of the member.
	 * @param schema The XSD file containing the definition of this member. 
	 */
	public NamedEntityMember(MemberType type, String name, File schema)
	{
		this.type = type;
		this.name = name;
		this.schema = schema;
		this.javaName = new NameConverter.Standard().toPropertyName(name);
		this.isJavaNameOverridden = false;
	}
	
	public void overrideJavaName(String javaName)
	{
		LOGGER.debug("Overriding member {} to java name {}", this, javaName);
		this.javaName = javaName;
		this.isJavaNameOverridden = true;
	}
	
	public MemberType getType()
	{
		return type;
	}
	
	public String getName()
	{
		return name;
	}
	
	public String getJavaName()
	{
		return javaName;
	}
	
	public boolean isJavaNameOverridden()
	{
		return isJavaNameOverridden;
	}
	
	public File getSchema()
	{
		return schema;
	}
	
	public void setPropertyBoundNode(XMLElement propertyBoundNode)
	{
		this.propertyBoundNode = propertyBoundNode;
	}
	
	/**
	 * @return The location of the node that will bear property bindings for this member. 
	 * 	Null if there is no jaxb property generated by this member.
	 */
	public XMLElement getPropertyBoundNode()
	{
		return propertyBoundNode;
	}
	
	public String toString()
	{
		return "(" + 
			type + ":" + name +
			", javaName:" + javaName + (isJavaNameOverridden ? "(overridden)" : "") +
			" => " + schema + "|" + propertyBoundNode +
		")";
	}
}
