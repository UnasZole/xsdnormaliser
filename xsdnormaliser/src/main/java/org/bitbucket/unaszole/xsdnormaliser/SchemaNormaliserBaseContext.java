package org.bitbucket.unaszole.xsdnormaliser;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.bitbucket.unaszole.xmlstreameditor.*;
import org.bitbucket.unaszole.xsdnormaliser.schemahandlers.xsd.NamedEntity;
import org.bitbucket.unaszole.xsdnormaliser.schemahandlers.*;

public abstract class SchemaNormaliserBaseContext<IncludedContext> implements
	IncludeHandler.Context<IncludedContext>,
	NamespaceHandler.Context,
	SchemaNamespaceSetter.Context,
	StructureAwareEventHandler.Context
{
	private File sourceFile;
	private List<File> includingSourceFiles;
	private IncludeHandler.Strategy strategy;
	private List<Class<? extends XMLEventHandler<? super IncludedContext>>> includeHandlerClasses;
	private String targetNamespace;
	protected File targetFile;
	private IncludeHandler.NormalisedSchemasManager normalisedSchemasManager;
	private Set<String> dependencies;
	
	protected boolean sameSchemaName_KeepOnlyOne;
	
	private StructureAwareEventHandler.CurrentElementProvider elementProvider;
	
	/**
	@param sourceFile The location of the current file.
	@param strategy The strategy to use for managing inclusions.
	@param includeHandlerClasses The handlers to enable to normalise included files.
	@param dependencies The list that should be filled by all dependencies of the current namespace.
	@param entities The list that should be filled by all entities registered in the current namespace.
	*/
	public SchemaNormaliserBaseContext(
		// Inputs of the edition in progress.
		File sourceFile,
		List<File> includingSourceFiles,
		List<Class<? extends XMLEventHandler<? super IncludedContext>>> includeHandlerClasses,
		
		// State of the edition in progress.
		IncludeHandler.NormalisedSchemasManager normalisedSchemasManager,
		Set<String> dependencies,
		
		// Configuration of the editor.
		IncludeHandler.Strategy strategy,
		boolean sameSchemaName_KeepOnlyOne)
	{
		this.sourceFile = sourceFile;
		this.includingSourceFiles = includingSourceFiles;
		this.includeHandlerClasses = includeHandlerClasses;
		
		this.normalisedSchemasManager = normalisedSchemasManager;
		this.dependencies = dependencies;
		
		this.strategy = strategy;
		this.sameSchemaName_KeepOnlyOne = sameSchemaName_KeepOnlyOne;
		
		this.elementProvider = new StructureAwareEventHandler.CurrentElementProvider();
	}
	
	@Override
	public final String getTargetNamespace()
	{
		return targetNamespace;
	}
	
	@Override
	public final void setTargetNamespace(String targetNamespace)
	{
		// If targetNamespace currently unknown, set it.
		if(this.targetNamespace == null)
		{
			this.targetNamespace = targetNamespace;
		}
		
		// Else, ignore.
	}
	
	@Override
	public final File getSourceFile()
	{
		return sourceFile;
	}
	
	@Override
	public final List<File> getIncludingSourceFiles()
	{
		return includingSourceFiles;
	}
	
	@Override
	public final IncludeHandler.Strategy getInclusionStrategy()
	{
		return strategy;
	}
	
	@Override
	public final IncludeHandler.NormalisedSchemasManager getNormalisedSchemasManager()
	{
		return normalisedSchemasManager;
	}
	
	@Override
	public final List<Class<? extends XMLEventHandler<? super IncludedContext>>> getIncludeHandlerClasses()
	{
		return includeHandlerClasses;
	}
	
	@Override
	public abstract IncludedContext getIncludeContext(File sourceFile);
	
	@Override
	public final void addDependency(String dependencyNS)
	{
		this.dependencies.add(dependencyNS);
	}
	
	public final Set<String> getDependencies()
	{
		return dependencies;
	}
	
	@Override
	public final StructureAwareEventHandler.CurrentElementProvider getEltProvider()
	{
		return this.elementProvider;
	}
}
