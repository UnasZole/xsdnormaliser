package org.bitbucket.unaszole.xsdnormaliser.schemahandlers;

import java.io.InputStream;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.xml.namespace.QName;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.StartDocument;
import javax.xml.stream.events.XMLEvent;
import org.bitbucket.unaszole.xmlstreameditor.XMLEventHandler;
import org.bitbucket.unaszole.xmlstreameditor.XMLStreamEditor;
import org.bitbucket.unaszole.xmlstreameditor.eventsink.BufferedSink;
import org.junit.Test;

import static org.junit.Assert.*;

public class SchemaContentExtractorTest
{
	private static String XMLSCHEMA_NS = "http://www.w3.org/2001/XMLSchema";
	
	@Test
	public void testSchemaContentExtracted() throws Exception
	{
		XMLStreamEditor<Object> editor = new XMLStreamEditor<Object>(Arrays.<Class<? extends XMLEventHandler<? super Object>>>asList(SchemaContentExtractor.class));
		InputStream input = this.getClass().getResourceAsStream("nsTest.xsd");
		
		BufferedSink outputSink = new BufferedSink();
		
		editor.run(input, outputSink, null);
		
		int importsFound = 0;
		
		for(XMLEvent event : outputSink.flush())
		{
			if(event.isStartDocument())
			{
				fail("Start document was not removed.");
			}
			if(event.isEndDocument())
			{
				fail("End document was not removed.");
			}
			
			if(event.isStartElement())
			{
				StartElement startElt = event.asStartElement();
				if(startElt.getName().equals(new QName(XMLSCHEMA_NS, "schema")))
				{
					fail("Schema start tag not removed.");
				}
				else if(startElt.getName().equals(new QName(XMLSCHEMA_NS, "import")))
				{
					importsFound++;
				}
			}
			
			if(event.isEndElement())
			{
				EndElement endElt = event.asEndElement();
				if(endElt.getName().equals(new QName(XMLSCHEMA_NS, "schema")))
				{
					fail("Schema end tag not removed.");
				}
			}
		}
		
		assertEquals("All contents (2 imports) are found.", 2, importsFound);
	}
}

