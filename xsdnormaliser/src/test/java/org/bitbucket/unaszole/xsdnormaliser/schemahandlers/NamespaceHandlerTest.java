package org.bitbucket.unaszole.xsdnormaliser.schemahandlers;

import java.io.InputStream;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.bitbucket.unaszole.xmlstreameditor.XMLEventHandler;
import org.bitbucket.unaszole.xmlstreameditor.XMLStreamEditor;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.junit.Assert.*;

public class NamespaceHandlerTest
{
	private static final Logger LOGGER = LoggerFactory.getLogger(NamespaceHandlerTest.class);
	
	public class TestNSContext implements NamespaceHandler.Context
	{
		private String targetNS = null;
		private List<String> dependencies = new ArrayList<String>();
		
		@Override
		public void setTargetNamespace(String targetNS)
		{
			this.targetNS = targetNS;
		}
		
		public String getTargetNamespace()
		{
			return targetNS;
		}
		
		@Override
		public void addDependency(String dependencyNS)
		{
			this.dependencies.add(dependencyNS);
		}
		
		public List<String> getDependencies()
		{
			return dependencies;
		}
	}
	
	@Test
	public void testTargetNamespacePickedUp() throws Exception
	{
		TestNSContext context = new TestNSContext();
		
		XMLStreamEditor<NamespaceHandler.Context> editor = new XMLStreamEditor<NamespaceHandler.Context>(Arrays.<Class<? extends XMLEventHandler<? super NamespaceHandler.Context>>>asList(NamespaceHandler.class));
		
		InputStream input = this.getClass().getResourceAsStream("nsTest.xsd");
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		
		editor.run(input, output, context);
		
		String result = output.toString("UTF-8");
		
		assertEquals("TargetNamespace correctly read.", "http://bitbucket.org/UnasZole/nsTest", context.getTargetNamespace());
		assertEquals("Correct number of dependencies.", 2, context.getDependencies().size());
		assertEquals("First dependency correct.", "http://bitbucket.org/UnasZole/nsDependencyTest1", context.getDependencies().get(0));
		assertEquals("Second dependency correct.", "http://bitbucket.org/UnasZole/nsDependencyTest2", context.getDependencies().get(1));
		assertEquals("First schemaLocation removed.", -1, result.indexOf("schemaLocation=\"here\""));
		assertEquals("Second schemaLocation removed.", -1, result.indexOf("schemaLocation=\"there\""));
	}
	
	@Test
	public void canBeTerminatedDependingOnNamespaceRead() throws Exception
	{
		TestNSContext context = new TestNSContext();
		XMLStreamEditor.Controller<TestNSContext> controller = new XMLStreamEditor.Controller<TestNSContext>() {
			public boolean terminate(TestNSContext context)
			{
				// Terminate if target namespace is read (not null) but empty.
				return "".equals(context.getTargetNamespace());
			}
		
			public boolean unplug(XMLEventHandler<? super TestNSContext> handler, TestNSContext context)
			{
				return false;
			}
		};
		
		XMLStreamEditor<TestNSContext> editor = new XMLStreamEditor<TestNSContext>(Arrays.<Class<? extends XMLEventHandler<? super TestNSContext>>>asList(NamespaceHandler.class));
		
		InputStream input = this.getClass().getResourceAsStream("chameleonTest.xsd");
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		
		editor.run(input, output, context, controller);
		
		String result = output.toString("UTF-8");
		
		LOGGER.trace("Resulting document : {}", result);
		
		assertEquals("TargetNamespace correctly read as unset.", "", context.getTargetNamespace());
		assertEquals("Tags after schema (here import) were not parsed.", -1, result.indexOf(":import"));
	}
}

