package org.bitbucket.unaszole.xsdnormaliser.schemahandlers;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.bitbucket.unaszole.xmlstreameditor.XMLEventHandler;
import org.bitbucket.unaszole.xmlstreameditor.XMLStreamEditor;
import org.bitbucket.unaszole.xsdnormaliser.NormalisedSchemasManager;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.junit.Assert.*;

public class IncludeHandlerTest
{
	private static final Logger LOGGER = LoggerFactory.getLogger(IncludeHandlerTest.class);
	
	public static class TestIncludeContext implements IncludeHandler.Context<IncludeHandlerTest.TestIncludeContext>, SchemaNamespaceSetter.Context
	{
		private String targetNamespace;
		private File sourceFile;
		private List<File> includingSourceFiles;
		private IncludeHandler.Strategy strategy;
		private List<Class<? extends XMLEventHandler<? super TestIncludeContext>>> handlerClasses;
		NormalisedSchemasManager manager = new NormalisedSchemasManager(null, true, true, null);
		
		public TestIncludeContext(String targetNamespace, File sourceFile, List<File> includingSourceFiles, IncludeHandler.Strategy strategy, List<Class<? extends XMLEventHandler<? super TestIncludeContext>>> handlerClasses)
		{
			this.targetNamespace = targetNamespace;
			this.sourceFile = sourceFile;
			this.includingSourceFiles = includingSourceFiles;
			this.strategy = strategy;
			this.handlerClasses = handlerClasses;
		}
		
		@Override
		public String getTargetNamespace()
		{
			return targetNamespace;
		}
		
		@Override
		public File getSourceFile()
		{
			return sourceFile;
		}
		
		@Override
		public List<File> getIncludingSourceFiles()
		{
			return includingSourceFiles;
		}
		
		@Override
		public IncludeHandler.Strategy getInclusionStrategy()
		{
			return strategy;
		}
		
		@Override
		public IncludeHandler.NormalisedSchemasManager getNormalisedSchemasManager()
		{
			return manager;
		}
		
		@Override
		public List<Class<? extends XMLEventHandler<? super TestIncludeContext>>> getIncludeHandlerClasses()
		{
			return handlerClasses;
		}
		
		@Override
		public TestIncludeContext getIncludeContext(File sourceFile)
		{
			List<File> childIncludingSourceFiles = new ArrayList<File>(getIncludingSourceFiles());
			childIncludingSourceFiles.add(getSourceFile());
			
			return new TestIncludeContext(targetNamespace, sourceFile, childIncludingSourceFiles, strategy, handlerClasses);
		}
	}
	
	public static class TestIncludeHandler extends IncludeHandler<TestIncludeContext>{}
	
	@Test
	public void testInBodyInclusion() throws Exception
	{
		File sourceFile = new File(this.getClass().getResource("includeTest.xsd").toURI());
		List<Class<? extends XMLEventHandler<? super TestIncludeContext>>> handlerClasses = Arrays.<Class<? extends XMLEventHandler<? super TestIncludeContext>>>asList(TestIncludeHandler.class);
		
		TestIncludeContext context = new TestIncludeContext(null, sourceFile, new ArrayList<File>(), IncludeHandler.Strategy.IN_BODY, handlerClasses);
		XMLStreamEditor<TestIncludeContext> editor = new XMLStreamEditor<TestIncludeContext>(handlerClasses);
		
		FileInputStream input = new FileInputStream(sourceFile);
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		
		editor.run(input, output, context);
		
		String result = output.toString("UTF-8");
		
		LOGGER.trace("Resulting document : {}", result);
		
		assertTrue("Namespace 1 import present.", result.indexOf("nsDependencyTest1") != -1);
		assertTrue("Namespace 2 import present.", result.indexOf("nsDependencyTest2") != -1);
		assertTrue("Namespace 3 import present.", result.indexOf("nsIncludeTest3") != -1);
		assertTrue("Namespace 4 import present.", result.indexOf("nsIncludeTest4") != -1);
		assertTrue("Plop element present.", result.indexOf("name=\"Plop\"") != -1);
		assertTrue("No include left.", result.indexOf(":include") == -1);
		
		assertTrue("xs prefix only used if defined.", result.indexOf("xmlns:xs=") != -1 || result.indexOf("<xs:") == -1);
		assertTrue("xsd prefix only used if defined.", result.indexOf("xmlns:xsd=") != -1 || result.indexOf("<xsd:") == -1);
	}
	
	@Test
	public void testInBodyInclusionMultipleNestedIncludes() throws Exception
	{
		File sourceFile = new File(this.getClass().getResource("includeTest2.xsd").toURI());
		
		List<Class<? extends XMLEventHandler<? super TestIncludeContext>>> handlerClasses = Arrays.<Class<? extends XMLEventHandler<? super TestIncludeContext>>>asList(TestIncludeHandler.class);
		
		TestIncludeContext context = new TestIncludeContext(null, sourceFile, new ArrayList<File>(), IncludeHandler.Strategy.IN_BODY, handlerClasses);
		XMLStreamEditor<TestIncludeContext> editor = new XMLStreamEditor<TestIncludeContext>(handlerClasses);
		
		FileInputStream input = new FileInputStream(sourceFile);
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		
		editor.run(input, output, context);
		
		String result = output.toString("UTF-8");
		
		LOGGER.trace("Resulting document : {}", result);
		
		assertTrue("Namespace 1 import present.", result.indexOf("nsDependencyTest1") != -1);
		assertTrue("Namespace 2 import present.", result.indexOf("nsDependencyTest2") != -1);
		assertTrue("Namespace 3 import present.", result.indexOf("nsIncludeTest3") != -1);
		assertTrue("Namespace 4 import present.", result.indexOf("nsIncludeTest4") != -1);
		assertTrue("Namespace 5 import present.", result.indexOf("nsIncludeTest5") != -1);
		assertTrue("Namespace 6 import present.", result.indexOf("nsIncludeTest6") != -1);
		assertTrue("Plop element present.", result.indexOf("name=\"Plop\"") != -1);
		assertTrue("Blahblah element present.", result.indexOf("name=\"Blahblah\"") != -1);
		assertTrue("No include left.", result.indexOf(":include") == -1);
		
		assertTrue("Elements are after imports.", result.indexOf(":element") > result.lastIndexOf(":import"));
	}
}

