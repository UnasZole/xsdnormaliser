package org.bitbucket.unaszole.xsdnormaliser.schemahandlers;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import org.bitbucket.unaszole.xmlstreameditor.StructureAwareEventHandler;
import org.bitbucket.unaszole.xmlstreameditor.XMLEventHandler;
import org.bitbucket.unaszole.xmlstreameditor.XMLStreamEditor;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.junit.Assert.*;

public class GroupElementsHandlerTest
{
	private static final Logger LOGGER = LoggerFactory.getLogger(GroupElementsHandlerTest.class);
	
	public static class TestGroupElementsContext implements StructureAwareEventHandler.Context
	{
		StructureAwareEventHandler.CurrentElementProvider elementProvider = new StructureAwareEventHandler.CurrentElementProvider();
		@Override
		public StructureAwareEventHandler.CurrentElementProvider getEltProvider()
		{
			return this.elementProvider;
		}
	}
	
	@Test
	public void testGroupElements() throws Exception
	{
		File sourceFile = new File(this.getClass().getResource("groupElementsTest.xsd").toURI());
		List<Class<? extends XMLEventHandler<? super TestGroupElementsContext>>> handlerClasses = Arrays.<Class<? extends XMLEventHandler<? super TestGroupElementsContext>>>asList(GroupElementsHandler.class);
		
		TestGroupElementsContext context = new TestGroupElementsContext();
		XMLStreamEditor<TestGroupElementsContext> editor = new XMLStreamEditor<TestGroupElementsContext>(handlerClasses);
		
		FileInputStream input = new FileInputStream(sourceFile);
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		
		editor.run(input, output, context);
		
		String result = output.toString("UTF-8");
		
		assertTrue("New named ComplexType present.", result.indexOf("name=\"SampleGroupSampleAnonymousTypeElt\"") != -1);
		assertTrue("New named ComplexType referenced.", result.indexOf("type=\"SampleGroupSampleAnonymousTypeElt\"") != -1);
		assertTrue("xmlns on new tags is not set to xsd.", result.indexOf("xmlns=\"http://www.w3.org/2001/XMLSchema\"") == -1);
		
		LOGGER.trace("Resulting document : {}", result);
	}
}

