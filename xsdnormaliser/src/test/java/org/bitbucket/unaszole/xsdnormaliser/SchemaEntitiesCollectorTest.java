package org.bitbucket.unaszole.xsdnormaliser;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;
import org.bitbucket.unaszole.xsdnormaliser.schemahandlers.xsd.NamedEntity;
import org.bitbucket.unaszole.xsdnormaliser.schemahandlers.xsd.NamedEntityMember;
import org.bitbucket.unaszole.xsdnormaliser.schemahandlers.xsd.NamedEntityMember.MemberType;
import org.bitbucket.unaszole.xsdnormaliser.schemahandlers.xsd.NamedEntityReference;
import org.bitbucket.unaszole.xsdnormaliser.schemahandlers.xsd.NamedEntityReference.EntityType;
import org.bitbucket.unaszole.xmlstreameditor.StructureAwareEventHandler;
import org.bitbucket.unaszole.xmlstreameditor.XMLEventHandler;
import org.bitbucket.unaszole.xmlstreameditor.XMLStreamEditor;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.junit.Assert.*;

public class SchemaEntitiesCollectorTest
{
	private static final Logger LOGGER = LoggerFactory.getLogger(SchemaEntitiesCollectorTest.class);
	
	public static class TestEntityNamesContext implements StructureAwareEventHandler.Context
	{
		StructureAwareEventHandler.CurrentElementProvider elementProvider = new StructureAwareEventHandler.CurrentElementProvider();
		@Override
		public StructureAwareEventHandler.CurrentElementProvider getEltProvider()
		{
			return this.elementProvider;
		}
	}
	
	@Test
	public void testRegisteredEntities() throws Exception
	{
		File sourceFile = new File(this.getClass().getResource("nameConflictsTest.xsd").toURI());
		List<Class<? extends XMLEventHandler<? super TestEntityNamesContext>>> handlerClasses = Arrays.<Class<? extends XMLEventHandler<? super TestEntityNamesContext>>>asList(StructureAwareEventHandler.StructureCollectingHandler.class);
		
		TestEntityNamesContext context = new TestEntityNamesContext();
		XMLStreamEditor<TestEntityNamesContext> editor = new XMLStreamEditor<TestEntityNamesContext>(handlerClasses);
		
		FileInputStream input = new FileInputStream(sourceFile);
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		
		editor.run(input, output, context);
		
		Set<NamedEntity> registeredEntities = SchemaEntitiesCollector.collectEntities(context.getEltProvider().getDocumentRootElement(),
				"http://bitbucket.org/UnasZole/nameConflictsTest", null, false);
		
		assertTrue("Entities were registered.", registeredEntities.size() > 0);
		
		LOGGER.trace("Registered entities : {}", registeredEntities);
		
		// Check Child conflict : inner element is overridden and registered; outer element is not overridden (but here, is registered because toplevel).
		
		Collection<NamedEntity> innerChildConflictElts = CollectionUtils.select(registeredEntities, new Predicate<NamedEntity>() {
			@Override
			public boolean evaluate(NamedEntity entity)
			{
				return 
					EntityType.INNER_ELEMENT.equals(entity.getType()) && 
					"ChildConflictElt".equals(entity.getQName().getLocalPart());
			} 
		});
		assertEquals("Inner element is returned in case of child conflict.", 1, innerChildConflictElts.size());
		assertTrue("Inner element has name overridden in case of child conflict.", innerChildConflictElts.iterator().next().isJavaNameOverridden());
		
		Collection<NamedEntity> outerChildConflictElts = CollectionUtils.select(registeredEntities, new Predicate<NamedEntity>() {
			@Override
			public boolean evaluate(NamedEntity entity)
			{
				return 
					EntityType.TOPLEVEL_ELEMENT.equals(entity.getType()) && 
					"ChildConflictElt".equals(entity.getQName().getLocalPart());
			} 
		});
		assertEquals("Top level element is returned.", 1, outerChildConflictElts.size());
		assertFalse("Top level element is not overridden in case of child conflict.", outerChildConflictElts.iterator().next().isJavaNameOverridden());
		
		// Check elements for type and elt conflict are properly registered.
		Collection<NamedEntity> typeAndEltConflictElts = CollectionUtils.select(registeredEntities, new Predicate<NamedEntity>() {
			@Override
			public boolean evaluate(NamedEntity entity)
			{
				return "TypeAndEltNonConflict".equals(entity.getQName().getLocalPart());
			} 
		});
		assertEquals("Two conflicting elements for type and elt conflict.", 2, typeAndEltConflictElts.size());
		for(NamedEntity entity : typeAndEltConflictElts)
		{
			if(EntityType.TOPLEVEL_ELEMENT.equals(entity.getType()))
			{
				assertEquals("Toplevel element has one base type.", 1, entity.getBases().size());
				NamedEntityReference base = entity.getBases().iterator().next();
				assertTrue("Toplevel element has base type properly set.", 
					EntityType.TYPE.equals(base.getType()) && "TypeAndEltNonConflict".equals(base.getQName().getLocalPart()));
			}
		}
		
		// Check elements for attr and prop conflict are correctly registered, with all necessary information to detect conflicts later on.
		Collection<NamedEntity> attrAndPropConflictBaseElts = CollectionUtils.select(registeredEntities, new Predicate<NamedEntity>() {
			@Override
			public boolean evaluate(NamedEntity entity)
			{
				return "AttrAndPropConflictBase".equals(entity.getQName().getLocalPart());
			}
		});
		assertEquals("One base type with conflicting property.", 1, attrAndPropConflictBaseElts.size());
		assertEquals("Base type has a property.", 1, attrAndPropConflictBaseElts.iterator().next().getMembers().size());
		assertEquals("Base type property has correct conflicting name.", "AttrAndPropConflict", attrAndPropConflictBaseElts.iterator().next().getMembers().iterator().next().getName());
		
		Collection<NamedEntity> attrAndPropConflictExtensionElts = CollectionUtils.select(registeredEntities, new Predicate<NamedEntity>() {
			@Override
			public boolean evaluate(NamedEntity entity)
			{
				return "AttrAndPropConflictExtension".equals(entity.getQName().getLocalPart());
			}
		});
		assertEquals("One extension type with conflicting property.", 1, attrAndPropConflictExtensionElts.size());
		assertEquals("Extension has one base.", 1, attrAndPropConflictExtensionElts.iterator().next().getBases().size());
		assertEquals("Extension base is Base type.", "AttrAndPropConflictBase", attrAndPropConflictExtensionElts.iterator().next().getBases().iterator().next().getQName().getLocalPart());
		assertEquals("Extension type has an attribute.", 1, attrAndPropConflictExtensionElts.iterator().next().getMembers().size());
		assertEquals("Extension type attribute has correct conflicting name.", "AttrAndPropConflict", attrAndPropConflictExtensionElts.iterator().next().getMembers().iterator().next().getName());
	}
}
