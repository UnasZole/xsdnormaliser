package org.bitbucket.unaszole.xsdnormaliser;

import static org.junit.Assert.*;

import com.sun.tools.xjc.Driver;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class XSDNormaliserTest
{
	private static final Logger LOGGER = LoggerFactory.getLogger(XSDNormaliserTest.class);
	
	@Rule
	public final TemporaryFolder junitTemporaryFolder = new TemporaryFolder();
	
	public class TestConfiguration implements Configuration
	{
		private File srcPath;
		private boolean inBody;
		private File targetDirectory;
		
		public TestConfiguration(boolean inBody) throws Exception
		{
			srcPath = new File(this.getClass().getResource("globaltest").toURI());
			this.inBody = inBody;
			this.targetDirectory = junitTemporaryFolder.newFolder("xsdn_target");
		}
		
		@Override
		public File getSourcePath()
		{
			return srcPath;
		}
		
		@Override
		public File getTargetDirectory()
		{
			return targetDirectory;
		}
		
		@Override
		public boolean include_InBody()
		{
			return inBody;
		}
		
		@Override
		public boolean include_EnforceTargetNamespace()
		{
			return false;
		}
		
		@Override
		public boolean sameSchemaName_KeepOnlyOne()
		{
			return true;
		}
		
		@Override
		public boolean sameSchemaName_ReverseOrder()
		{
			return true;
		}
		
		@Override
		public boolean localNamespace_Ignore()
		{
			return true;
		}
		
		@Override
		public boolean deterministicBuild_groupElements()
		{
			return true;
		}
		
		@Override
		public boolean nameBinding_duplicateNames()
		{
			return true;
		}
		
		@Override
		public boolean nameBinding_duplicateNamesSupportTopLevelScoping()
		{
			return false;
		}
		
		@Override
		public boolean nameBinding_javaTypeNames()
		{
			return true;
		}
		
		@Override
		public String packageBinding_Prefix()
		{
			return "xsdnormalisertest.generatedclasses";
		}
	}
	
	private File getTargetFilePath(File rootFolder, String namespaceFolder, String fileId)
	{
		int dashIndex = fileId.indexOf('-');
		String fileName;
		if(dashIndex >= 0)
		{
			fileName = fileId.substring(0, dashIndex) + ".xsd";
		}
		else
		{
			fileName = fileId + ".xsd";
		}
		return new File(rootFolder, namespaceFolder + '/' + fileName);
	}
	
	private boolean fileContainsString(File file, String string) throws Exception
	{
		return FileUtils.readFileToString(file, "UTF-8").indexOf(string) >= 0;
	}
	
	private static final String NS_1 = "http://bitbucket.org/UnasZole/nsTest1";
	private static final String NS_1_FOLDER = "org.bitbucket.unaszole.nstest1";
	private static final String NS_2 = "http://bitbucket.org/UnasZole/nsTest2";
	private static final String NS_2_FOLDER = "org.bitbucket.unaszole.nstest2";
	
	private static final String FILEID_CHAMELEON_INCLUDING_NS1 = "ChameleonIncludingFileNS1";
	private static final String FILEID_CHAMELEON_INCLUDING_NS2 = "ChameleonIncludingFileNS2";
	private static final String FILEID_CHAMELEON_INCLUDED = "ChameleonIncludedFile";
	
	private static final String FILEID_SAMENAME_DIFFNS_NS1 = "SameFileNameDifferentNS-nsTest1";
	private static final String FILEID_SAMENAME_DIFFNS_NS2 = "SameFileNameDifferentNS-nsTest2";
	
	private static final String FILEID_SAMENAME_DIFFSAMENS_RA = "SameFileNameSameNS-nsTest1-releaseA";
	private static final String FILEID_SAMENAME_DIFFSAMENS_RB = "SameFileNameSameNS-nsTest1-releaseB";
	
	private void verifyChameleonInclusionInBody(File targetSchemasDirectory) throws Exception
	{
		// Check that the contents (including File ID) of the chameleon included file was copied into the body of both including files
		assertTrue(fileContainsString(getTargetFilePath(targetSchemasDirectory, NS_1_FOLDER, FILEID_CHAMELEON_INCLUDING_NS1), FILEID_CHAMELEON_INCLUDING_NS1));
		assertTrue(fileContainsString(getTargetFilePath(targetSchemasDirectory, NS_1_FOLDER, FILEID_CHAMELEON_INCLUDING_NS1), FILEID_CHAMELEON_INCLUDED));
		assertTrue(fileContainsString(getTargetFilePath(targetSchemasDirectory, NS_2_FOLDER, FILEID_CHAMELEON_INCLUDING_NS2), FILEID_CHAMELEON_INCLUDING_NS2));
		assertTrue(fileContainsString(getTargetFilePath(targetSchemasDirectory, NS_2_FOLDER, FILEID_CHAMELEON_INCLUDING_NS2), FILEID_CHAMELEON_INCLUDED));
	}
	
	private void verifySameNameFilesKeepOnlyOneReverseOrder(File targetSchemasDirectory) throws Exception
	{
		// Check that the file with same name and same NS is actually the one from release B, not from release A.
		assertTrue(fileContainsString(getTargetFilePath(targetSchemasDirectory, NS_1_FOLDER, FILEID_SAMENAME_DIFFSAMENS_RA), FILEID_SAMENAME_DIFFSAMENS_RB));
		assertFalse(fileContainsString(getTargetFilePath(targetSchemasDirectory, NS_1_FOLDER, FILEID_SAMENAME_DIFFSAMENS_RA), FILEID_SAMENAME_DIFFSAMENS_RA));
		
		// Check that both files with same name but different NS are correctly dispatched to their respective folder.
		assertTrue(fileContainsString(getTargetFilePath(targetSchemasDirectory, NS_1_FOLDER, FILEID_SAMENAME_DIFFNS_NS1), FILEID_SAMENAME_DIFFNS_NS1));
		assertTrue(fileContainsString(getTargetFilePath(targetSchemasDirectory, NS_2_FOLDER, FILEID_SAMENAME_DIFFNS_NS2), FILEID_SAMENAME_DIFFNS_NS2));
	}
	
	private void verifyXjcGeneratesClasses(File targetDirectory) throws Exception
	{
		File targetSchemasDirectory = new File(targetDirectory, "schemas");
		File targetCatalogsDirectory = new File(targetDirectory, "catalogs");
		File targetBindingsDirectory = new File(targetDirectory, "bindings");
		File xjcOutputDirectory = junitTemporaryFolder.newFolder("xjc_target");
		
		// Invoke xjc to ensure classes can be generated.
		List<String> xjcArguments = new ArrayList<String>();
		
		// Add source schema directory.
		xjcArguments.add(targetSchemasDirectory.getAbsolutePath() + "/entrypoints");
		
		// Add source binding directory.
		xjcArguments.add("-b");
		xjcArguments.add(targetBindingsDirectory.getAbsolutePath());
		
		// Add catalog files one by one.
		for(File catalogFile : targetCatalogsDirectory.listFiles())
		{
			xjcArguments.add("-catalog");
			xjcArguments.add(catalogFile.getAbsolutePath());
		}
		
		// Add output directory for generated classes.
		xjcArguments.add("-d");
		xjcArguments.add(xjcOutputDirectory.getAbsolutePath());
		
		// Debug mode to help investigating potential issues.
		xjcArguments.add("-debug");
		
		try
		{
			LOGGER.info("Calling xjc with arguments {}.", xjcArguments);
			Driver.run(xjcArguments.toArray(new String[]{}), System.out, System.out);
		}
		catch(Throwable e)
		{
			LOGGER.error("Received exception from XJC.", e);
		}
		
		//Thread.sleep(Long.MAX_VALUE);
		
		// Check that some classes have actually been generated - and in the package prefix folder.
		assertTrue("XJC generated some classes", new File(xjcOutputDirectory, "xsdnormalisertest").exists());
	}
	
	/*@Test
	public void testXSDNormaliserIncludeInBody() throws Exception
	{
		Configuration testConfig = new TestConfiguration(true);
		
		// Run the normaliser.
		new XSDNormaliser(testConfig).run();
		
		// Perform simple validations on the normalised schemas.
		File targetSchemasDirectory = new File(testConfig.getTargetDirectory(), "schemas");
		verifyChameleonInclusionInBody(targetSchemasDirectory);
		verifySameNameFilesKeepOnlyOneReverseOrder(targetSchemasDirectory);
		
		// Perform XJC run to validate it generates classes successfully.
		verifyXjcGeneratesClasses(testConfig.getTargetDirectory());
	}*/
	
	@Test
	public void testXSDNormaliserIncludeOutOfBody() throws Exception
	{
		Configuration testConfig = new TestConfiguration(false);
		
		// Run the normaliser.
		new XSDNormaliser(testConfig).run();
		
		// Perform simple validations on the normalised schemas.
		File targetSchemasDirectory = new File(testConfig.getTargetDirectory(), "schemas");
		verifySameNameFilesKeepOnlyOneReverseOrder(targetSchemasDirectory);
		
		// Perform XJC run to validate it generates classes successfully.
		verifyXjcGeneratesClasses(testConfig.getTargetDirectory());
	}
}