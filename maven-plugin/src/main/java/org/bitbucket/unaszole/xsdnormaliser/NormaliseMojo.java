package org.bitbucket.unaszole.xsdnormaliser;

import java.io.File;
import java.io.FileNotFoundException;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.bitbucket.unaszole.xsdnormaliser.Configuration;
import org.bitbucket.unaszole.xsdnormaliser.XSDNormaliser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Mojo(name = "normalise")
public class NormaliseMojo extends AbstractMojo implements Configuration
{
	private static final Logger LOGGER = LoggerFactory.getLogger(NormaliseMojo.class);
	
	@Parameter(defaultValue = "${project.basedir}/src")
	private File sourcePath;
	
	@Parameter(defaultValue = "${project.build.directory}/normalisedsrc")
	private File targetDirectory;
	
	@Parameter(defaultValue = "false")
	private boolean includeInBody;
	
	@Parameter(defaultValue = "true")
	private boolean includeEnforceTargetNamespace;
	
	@Parameter(defaultValue = "true")
	private boolean sameSchemaNameKeepOnlyOne;
	
	@Parameter(defaultValue = "true")
	private boolean sameSchemaNameReverseOrder;
	
	@Parameter(defaultValue = "true")
	private boolean localNamespaceIgnore;
	
	@Parameter(defaultValue = "true")
	private boolean deterministicBuildGroupElements;
	
	@Parameter(defaultValue = "true")
	private boolean nameBindingDuplicateNames;
	
	@Parameter(defaultValue = "false")
	private boolean nameBindingDuplicateNamesSupportTopLevelScoping;
	
	@Parameter(defaultValue = "true")
	private boolean nameBindingJavaTypeNames;
	
	@Parameter(defaultValue = "")
	private String packageBindingPrefix;
	
	@Parameter(defaultValue = "WARN", property = "xsdn.loglevel")
	private String logLevel;
	
	@Override
	public File getSourcePath()
	{
		return sourcePath;
	}

	@Override
	public File getTargetDirectory()
	{
		return targetDirectory;
	}
	
	@Override
	public boolean include_InBody()
	{
		return includeInBody;
	}
	
	@Override
	public boolean include_EnforceTargetNamespace()
	{
		return includeEnforceTargetNamespace;
	}
	
	@Override
	public boolean sameSchemaName_KeepOnlyOne()
	{
		return sameSchemaNameKeepOnlyOne;
	}
	
	@Override
	public boolean sameSchemaName_ReverseOrder()
	{
		return sameSchemaNameReverseOrder;
	}
	
	@Override
	public boolean localNamespace_Ignore()
	{
		return localNamespaceIgnore;
	}
	
	@Override
	public boolean deterministicBuild_groupElements()
	{
		return deterministicBuildGroupElements;
	}
	
	@Override
	public boolean nameBinding_duplicateNames()
	{
		return nameBindingDuplicateNames;
	}
	
	@Override
	public boolean nameBinding_duplicateNamesSupportTopLevelScoping()
	{
		return nameBindingDuplicateNamesSupportTopLevelScoping;
	}
	
	@Override
	public boolean nameBinding_javaTypeNames()
	{
		return nameBindingJavaTypeNames;
	}
	
	@Override
	public String packageBinding_Prefix()
	{
		return packageBindingPrefix;
	}
	
	public void execute() throws MojoExecutionException
	{
		org.apache.log4j.Logger rootLogger = org.apache.log4j.Logger.getRootLogger();
		rootLogger.setLevel(org.apache.log4j.Level.toLevel(logLevel));
		try
		{
			new XSDNormaliser(this).run();
		}
		catch(Exception e)
		{
			LOGGER.error("Exception received while running XSD normaliser.", e);
			throw new MojoExecutionException("Exception received while running XSD normaliser.", e);
		}
	}
}
